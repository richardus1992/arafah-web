<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Session;
use Crypt;
use DB;
use Validator;
use App\Http\Controllers\Utils\MenuAccess;

class AdminControll extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:admin', ['except' => ['logout', 'getLogout']]);
    }
    public function showLoginForm()
    {
        $cript = Crypt::encrypt('admin');
        return view('backend.login.index', compact('cript'));
    }
    public function login( Request $request ){
        $username   = $request->input('username');
        $password   = $request->input('password');

        $this->validate($request, [
            'username'     => 'required',
            'password'  => 'required'
        ]);

        $user =DB::table('USERS as usr')
            ->select(
                'usr.*',
                'usr.USERNAME as UserName',

                'pgw.NAMA_PGW as EmployeName'

            )
            ->join('PEGAWAI as pgw','pgw.ID_PGW','usr.IDEMPLOYE')
            ->where('usr.USERNAME',$username)
            ->where('usr.ACTIVED','>',0)
            ->first();

        if(!empty($user)) {
            if (Crypt::decrypt($user->PASSWORD) == $password) {
                session()->put('admin', $user);
                $userAccess = DB::table('USER_ACCES')
                    ->select(
                        'IDMODULE',
                        'ACTION'
                    )

                    ->where('IDTYPEUSER',$user->TYPEUSER)
                    ->get();
//                return dump($userAccess);
                MenuAccess::allowAccess($userAccess);


                return redirect()->to('/HomeAdmin');
            }else{
                return redirect()->back()->withErrors(['errmsg'=>"Username Or Password Wrong"]);
            }
        }else{
            return redirect()->back()->withErrors(['errmsg'=>"Username Or Password Wrong"]);
        }



//        return redirect()->back()->withInput( $request->only('username', 'password') );
        //return redirect()->back();
    }
    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect()->to('manage');
    }

}
