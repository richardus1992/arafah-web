<?php

namespace App\Http\Controllers\Utils;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExportWordControll extends Controller{

    public function exportFile($filename,$data){
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection(array('orientation'=>'landscape'));
        $fontStyleName = 'oneUserDefinedStyle';
        $phpWord->addFontStyle(
            $fontStyleName,
            array('name' => 'Tahoma', 'size' => 10, 'color' => '1B2232', 'bold' => true)
        );

        $fontStyle = new \PhpOffice\PhpWord\Style\Font();
        $fontStyle->setBold(true);
        $fontStyle->setName('Tahoma');
        $fontStyle->setSize(13);

        $styleTable = array('borderSize'=>6, 'borderColor'=>'006699', 'cellMargin'=>80);
        $styleFirstRow = array('borderBottomSize'=>18, 'borderBottomColor'=>'0000FF', 'bgColor'=>'66BBFF');
        $styleCell = array('valign'=>'center');

        $phpWord->addTableStyle('myOwnTableStyle', $styleTable, $styleFirstRow);
        $table = $section->addTable('myOwnTableStyle');
        $table->addRow(1000);
        foreach($data[0] as $key => $value){
            $table->addCell(2000, $styleCell)->addText(ucfirst($key), $fontStyle);

        }
        foreach($data as $row){
            $table->addRow();
            foreach ($row as $value){
                $table->addCell(2000)->addText($value);
            }

        }


        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('clouds/backend/file/document/'.$filename.'.docx');

        return response()->download('clouds/backend/file/document/'.$filename.'.docx');
    }
    public function exportFileDoc($filename,$data){
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        $fontStyleName = 'oneUserDefinedStyle';
        $phpWord->addFontStyle(
            $fontStyleName,
            array('name' => 'Tahoma', 'size' => 10, 'color' => '1B2232', 'bold' => true)
        );

        $fontStyle = new \PhpOffice\PhpWord\Style\Font();
        $fontStyle->setBold(true);
        $fontStyle->setName('Tahoma');
        $fontStyle->setSize(13);

        $styleTable = array('borderSize'=>6, 'borderColor'=>'006699', 'cellMargin'=>80);
        $styleFirstRow = array('borderBottomSize'=>18, 'borderBottomColor'=>'0000FF', 'bgColor'=>'66BBFF');
        $styleCell = array('valign'=>'center');

        $phpWord->addTableStyle('myOwnTableStyle', $styleTable, $styleFirstRow);
        $table = $section->addTable('myOwnTableStyle');
        $table->addRow(1000);
        $icol=0;
        foreach($data[0] as $key => $value){
            $icol++;
            $table->addCell(2000, $styleCell)->addText(ucfirst($key), $fontStyle);

        }
        foreach($data as $row){
            $table->addRow();
            foreach ($row as $value){
                $section = $phpWord->addSection();
                $table->addCell(2000)->addText(\PhpOffice\PhpWord\Shared\Html::addHtml($section, $value, false, false));
            }

        }


        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('clouds/backend/file/document/'.$filename.'.docx');

        return response()->download('clouds/backend/file/document/'.$filename.'.docx');
    }
}
