<?php

namespace App\Http\Controllers\Utils;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DayControll extends Controller
{
    public function dayName($date){
        $tgl=substr($date,8,2);
        $bln=substr($date,5,2);
        $thn=substr($date,0,4);

        $info=date('w', mktime(0,0,0,$bln,$tgl,$thn));

        switch($info){
            case '0': return "MINGGU"; break;
            case '1': return "SENIN"; break;
            case '2': return "SELASA"; break;
            case '3': return "RABU"; break;
            case '4': return "KAMIS"; break;
            case '5': return "JUMAT"; break;
            case '6': return "SABTU"; break;
        };

        return $info;
    }

}
