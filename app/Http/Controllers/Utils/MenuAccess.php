<?php
/**
 * Created by PhpStorm.
 * User: IRUR
 * Date: 17/12/2017
 * Time: 21:13
 */

namespace App\Http\Controllers\Utils;

use Illuminate\Http\Request;
use Session;

class MenuAccess
{

    protected $listMenu;

    public static function preventAccessMenu(Request $req)
    {
        $result = false;
        $actionReq = $req->route()->action;
        $currMenu = isset($actionReq['as'])?'HomeAdmin/'.$actionReq['as']:'';
        $regRoute = session()->has('registered_route')?session('registered_route'):[];
        $allowURL = session()->has('allow_url')?session('allow_url'):[];

        if(in_array($currMenu,$regRoute) && !in_array($currMenu,$allowURL)){
            $result = true;
        }

//    dump($allowURL);


        return $result;
    }
    public static function getModuleId(Request $req)
    {
        $result = false;
        $actionReq = $req->route()->action;
        $currMenu = isset($actionReq['as'])?'HomeAdmin/'.$actionReq['as']:'';
        $regRoute = session()->has('registered_route')?session('registered_route'):[];
        $allowURL = session()->has('allow_url')?session('allow_url'):[];

        if(in_array($currMenu,$regRoute) && !in_array($currMenu,$allowURL)){
            $result = true;
        }


        return $result;
    }

    public function listMenu(){
        return [
            'customer.create',
            'customer.create',
            'customer.create',
            'customer.create',
            'customer.create',
            'customer.create',
        ];
    }

    public static function buildNavMenu()
    {
        $path = base_path()."/clouds/menu.json";

        $jsonMenu = json_decode(file_get_contents($path), true);
        $navHTML = MenuAccess::generateItemMenu($jsonMenu);

        return $navHTML;
    }

    public static function generateItemMenu($jsonMenu){
        $navHTML = "";
        foreach ($jsonMenu as $i=>$v)
        {
            $navHTML .= '<li id="menu-'.$v['id'].'" class="nav-menu"><a href="#'.(isset($v['route'])?$v['route']:'').'">'
                .(isset($v['icon'])?'<i class="fa '.$v['icon'].' menu-item-icon"></i>':'')
                .'<span class="inner-text">'.$v['name'].'</span>'
                .'</a>';
            if(isset($v['sub']))
            {

                $navHTML .= '<ul>'.MenuAccess::generateItemMenu($v['sub']).'</ul>';
            }
            $navHTML .= '</li>';
        }
        return $navHTML;
    }

    public static function listingRegisteredRoute()
    {
        $path = base_path()."/clouds/menu.json";
        $jsonMenu = json_decode(file_get_contents($path), true);


        $arrRegRoute=MenuAccess::addRoute($jsonMenu);
        session()->put('registered_route', $arrRegRoute);

        return $arrRegRoute;
    }

    public static function addRoute($jsonMenu){
        $arrRegRoute=[];
        foreach ($jsonMenu as $i=>$v)
        {
            if(isset($v['route'])){
                array_push($arrRegRoute,$v['route'].'.index');
                array_push($arrRegRoute,$v['route'].'.create');
                array_push($arrRegRoute,$v['route'].'.edit');
                array_push($arrRegRoute,$v['route'].'.show');
                array_push($arrRegRoute,$v['route'].'.delete');
                array_push($arrRegRoute,$v['route'].'.unpublish');
//        array_push($arrRegRoute,$v['route'].'.appstatus');
//        array_push($arrRegRoute,$v['route'].'.exportData');
            }
            if(isset($v['sub'])){
                $temp = MenuAccess::addRoute($v['sub']);
                foreach ($temp as $val)
                {
                    array_push($arrRegRoute,$val);
                }
            }
            if(isset($v['add_route'])){
                foreach ($v['add_route'] as $val){
                    array_push($arrRegRoute,$val['route']);
                }
            }
        }
        return $arrRegRoute;
    }

    public static function buildForm()
    {
        $path = base_path()."/clouds/menu.json";
        $jsonMenu = json_decode(file_get_contents($path), true);


        $treeForm = "<ul class='access'>".MenuAccess::generateTreeForm($jsonMenu)."</ul>";


        return $treeForm;
    }

    public static function generateTreeForm($jsonMenu)
    {
        $formHTML = "";
        foreach ($jsonMenu as $i=>$v)
        {
            $formHTML .= '<li><input type="checkbox" name="UserAccess[]" '
                .'value="{\'id\':\''.$v['id'].'\''.(isset($v['route'])?',\'action\':\''.$v['route'].'.index\'':'').'}"'
                .' id="'.$v['id'].'" '.(isset($v['route'])?'act="'.$v['route'].'.index"':'').'>'.$v['name'];

            if(isset($v['sub']))
            {
                $formHTML .= '<ul>'.MenuAccess::generateTreeForm($v['sub']).'</ul>';
            }
            elseif (isset($v['route']))
            {
                $formHTML .= '<ul class="action">';

                if(isset($v['add_route']))
                {
                    foreach ($v['add_route'] as $val)
                    {
                        $formHTML .= '<li><input type="checkbox" name="UserAccess[]" '
                            .'value="{\'id\':\''.$v['id'].'\',\'action\':\''.$val['route'].'\'}"'
                            .' id="'.$v['id'].'" act="'.$val['route'].'">'.$val['title'].'</li>';
                    }
                }

                $formHTML .= '<li><input type="checkbox" name="UserAccess[]" '
                    .'value="{\'id\':\''.$v['id'].'\',\'action\':\''.$v['route'].'.create\'}"'
                    .' id="'.$v['id'].'" act="'.$v['route'].'.create">Create</li>'
                    .'<li><input type="checkbox" name="UserAccess[]" '
                    .'value="{\'id\':\''.$v['id'].'\',\'action\':\''.$v['route'].'.edit\'}"'
                    .' id="'.$v['id'].'" act="'.$v['route'].'.edit">Edit</li>'
                    .'<li><input type="checkbox" name="UserAccess[]" '
                    .'value="{\'id\':\''.$v['id'].'\',\'action\':\''.$v['route'].'.delete\'}"'
                    .' id="'.$v['id'].'" act="'.$v['route'].'.delete">Delete</li>'
                    .'</ul>';
            }

            $formHTML .= '</li>';
        }

        return $formHTML;
    }

    public static function allowAccess($userAccess)
    {
        MenuAccess::listingRegisteredRoute();
        $allowMenu = [];
        $allowUrl = [];
        foreach ($userAccess as $val){
            array_push($allowMenu,$val->IDMODULE);
            if(!empty($val->ACTION)){
                $action = json_decode($val->ACTION,true);
//                $action = $val->ACTION;
                foreach ((array) $action  as $act){
                    array_push($allowUrl,$act);
                }
            }
        }
        session()->put('allow_menu', $allowMenu);
        session()->put('allow_url', $allowUrl);
    }

}