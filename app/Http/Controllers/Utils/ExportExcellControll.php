<?php

namespace App\Http\Controllers\Utils;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ExportExcellControll implements FromView, WithEvents
{

    private $data;
    private $layout;
    private $col;

    public function __construct($data, $col, $layout='backend.layouts.export-excel')
    {
        $this->data = $data;
        $this->layout = $layout;
        $this->col = $col;
    }


    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $spreadsheet = new Spreadsheet();
                $Alph = [];
                foreach (range('A', 'Z') as $char) {
                    $Alph[] = $char;
                }
                $countData = count($this->data);
                $countData1 = count($this->data)+1;

                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000'],
                        ],
                    ],
                ];

                $styleArrayOutline = [
                    'borders' => [
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000'],
                        ],
                    ],
                ];

                $event->sheet->getDelegate()->getStyle('A1:'.$Alph[$this->col].'1')->getFont()->setSize(12);
                $event->sheet->getDelegate()->getStyle('A1:'.$Alph[$this->col].'1')->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle('A1:'.$Alph[$this->col].$countData1)->applyFromArray($styleArray);

                $event->sheet->getDelegate()->getStyle('A1:'.$Alph[$this->col].$countData1)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $event->sheet->getDelegate()->getStyle('A1:'.$Alph[$this->col].$countData1)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                $event->sheet->getDelegate()->getStyle('A1:'.$Alph[$this->col].$countData1)->getAlignment()->setWrapText(true);

                for ($i=0; $i < $this->col+1; $i++) {
                    $event->sheet->getDelegate()->getColumnDimension($Alph[$i])->setWidth(20);
                }
                
            },
        ];
    }


    public function view(): View
    {
        return view($this->layout, [
            'data' => $this->data
        ]);
    }
}
