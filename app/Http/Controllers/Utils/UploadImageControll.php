<?php

namespace App\Http\Controllers\Utils;


use App\Http\Controllers\BackEnd\HistoryControll;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use App\Product;
use Illuminate\Http\Request;
use Validator;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class UploadImageControll extends Controller{

    protected $History;
    protected $ActElement;
    protected $PathControll;

    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();
        $this->PathControll = new PathControll();

    }

    public function uploadimage(Request $request){
        $uploadedFile = $request->file('file');
        $idlocaion  =$request->input('IdLocation');
        $fileName   ='';
        if($uploadedFile!==null){
            if(!empty($uploadedFile)){
                try{
                    $tempimg = Image::make($uploadedFile);
                    $width = $tempimg->getWidth();
                    $height = $tempimg->getHeight();

                    if($width > $height){
                        $tempimg->resize(500, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }else{
                        $tempimg->resize(null, 500, function ($constraint) {
                            $constraint->aspectRatio();
                        });

                    }


                    $fileName = time().rand(1, 99999).'.'.pathinfo($uploadedFile->getClientOriginalName(),PATHINFO_EXTENSION);
//            $uploadedFile->move($this->PathControll->PathFile(1),$fileName);
                    $tempimg->save($this->PathControll->saveimage(1).$fileName);
                }catch (\Exception $e){
                    dump($e->getMessage());
                }

            }
        }


        return $fileName;
    }

    public function uploadimagechamp(Request $request){
        $uploadedFile = $request->file('Image');
        $fileName   ='';
        if($uploadedFile!==null){
            if(!empty($uploadedFile)){
                try{
                    $tempimg = Image::make($uploadedFile);
                    $width = $tempimg->getWidth();
                    $height = $tempimg->getHeight();

                    if($width > $height){
                        $tempimg->resize(500, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }else{
                        $tempimg->resize(null, 500, function ($constraint) {
                            $constraint->aspectRatio();
                        });

                    }


                    $fileName = time().rand(1, 99999).'.'.pathinfo($uploadedFile->getClientOriginalName(),PATHINFO_EXTENSION);
                    $tempimg->save($this->PathControll->saveimage(2).$fileName);
                }catch (\Exception $e){
                    dump($e->getMessage());
                }

            }
        }


        return array("message"=>$fileName, "error"=>false, "Judging"=>[]);
    }
}
