<?php

namespace App\Http\Controllers\Utils;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MessageControll extends Controller
{
    public function messageregister($id){
        
        switch ($id){
            
            case 0:
                
                return json_encode(array(
                    "id"=>$id,
                    "tittle"=>"Sucess",
                    "button"=>true,
                    "buttontittle"=>"Kirim Ulang",
                    "content"=>"Register Telah Berhasil Silahkan Check Email Anda Untuk Melakukan Verifikasi Acount Anda.Jika Anda Belum Mendapatkan Verifikasi Email Silahkan Tekan Tombol Di Bawah Ini"
                    ));
                break;
            case 1:

                return  json_encode(array(
                    "id"=>$id,
                    "tittle"=>"Gagal",
                    "button"=>true,
                    "buttontittle"=>"Kirim Ulan",
                    "content"=>"Maaf Email Anda Sudah Terdaftar Silahkan Check Email Anda.Atau Menekan Tombol Lupa Password Jika Anda Lupa Dengan Password Anda."
                ));
                break;

            case 2:

                return  json_encode(array(
                    "id"=>$id,
                    "tittle"=>"Verify",
                    "button"=>true,
                    "buttontittle"=>"Lupa Password",
                    "content"=>"Verifikasi Telah Berhasil Silahkan Login Di Akun Anda.Terima Kasih"
                ));
                break;
            case 3:

                return  json_encode(array(
                    "id"=>$id,
                    "tittle"=>"Verify",
                    "button"=>true,
                    "buttontittle"=>"Lupa Password",
                    "content"=>"Silahkan Check Email Anda Untuk Restart Password Anda"
                ));
                break;
            case 99:

                return  json_encode(array(
                    "id"=>$id,
                    "tittle"=>"Sorry",
                    "button"=>true,
                    "buttontittle"=>"Kembali Ke Home",
                    "content"=>"Maaf Kami Tidak Bisa Menemukan Halaman Yang Anda Maksud"
                ));
                break;
        }
    }

    public function apimessage($id){
        switch ($id){
            case 1:
                return  json_encode(array(
                    "message"=>"Login Succes",
                    "erorr"=>false,
                    "data"=>[],

                ));

                break;
            case 2:
                return  json_encode(array(
                    "message"=>"Login Failed Check UserName And Password",
                    "erorr"=>true,
                    "data"=>[],

                ));

                break;


        }

    }
}
