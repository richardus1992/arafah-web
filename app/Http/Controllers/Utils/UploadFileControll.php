<?php

namespace App\Http\Controllers\Utils;

use App\Http\Controllers\BackEnd\HistoryControll;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use App\Product;
use Validator;
use File;
use Storage;
use Intervention\Image\ImageManagerStatic as Image;

class UploadFileControll extends Controller
{
    protected $History;
    protected $ActElement;
    protected $PathControll;

    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();
        $this->PathControll = new PathControll();

    }

    public function uploadFile($uploadedFile){
        $filepath='';
            try {
                $exten = $uploadedFile->getClientOriginalExtension();
                $fileName = time() . rand(1, 99999) . '.' . pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_EXTENSION);

                if ($exten == 'doc' || $exten == 'DOCS' || $exten == 'docs' || $exten == 'Doc' || $exten == 'xls' || $exten == 'XLS' || $exten == 'xlss' || $exten == 'xlss' || $exten == 'xlsx' || $exten == 'PDF' || $exten == 'pdf') {

                    $uploadedFile->move($this->PathControll->saveDocument(1), $fileName);
                    $filepath = $this->PathControll->saveDocument(1) . $fileName;

                } elseif ($exten == 'jpg' || $exten == 'png' || $exten == 'JPG' || $exten == 'PNG' || $exten == 'jpeg' || $exten == 'JPEG') {
                    $filepath = $this->uploadimage($uploadedFile);
                } elseif ($exten == 'mp4' || $exten == 'MP4' || $exten == 'flv' || $exten == 'MPEG' || $exten == 'mpeg') {
                    $uploadedFile->move($this->PathControll->saveDocument(3), $fileName);
                    $filepath = $this->PathControll->saveDocument(3) . $fileName;
                }
            } catch (Exception $e) {
                 return $e->getMessage();
//                return '';
            }
            return $filepath;


    }

    public function uploadimage($file){
        $uploadedFile = $file;
        $filepath   ='';
        if($uploadedFile!==null){
            if(!empty($uploadedFile)){
                try{
                    $tempimg = Image::make($uploadedFile);
                    $width = $tempimg->getWidth();
                    $height = $tempimg->getHeight();

                    if($width > $height){
                        $tempimg->resize(500, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }else{
                        $tempimg->resize(null, 500, function ($constraint) {
                            $constraint->aspectRatio();
                        });

                    }


                    $fileName = time().rand(1, 99999).'.'.pathinfo($uploadedFile->getClientOriginalName(),PATHINFO_EXTENSION);
                    $tempimg->save($this->PathControll->saveDocument(2).$fileName);
                    $filepath = $this->PathControll->saveDocument(2) . $fileName;
                }catch (\Exception $e){
                    dump($e->getMessage());
                }

            }
        }

        return $filepath;


    }
}
