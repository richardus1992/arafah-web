<?php

namespace App\Http\Controllers\Utils;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ViewControll extends Controller
{

    public function genAction($modul,$id)
    {
      return $this->linkButton($modul.'/'.$id,'btn-primary','glyphicon-eye-open','Show','')
        .$this->linkButton($modul.'/'.$id.'/edit','btn-info','glyphicon-edit','Edit','')
        .$this->linkButton($modul.'/'.$id.'/delete','btn-danger','glyphicon-trash','Delete',
                            'onclick="javascript:return confirm(\'Yakin ingin menghapus data?\')"');
    }

    public function genActionDonwload($url,$class,$icon,$caption){
        return '<a href="'.$url.'" class="btn btn-xs '.$class.'" target="_blank">
                <i class="glyphicon '.$icon.'"></i>'.$caption.'</a> ';
    }

    public function genActionShowEdit($modul,$id)
    {
        return $this->linkButton($modul.'/'.$id,'btn-primary','glyphicon-eye-open','Show','')
        .$this->linkButton($modul.'/'.$id.'/edit','btn-info','glyphicon-edit','Edit','');

    }


    public function genActionShowReplay($modul,$id)
    {
        return $this->linkButton($modul.'/'.$id,'btn-primary','glyphicon-eye-open','Lihat','')
            .$this->linkButton($modul.'/'.$id.'/edit','btn-success','glyphicon-edit','Balas','');

    }
    public function genActionEdit($modul,$id)
    {
        return $this->linkButton($modul.'/'.$id.'/edit','btn-info','glyphicon-edit','Edit','');

    }

    public function genActionDelete($modul,$id)
    {
        return $this->linkButton($modul.'/'.$id.'/delete','btn-danger','glyphicon-trash','Delete',
            'onclick="javascript:return confirm(\'Yakin ingin menghapus data?\')"');

    }

    public function genActionConfirmation($modul,$id)
    {
        return $this->linkButton($modul.$id,'btn-info','glyphicon-edit','Confirmasi','');

    }

    public function genActionEditDel($modul,$id)
    {
        return $this->linkButton($modul.'/'.$id.'/edit','btn-info','glyphicon-edit','Edit','')
        .$this->linkButton($modul.'/'.$id.'/delete','btn-danger','glyphicon-trash','Delete',
            'onclick="javascript:return confirm(\'Yakin ingin menghapus data?\')"');
    }

    public function genActionShow($modul,$id){
        return $this->linkButton(''.$modul.'/'.$id,'btn-primary','glyphicon-eye-open','Show','');
    }

    public function genActionShowAndPaidOff($modul,$id){
        return $this->linkButton($modul.'/'.$id,'btn-primary','glyphicon-eye-open','Show','')
        .$this->linkButton($modul.'/'.$id.'/status','btn-success','glyphicon glyphicon-ok','Edit','');
    }

    public function genActionPrint($url,$class,$icon,$caption){

        return '<button class="btn btn-success '.$class.'" url="'.$url.'">
                    <i class="fa '.$icon.'" aria-hidden="true"></i> '.$caption.'</button>';
    }

    public function genActionSwitch($modul,$status){

        return ' <label class="switch switch-sm text-center" ><input class="check-data" type="checkbox" '.$status.' idaction="'.$modul.'" statusview="'.$status.'"/><i data-switch-off="OFF" data-switch-on="ON" ></i></label>';
    }




    public function addButton($url,$class,$icon,$caption)
    {
        return '<button class="btn btn-xs '.$class.'" url="'.$url.'">
                    <i class="fa '.$icon.'" aria-hidden="true"></i> '.$caption.'</button>';
    }

    public function linkButton($url,$class,$icon,$caption,$js)
    {
        return '<a style=margin:5px; href="#HomeAdmin/'.$url.'" class="btn btn-xs btn-outline '.$class.'" '.$js.'>
                <i class="glyphicon '.$icon.'"></i>'.$caption.'</a> ';
    }

    public function linkButtonUnpublish($url,$class,$icon,$caption,$js,$urlunpublish)
    {
        return '<a href="#'.$url.'" class="btn btn-xs '.$class.'" '.$js.'>
                <i class="glyphicon '.$icon.'"></i>'.$caption.'</a> '.
            '<a href="#'.$urlunpublish.'" class="btn btn-xs btn-success" '.$js.'>
                <i class="glyphicon '.$icon.'"></i>Unpublish</a> ';

    }

    public function linkButtonActived($caption){
        return '<span class='.'"label label-success"'.'>'.$caption.'</span>';
    }

    public function linkButtonVerified($caption){
        return '<span class='.'"label label-warning"'.'>'.$caption.'</span>';
    }

    public function linkButtonNotVerified($caption){
        return '<span class='.'"label label-danger"'.'>'.$caption.'</span>';
    }

    public function linkButtonInfo($caption){
        return '<span class='.'"label label-info"'.'>'.$caption.'</span>';
    }

    public function linkText($url,$icon,$caption)
    {
        return '<a href="#HomeAdmin/'.$url.'">
                <i class="glyphicon '.$icon.'"></i>'.$caption.'</a> ';
    }

    public function viewPersen($valnow,$valmax,$valpersen)
    {
        return '<div class='.'"progress progress-xs"'.'>
                <div class='.'"progress-bar progress-bar-striped progress-bar-warning   
                active"'.' role='.'"progressbar"'.' aria-valuenow='.'"'.$valnow.'"'.' 
                aria-valuemin="0" aria-valuemax='.'"'.$valmax.'"'.' style="width:'.$valpersen.';">
                    </div>
                </div>';
    }

    /**
     * @param $date
     * @param $format
     * @return bool|string
     */
    public static function convertDateTime($date,$format){
      $dateTime = strtotime($date);
      $result = date($format,$dateTime);

      return $result;
    }

    public static function getNumeric($var){
        $res = preg_replace("/[^0-9,-]+/","",$var);
        $res = str_replace(",",".",$res);
        $res = floatval($res);
        return $res;
    }

    public static function MonthName($id){
        $name   ='';
        switch ($id){
            case 1:
                $name   ='JANUARI';
                break;
            case 2:
                $name   ='FEBUARI';
                break;
            case 3:
                $name   ='MARET';
                break;
            case 4:
                $name   ='APRIL';
                break;
            case 5:
                $name   ='MEI';
                break;
            case 6:
                $name   ='JUNI';
                break;
            case 7:
                $name   ='JULI';
                break;
            case 8:
                $name   ='AGUSTUS';
                break;
            case 9:
                $name   ='SEPTEMBER';
                break;
            case 10:
                $name   ='OKTOBER';
                break;
            case 11:
                $name   ='NOVEMBER';
                break;
            case 12:
                $name   ='DESEMBER';
                break;
        }

        return $name;

    }

    public static function DayName($id){
        $name   ='';
        switch ($id){
            case 1:
                $name   ='SENIN';
                break;
            case 2:
                $name   ='SELASA';
                break;
            case 3:
                $name   ='RABU';
                break;
            case 4:
                $name   ='KAMIS';
                break;
            case 5:
                $name   ='JUMAT';
                break;
            case 6:
                $name   ='SABTU';
                break;
            case 7:
                $name   ='MINGGU';
                break;
        }

        return $name;

    }

    public function arrDayName(){
        return array(
            '1'=>'SENIN',
            '2'=>'SELASA',
            '3'=>'RABU',
            '4'=>'KAMIS',
            '5'=>'JUMAT',
            '6'=>'SABTU',
            '7'=>'MINGGU'
        );
    }
}
