<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Cache;

class ApiMethodePayControll extends Controller
{
    protected $Paginate=20;
    public function index(){
        $item =DB::table('METODE_BAYAR as mtb')
            ->select(
                'mtb.Id as Id',
                'jls.NAMA_METODE as Name')
            ->paginate($this->Paginate);

        if(count($item)>0){
            return response()->json(array('MethodePay'=>$item,'message'=>'Data Tersedia','erorr'=>false));
        }
        return response()->json(array('MethodePay'=>null,'message'=>'Data Tidak Ada','erorr'=>true));
    }

    public function indexAll(){
        $item = Cache::rememberForever('methode-pay-all', function () {
            return DB::table('METODE_BAYAR as mtb')
                ->select(
                    'mtb.Id as Id',
                    'mtb.NAMA_METODE as Name')
                ->get();
        });

        if(count($item)>0){
            return response()->json(array('MethodePay'=>$item,'message'=>'Data Tersedia','erorr'=>false));
        }
        return response()->json(array('MethodePay'=>null,'message'=>'Data Tidak Ada','erorr'=>true));
    }
}
