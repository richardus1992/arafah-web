<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BackEnd\HistoryControll;
use App\Http\Controllers\Utils\ViewControll;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;

class ApiHistoryCheckControll extends Controller
{
    protected $History;
    protected $ActElement;
    protected $Paginate=20;

    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();

    }

    public function index($id,Request $request){
        $search =json_decode($request->get('search'));
        if(!empty($search)){
            $item =DB::table('REKAM_MEDIK_PASIEN as rmp')
                ->select(
                    'ply.NAMA_LAYANAN_RS as RoomService',
                    'rmp.ID_RM_PASIEN as IdRm',
                    'rmp.TGL_PERMINTAAN_RM as DateChek',
                    'rmp.TGL_KELUAR_RM as DateOutCheck',
                    'rmp.STATUS_SELESAI_RM_PASIEN as StatusFinsihCheck',
                    'rmp.STATUS_RM as StatusCheck'
                )
                ->join('JENIS_LAYANAN_RS as ply','ply.ID_LAYANAN_RS','rmp.ID_LAYANAN_RS')
                ->where('rmp.ID_PASIEN',$id)
                ->where(function ($q)use ($search) {
                    switch ($search->id){
                        case 1:
                            $q->where('rmp.TGL_PERMINTAAN',$this->formatDate($search->value, 'Y-m-d'));
                            break;

                        case 2:
                            $q->where('ply.NAMA_LAYANAN_RS','LIKE', '%'.$search->value.'%');
                            break;

                    }
                })
                ->orderBy('rmp.TGL_PERMINTAAN_RM',$search->id==3?$search->value:'desc')
                ->paginate($this->Paginate);

        }else{
            $item =DB::table('REKAM_MEDIK_PASIEN as rmp')
                ->select(
                    'ply.NAMA_LAYANAN_RS as RoomService',
                    'rmp.ID_RM_PASIEN as IdRm',
                    'rmp.TGL_PERMINTAAN_RM as DateChek',
                    'rmp.TGL_KELUAR_RM as DateOutCheck',
                    'rmp.STATUS_SELESAI_RM_PASIEN as StatusFinsihCheck',
                    'rmp.STATUS_RM as StatusCheck'
                )
                ->join('JENIS_LAYANAN_RS as ply','ply.ID_LAYANAN_RS','rmp.ID_LAYANAN_RS')
                ->where('rmp.ID_PASIEN',$id)
                ->orderBy('rmp.ID_RM_PASIEN','desc')
                ->paginate($this->Paginate);

        }

        if(count($item)>0){
            foreach ($item as $val){
                $val->DateChek =Carbon::parse($val->DateChek)->format('d-m-Y');
                $val->DateOutCheck =$val->DateOutCheck==null?'':Carbon::parse($val->DateChek)->format('d-m-Y');

            }
            return response()->json(array('HistoryCheck'=>$item,'message'=>'Data Tersedia ','erorr'=>false));
        }
        return response()->json(array('HistoryCheck'=>null,'message'=>'Data Tidak Ada','erorr'=>true));
    }


    public function indexDetail($id,Request $request){
        $search =$request->get('searc');
        $item =DB::table('DETIL_REKAM_MEDIK as drm')
            ->select(
                'drm.ID_DIAGNOSIS_RM as IdDiagnosa',
                'drm.ID_RM_PASIEN as IdRm',
                'drm.DIAGNOSIS_AWAL as DiagnosaFirst',
                'drm.TENSI as Tensi',
                'drm.NADI as Nadi',
                'drm.BB as Bb',
                'drm.PEMERIKSAAN_LAIN as CheckOther',
                'drm.TERAPI as Trapi',
                'mcd.NAMA_ICD10 as NameMcd',
                'mcd.KODE_ICD10 as CodeIcd',
                'mcd.NAMA_ALIAS as NameAlias'
            )
            ->leftjoin('MASTER_ICD10 as mcd','mcd.ID_ICD10','drm.DIAGNOSIS_AWAL')
            ->where('drm.ID_RM_PASIEN',$id)
            ->get();

        if(count($item)>0){
           foreach ($item as $val){
               $val->DiagnosaFirst=$val->DiagnosaFirst==null ?'':$val->DiagnosaFirst;
               $val->NameMcd=$val->NameMcd==null ?'':$val->NameMcd;
               $val->CodeIcd=$val->CodeIcd==null ?'':$val->CodeIcd;
               $val->NameAlias=$val->NameAlias==null ?'':$val->NameAlias;
           }
            return response()->json(array('HistoryCheckDetail'=>$item,'message'=>'Data Tersedia','erorr'=>false));
        }
        return response()->json(array('HistoryCheckDetail'=>null,'message'=>'Data Tidak Ada','erorr'=>true));
    }

    public function store(Request $request){
        $requestData    =$request->all();
        DB::begintransaction();
        try{
            DB::table('PENDAFTARAN_ONLINE')
                ->insert([
                    'ID_USER'=>$request->input('Id'),
                    'NAMA_PASIEN'=>$request->input('Name'),
                    'NIK'=>$request->input('Nik'),
                    'TGLLAHIR_PASIEN'=>$this->formatDate($request->input('DateBirth'),'Y-m-d'),
                    'ALAMAT_PASIEN'=>$request->input('Address'),
                    'KOTA_PASIEN'=>$request->input('IdCity'),
                    'PROVINSI_PASIEN'=>$request->input('IdProvince'),
                    'KECAMATAN_PASIEN'=>$request->input('IdDistricts'),
                    'JK_PASIEN'=>$request->input('Sex'),
                    'AGAMA'=>$request->input('Religion'),
                    'ID_LAYANAN_RS'=>$request->input('IdPoli'),
                    'ID_PGW'=>$request->input('IdDoctor'),
                    'TGL_PELAYANAN'=>$this->formatDate($request->input('DateService'),'Y-m-d'),
                    'NO_HP'=>$request->input('Phone'),
                    'CARA_BAYAR'=>$request->input('MethodePay'),
                    'EMAIL'=>$request->input('Email'),
                    'CODE' => Uuid::uuid1()->toString(),
                    'CREATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s')

                ]);

            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return response()->json(array('RegisterOnline'=>null,'message'=>$e->getMessage(),'error'=>true));

        }


        return response()->json(array('RegisterOnline'=>null,'message'=>'Data Tersedia','error'=>false));

    }
    function formatDate($date, $format) {
        return Carbon::createFromFormat('d/m/Y', $date)->format($format);
    }
}
