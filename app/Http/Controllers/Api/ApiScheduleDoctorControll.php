<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Utils\ViewControll;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Cache;

class ApiScheduleDoctorControll extends Controller{
    protected $Paginate=30;
    public function index(Request $request){
        $search =json_decode($request->get('search'));
        if(!empty($search)){
            $item =DB::table('MASTER_JADWAL as msj')
                ->select(
                    'jls.ID_LAYANAN_RS as Id',
                    'jls.NAMA_LAYANAN_RS as Name'
                )
                ->join('JENIS_LAYANAN_RS as jls','msj.ID_LAYANAN_RS','jls.ID_LAYANAN_RS')
                ->where('msj.flag_tampil',1)
                ->where(function ($q)use ($search) {
                    switch ($search->id){
                        case 1:
                            $q->where('jls.NAMA_LAYANAN_RS','LIKE', '%'.$search->value.'%');
                            break;

                    }
                })
                ->groupBy('jls.ID_LAYANAN_RS')
                ->groupBy('jls.NAMA_LAYANAN_RS')
                ->orderBy('jls.ID_LAYANAN_RS',$search->id==2?$search->value:'desc')
                ->paginate($this->Paginate);
        }else{
            $item =DB::table('MASTER_JADWAL as msj')
                ->select(
                    'jls.ID_LAYANAN_RS as Id',
                    'jls.NAMA_LAYANAN_RS as Name'
                )
                ->join('JENIS_LAYANAN_RS as jls','msj.ID_LAYANAN_RS','jls.ID_LAYANAN_RS')
                ->where('msj.flag_tampil',1)
                ->groupBy('jls.ID_LAYANAN_RS')
                ->groupBy('jls.NAMA_LAYANAN_RS')
                ->paginate($this->Paginate);
        }

        if(count($item)>0){
            return response()->json(array('ScheduleDoctor'=>$item,'message'=>'Data Tersedia','erorr'=>false));
        }
        return response()->json(array('ScheduleDoctor'=>null,'message'=>'Data Tidak Ada '.$search->value,'erorr'=>true));
    }

    public function indexDetail($id){
        $item =DB::table('MASTER_JADWAL as msj')
            ->select(
                'msj.ID_JADWAL as Id',
                'msj.HARI_JADWAL as DayName',
                'msj.JAM_MULAI_JADWAL as DateSchedule',
                'msj.JAM_MULAI_JADWAL as TimeStart',
                'msj.JAM_BERAKHIR_JADWAL as TimeEnd',
                'jlr.NAMA_LAYANAN_RS as RoomService',
                'pgw.NAMA_PGW as EmployeName',
                'mpi.NAMA_M_PROFESI as Profesi'
            )
            ->join('PEGAWAI as pgw','pgw.ID_PGW','msj.ID_PGW')
            ->join('JENIS_LAYANAN_RS as jlr','jlr.ID_LAYANAN_RS','msj.ID_LAYANAN_RS')
            ->join('MASTER_PROFESI as mpi','mpi.ID_M_PROFESI','pgw.ID_PROFESI')
            ->wheredate('msj.JAM_MULAI_JADWAL',Carbon::parse($id)->format('Y-m-d'))
            ->get();
        if(count($item)>0){
            foreach ($item as $va){
                $va->TimeStart  =Carbon::parse($va->TimeStart)->format('H:i');
                $va->TimeEnd  =Carbon::parse($va->TimeEnd)->format('H:i');
            }
        }
        return $item;
    }

    public function indexDetailAll(Request $request){
//        Cache::flush();
//        return Carbon::parse($date)->format('Y-m-d');
        $date=$request->get("date");
        $item = Cache::rememberForever('doctor-detail-all-'.$date, function () use ($date){
            return DB::table('MASTER_JADWAL as msj')
                ->select(
                    'msj.ID_JADWAL as Id',
                    'msj.HARI_JADWAL as DayName',
                    'msj.JAM_MULAI_JADWAL as DateSchedule',
                    'msj.JAM_MULAI_JADWAL as TimeStart',
                    'msj.JAM_BERAKHIR_JADWAL as TimeEnd',
                    'jlr.NAMA_LAYANAN_RS as RoomService',
                    'pgw.NAMA_PGW as EmployeName',
                    'pgw.ID_PGW as EmpId',
                    'mpi.NAMA_M_PROFESI as Profesi'
                )
                ->join('PEGAWAI as pgw','pgw.ID_PGW','msj.ID_PGW')
                ->join('JENIS_LAYANAN_RS as jlr','jlr.ID_LAYANAN_RS','msj.ID_LAYANAN_RS')
                ->join('MASTER_PROFESI as mpi','mpi.ID_M_PROFESI','pgw.ID_PROFESI')
//                ->wheredate('msj.JAM_MULAI_JADWAL',Carbon::parse($date)->format('Y-m-d'))
                ->get();
        });
        if(count($item)>0){
            Cache::flush();
            foreach ($item as $va){
                $va->TimeStart  =Carbon::parse($va->TimeStart)->format('H:i');
                $va->TimeEnd  =Carbon::parse($va->TimeEnd)->format('H:i');
            }
            return response()->json(array('ScheduleDoctorDetail'=>$item,'message'=>'Data Tersedia','erorr'=>false));
        }
        return response()->json(array('ScheduleDoctorDetail'=>null,'message'=>'Data Tidak Ada','erorr'=>true));
    }
    public function indexDetailRegister(Request $request){
//        Cache::flush();
//        return Carbon::parse($date)->format('Y-m-d');
        $date=$this->formatDate($request->get("date"),'Y-m-d');
        $today=Carbon::parse($date)->dayOfWeek;
        $idpoly=$request->get("poly");
        $item = DB::table('MASTER_JADWAL as msj')
            ->select(
                'msj.ID_JADWAL as Id',
                'msj.HARI_JADWAL as DayName',
                'msj.JAM_MULAI_JADWAL as DateSchedule',
                'msj.JAM_MULAI_JADWAL as TimeStart',
                'msj.JAM_BERAKHIR_JADWAL as TimeEnd',
                'jlr.NAMA_LAYANAN_RS as RoomService',
                'pgw.NAMA_PGW as EmployeName',
                'pgw.ID_PGW as EmpId',
                'mpi.NAMA_M_PROFESI as Profesi'

            )
            ->join('PEGAWAI as pgw','pgw.ID_PGW','msj.ID_PGW')
            ->join('JENIS_LAYANAN_RS as jlr','jlr.ID_LAYANAN_RS','msj.ID_LAYANAN_RS')
            ->join('MASTER_PROFESI as mpi','mpi.ID_M_PROFESI','pgw.ID_PROFESI')
            ->where('msj.ID_LAYANAN_RS',$idpoly)
            ->where('msj.flag_tampil',1)
            ->where('msj.HARI_JADWAL',$today)
            ->get();
        if(count($item)>0){
            foreach ($item as $va){
                $va->TimeStart  =ViewControll::DayName($today).', '.Carbon::parse($va->TimeStart)->format('H:i');
                $va->TimeEnd  =Carbon::parse($va->TimeEnd)->format('H:i');
            }
            return response()->json(array('ScheduleDoctorDetail'=>$item,'message'=>'Data Tersedia ','erorr'=>false));
        }
        return response()->json(array('ScheduleDoctorDetail'=>null,'message'=>'Data Tidak Ada ','erorr'=>true));
    }
    function formatDate($date, $format){
        return Carbon::createFromFormat('d/m/Y', $date)->format($format);
    }
}
