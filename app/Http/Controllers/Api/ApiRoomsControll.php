<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ApiRoomsControll extends Controller{

    protected $Paginate=20;

    public function indexOld(Request $request){
        $search =json_decode($request->get('search'));

        if(!empty($search)){
            $item=DB::table('MASTER_KAMAR_RAWAT_INAP as mkri')
                ->select(
                    'mkri.ID_KAMAR_RWT_INAP as Id',
                    'mkri.ID_KELAS_RINAP as IdClass',
                    'mkri.NAMA_ALIAS_RWT_INAP as Name',
                    'mkri.NAMA_ALIAS_RWT_INAP as Info',
                    'mkri.LANTAI_RWT_INAP as FloorLevel',
                    'mkri.KAPASITAS_RWT_INAP as Capacity',
                    'mkri.KAPASITAS_RWT_INAP as Avalaible',
                    'mkrp.NAMA_KELAS_RINAP as NameClass',
                    'mpv.NAMA_PAVILIUN as NamePaviliun',
                    'mpv.NAMA_PAVILIUN as Facilities'
                )
                ->join('MASTER_KELAS_RAWAT_INAP as mkrp','mkrp.ID_KELAS_RINAP','mkri.ID_KELAS_RINAP')
                ->join('MASTER_PAVILIUN as mpv','mpv.ID_PAVILIUN','mkri.ID_PAVILIUN')
                ->where(function ($q)use ($search) {
                    switch ($search->id){
                        case 1:
                            $q->where('mkri.NAMA_ALIAS_RWT_INAP','LIKE', '%'.$search->value.'%');
                            break;

                    }
                })
                ->orderBy('mkri.ID_KAMAR_RWT_INAP',$search->id==2?$search->value:'desc')
                ->paginate($this->Paginate);
        }else{
            $item=DB::table('MASTER_KAMAR_RAWAT_INAP as mkri')
                ->select(
                    'mkri.ID_KAMAR_RWT_INAP as Id',
                    'mkri.ID_KELAS_RINAP as IdClass',
                    'mkri.NAMA_ALIAS_RWT_INAP as Name',
                    'mkri.NAMA_ALIAS_RWT_INAP as Info',
                    'mkri.LANTAI_RWT_INAP as FloorLevel',
                    'mkri.KAPASITAS_RWT_INAP as Capacity',
                    'mkri.KAPASITAS_RWT_INAP as Avalaible',
                    'mkrp.NAMA_KELAS_RINAP as NameClass',
                    'mpv.NAMA_PAVILIUN as NamePaviliun',
                    'mpv.NAMA_PAVILIUN as Facilities'
                )
                ->join('MASTER_KELAS_RAWAT_INAP as mkrp','mkrp.ID_KELAS_RINAP','mkri.ID_KELAS_RINAP')
                ->join('MASTER_PAVILIUN as mpv','mpv.ID_PAVILIUN','mkri.ID_PAVILIUN')
                ->paginate($this->Paginate);
        }


        if(count($item)>0){

            foreach ($item as $val){
                $val->Info  = "<b>Lantai </b>".$val->FloorLevel." <br> Kelas </b>".$val->NameClass;
                $val->Capacity  = "<b>Kapasitas </b>".$val->Capacity;
                $val->Avalaible  = "<b>Tersedia </b>".$val->Avalaible;
                $val->Facilities    =$this->indexFacilities($val->Id);
            }

            return response()->json(array('Rooms'=>$item,'message'=>'Data Tersedia','erorr'=>false));
        }
        return response()->json(array('Rooms'=>null,'message'=>'Data Tidak Ada','erorr'=>true));
    }

    function indexFacilities($id){
        $item=DB::table('FASILISTAS_KAMAR_RWTINAP as fkr')
            ->select(
                'fkr.ID_FASILISTAS as Id',
                'fkr.FASILISTAS as Name',
                'fkr.JUMLAH as Qty',
                'fkr.KETERANGAN as Description'
            )
            ->join('MASTER_KAMAR_RAWAT_INAP as mkr','mkr.ID_KAMAR_RWT_INAP','fkr.ID_KAMAR_RWT_INAP')
//            ->where('fkr.ID_KAMAR_RWT_INAP',$id)
            ->where('mkr.nama_kamar',$id)
            ->get();
        
        
       return $item;
    }

    public function index(){
        $item=DB::select('select m.nama_kamar, count(m.ID_KAMAR_RWT_INAP) as bed
                            from Master_kamar_rawat_inap m
                            where status_mod = 0
                            and m.ID_KAMAR_RWT_INAP not in (select ID_KAMAR from PEMAKAIAN_KAMAR where status_pakai_kamar = 1 and tgl_selesai_pakai_kamar is null)
                            group by m.nama_kamar
                            
                            union all
                            
                            select t.nama_kamar,case 
                            when st.status = 0 then bed
                            when st.status = 1 then 0
                            end as bed_
                            from (select m.nama_kamar, count(m.ID_KAMAR_RWT_INAP) as bed
                            from Master_kamar_rawat_inap m 
                            where status_mod = 1 and m.ID_KELAS_RINAP != 2
                            and m.ID_KAMAR_RWT_INAP not in (select ID_KAMAR from PEMAKAIAN_KAMAR where status_pakai_kamar = 1 and tgl_selesai_pakai_kamar is null)
                            group by m.nama_kamar) t join (select m.nama_kamar, isnull(status_pakai_kamar,0) status
                            from Master_kamar_rawat_inap m 
                            left join pemakaian_kamar pk on  m.id_kamar_rwt_inap = pk.id_kamar and status_pakai_kamar = 1 
                            and tgl_selesai_pakai_kamar is null
                            where status_mod = 1 and ID_KELAS_RINAP = 2) st on t.nama_kamar = st.nama_kamar');

        if(count($item)>0){
            foreach ($item as $val){
                $val->Facilities    =$this->indexFacilities($val->nama_kamar);
                $val->Name    =$val->nama_kamar;
                $val->CountBed    =$val->bed * 1;
                if($val->CountBed >0){
                    $val->StatusBed='Tersedia';
                }else{
                    $val->StatusBed='Tida Tersedia';
                }
            }
            return response()->json(array('Rooms'=>$item,'message'=>'Data Tersedia','erorr'=>false));
        }else{
            return response()->json(array('Rooms'=>null,'message'=>'Data Tidak Ada','erorr'=>true));
        }

    }

}
