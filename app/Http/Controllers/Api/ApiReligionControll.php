<?php

namespace App\Http\Controllers\Api;

use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Cache;

class ApiReligionControll extends Controller
{
    protected $Paginate=20;
    public function indexAll(){
        $item = Cache::rememberForever('location-province', function () {
            return DB::table('KEPENDUDUKAN as pvc')
                ->select(
                    'pvc.ID_KEPENDUDUKAN as Id',
                    'pvc.NAMA_KEPENDUDUKAN as Name',
                    'pvc.ATASAN_KEPENDUDUKAN as Parent',
                    'pvc.JENIS_KEPENDUDUKAN as Type',
                    'pvc.ATASAN_KEPENDUDUKAN'
                )
                ->where('pvc.ATASAN_KEPENDUDUKAN',null)
                ->get();
        });

        if(count($item)>0){
            return response()->json(array('Religion'=>$item,'message'=>'Data Tersedia','erorr'=>false));
        }
        return response()->json(array('Religion'=>null,'message'=>'Data Tidak Ada','erorr'=>true));
    }


}
