<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BackEnd\HistoryControll;
use App\Http\Controllers\Utils\UploadFileControll;
use App\Http\Controllers\Utils\ViewControll;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Ramsey\Uuid\Uuid;

class ApiRegisterQueueControll extends Controller
{
    protected $History;
    protected $ActElement;
    protected $UploadFile;


    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();
        $this->UploadFile = new UploadFileControll();


    }

    public function store(Request $request){
        $requestData    =$request->all();

        $filepath='';
        if($request->has('FileAttachment')){
            $filepath  =$this->UploadFile->uploadFile($request->file('FileAttachment'));
        }
        DB::begintransaction();
        try{
            $id=DB::table('PENDAFTARAN_ONLINE')
                ->insertGetId([
                    'ID_USER'=>$request->input('Id'),
                    'NAMA_PASIEN'=>$request->input('Name'),
                    'NIK'=>$request->input('Nik'),
                    'TGLLAHIR_PASIEN'=>$this->formatDate($request->input('DateBirth'),'Y-m-d'),
                    'ALAMAT_PASIEN'=>$request->input('Address'),
                    'KOTA_PASIEN'=>$request->input('IdCity'),
                    'PROVINSI_PASIEN'=>$request->input('IdProvince'),
                    'KECAMATAN_PASIEN'=>$request->input('IdDistricts'),
                    'JK_PASIEN'=>$request->input('Sex'),
                    'AGAMA'=>$request->input('Religion'),
                    'ID_LAYANAN_RS'=>$request->input('IdPoli'),
                    'ID_PGW'=>$request->input('IdDoctor'),
                    'TGL_PELAYANAN'=>$this->formatDate($request->input('DateService'),'Y-m-d'),
                    'NO_HP'=>$request->input('Phone'),
                    'CARA_BAYAR'=>$request->input('MethodePay'),
                    'EMAIL'=>$request->input('Email'),
                    'LAMPIRAN_GAMBAR'=>$filepath,
                    'CODE' => Uuid::uuid1()->toString(),
                    'CREATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s')

                ]);

            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return response()->json(array('RegisterOnline'=>null,'message'=>$e->getMessage(),'error'=>true));

        }


        return response()->json(array('RegisterOnline'=>$this->readDataAfterInsert($id),'message'=>'Data Tersedia','error'=>false));

    }
    function formatDate($date, $format) {
        return Carbon::createFromFormat('d/m/Y', $date)->format($format);
    }


    public function checkPatientExist(Request $request){
        $idUser         =$request->input('Id');
        $namePatient    =$request->input('Name');
        $nik            =$request->input('Nik');
        $idCity         =$request->input('IdCity');
        $idProvince     =$request->input('IdProvince');
        $idDistricts    =$request->input('IdDistricts');


        $pdo=DB::table('PENDAFTARAN_ONLINE')
            ->where('ID_USER',$idUser)
            ->where('ID_PASIEN','<>',null)
            ->first();

        if(!empty($pdo)){
            return response()->json(array('RegisterOnline'=>null,'message'=>'Anda SUdah Terdaftar Sebagai Pasien Silahkan Logout Dan Login Kembali','error'=>true));

        }

        $patient=DB::table('PASIEN as psn')
               ->where('NAMA_PASIEN',$namePatient)
               ->where('KECAMATAN_PASIEN',$idDistricts)
               ->where('KABUPATEN_PASIEN',$idCity)
               ->where('propinsi_pasien',$idProvince)
               ->first();
        if(!empty($patient)){
            return response()->json(array('RegisterOnline'=>null,'message'=>'Anda SUdah Terdaftar Sebagai Pasien','error'=>true));

        }
        return response()->json(array('RegisterOnline'=>null,'message'=>'Belum Terdaftar Sebagai Patient','error'=>false));
    }

    function readDataAfterInsert($id){
        $item =DB::table('PENDAFTARAN_ONLINE as pdo')
            ->select(
                'pdo.ID_ONLINE as Id',
                'pdo.NO_ANTRIAN as OrdinalQueue',
                'pdo.ID_PASIEN as IdPatien',
                'pdo.NAMA_PASIEN as Name',
                'pdo.NIK as Nik',
                'pdo.TGLLAHIR_PASIEN as DateBirth',
                'pdo.ALAMAT_PASIEN as Addres',
                'pdo.KOTA_PASIEN as IdCity',
                'pdo.PROVINSI_PASIEN as IdProvince',
                'pdo.JK_PASIEN as Sex',
                'pdo.AGAMA as Religion',
                'pdo.STATUS_PERKAWINAN as Married',
                'pdo.ID_LAYANAN_RS as IdService',
                'pdo.ID_PGW as IdEmployee',
                'pdo.TGL_PELAYANAN as DateService',
                'pdo.KONFIRMASI_KEDATANGAN as Confirm',
                'pdo.KETERANGAN as Remarks',
                'pdo.NO_HP as Phone',
                'mpy.NAMA_METODE as MethodePay',
                'pvc.NAMA_KEPENDUDUKAN as Province',
                'cty.NAMA_KEPENDUDUKAN as City',
                'ply.NAMA_LAYANAN_RS as RoomService',
                'emp.NAMA_PGW as DoctorVisit'
            )
            ->join('KEPENDUDUKAN as pvc','pvc.ID_KEPENDUDUKAN','pdo.KOTA_PASIEN')
            ->join('KEPENDUDUKAN as cty','cty.ID_KEPENDUDUKAN','pdo.PROVINSI_PASIEN')
            ->join('JENIS_LAYANAN_RS as ply','ply.ID_LAYANAN_RS','pdo.ID_LAYANAN_RS')
            ->join('PEGAWAI as emp','emp.ID_PGW','pdo.ID_PGW')
            ->join('METODE_BAYAR as mpy','mpy.ID','pdo.CARA_BAYAR')
            ->where('pdo.ID_ONLINE',$id)
            ->orderBy('pdo.ID_ONLINE','desc')
            ->paginate(1);
        if(count($item)>0){
            foreach ($item as $val){
                $val->DateService  =$val->DateService ? with(new Carbon($val->DateService))->format('d/m/Y') : '';
                $val->DateBirth  = $val->DateBirth ? with(new Carbon($val->DateBirth))->format('d/m/Y') : '';
            }
            return $item;
        }
    }
}
