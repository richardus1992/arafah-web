<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ApiQueueControll extends Controller
{
    protected $Paginate=20;
    public function index(Request $request,$id){
        $search =json_decode($request->get('search'));
        if(!empty($search)){
            $item =DB::table('PENDAFTARAN_ONLINE as pdo')
                ->select(
                    'pdo.ID_ONLINE as Id',
                    'pdo.NO_ANTRIAN as OrdinalQueue',
                    'pdo.ID_PASIEN as IdPatien',
                    'pdo.NAMA_PASIEN as Name',
                    'pdo.NIK as Nik',
                    'pdo.TGLLAHIR_PASIEN as DateBirth',
                    'pdo.ALAMAT_PASIEN as Addres',
                    'pdo.KOTA_PASIEN as IdCity',
                    'pdo.PROVINSI_PASIEN as IdProvince',
                    'pdo.JK_PASIEN as Sex',
                    'pdo.AGAMA as Religion',
                    'pdo.STATUS_PERKAWINAN as Married',
                    'pdo.ID_LAYANAN_RS as IdService',
                    'pdo.ID_PGW as IdEmployee',
                    'pdo.TGL_PELAYANAN as DateService',
                    'pdo.KONFIRMASI_KEDATANGAN as Confirm',
                    'pdo.KETERANGAN as Remarks',
                    'pdo.NO_HP as Phone',
                    'mpy.NAMA_METODE as MethodePay',
                    'pvc.NAMA_KEPENDUDUKAN as Province',
                    'cty.NAMA_KEPENDUDUKAN as City',
                    'ply.NAMA_LAYANAN_RS as RoomService',
                    'emp.NAMA_PGW as DoctorVisit'
                )
                ->join('KEPENDUDUKAN as pvc','pvc.ID_KEPENDUDUKAN','pdo.KOTA_PASIEN')
                ->join('KEPENDUDUKAN as cty','cty.ID_KEPENDUDUKAN','pdo.PROVINSI_PASIEN')
                ->join('JENIS_LAYANAN_RS as ply','ply.ID_LAYANAN_RS','pdo.ID_LAYANAN_RS')
                ->join('PEGAWAI as emp','emp.ID_PGW','pdo.ID_PGW')
                ->join('METODE_BAYAR as mpy','mpy.ID','pdo.CARA_BAYAR')
                ->where('pdo.ID_USER',$id)
                ->where(function ($q)use ($search) {
                    switch ($search->id){
                        case 1:
                            $q->where('pdo.TGL_PELAYANAN',$this->formatDate($search->value, 'Y-m-d'));
                            break;

                        case 2:
                            $q->where('pdo.ID_ONLINE', $search->value);
                            break;

                    }
                })
                ->orderBy('pdo.ID_ONLINE',$search->id==3?$search->value:'desc')
                ->paginate($this->Paginate);
        }else{
            $item =DB::table('PENDAFTARAN_ONLINE as pdo')
                ->select(
                    'pdo.ID_ONLINE as Id',
                    'pdo.NO_ANTRIAN as OrdinalQueue',
                    'pdo.ID_PASIEN as IdPatien',
                    'pdo.NAMA_PASIEN as Name',
                    'pdo.NIK as Nik',
                    'pdo.TGLLAHIR_PASIEN as DateBirth',
                    'pdo.ALAMAT_PASIEN as Addres',
                    'pdo.KOTA_PASIEN as IdCity',
                    'pdo.PROVINSI_PASIEN as IdProvince',
                    'pdo.JK_PASIEN as Sex',
                    'pdo.AGAMA as Religion',
                    'pdo.STATUS_PERKAWINAN as Married',
                    'pdo.ID_LAYANAN_RS as IdService',
                    'pdo.ID_PGW as IdEmployee',
                    'pdo.TGL_PELAYANAN as DateService',
                    'pdo.KONFIRMASI_KEDATANGAN as Confirm',
                    'pdo.KETERANGAN as Remarks',
                    'pdo.NO_HP as Phone',
                    'mpy.NAMA_METODE as MethodePay',
                    'pvc.NAMA_KEPENDUDUKAN as Province',
                    'cty.NAMA_KEPENDUDUKAN as City',
                    'ply.NAMA_LAYANAN_RS as RoomService',
                    'emp.NAMA_PGW as DoctorVisit'
                )
                ->join('KEPENDUDUKAN as pvc','pvc.ID_KEPENDUDUKAN','pdo.KOTA_PASIEN')
                ->join('KEPENDUDUKAN as cty','cty.ID_KEPENDUDUKAN','pdo.PROVINSI_PASIEN')
                ->join('JENIS_LAYANAN_RS as ply','ply.ID_LAYANAN_RS','pdo.ID_LAYANAN_RS')
                ->join('PEGAWAI as emp','emp.ID_PGW','pdo.ID_PGW')
                ->join('METODE_BAYAR as mpy','mpy.ID','pdo.CARA_BAYAR')
                ->where('pdo.ID_USER',$id)
                ->orderBy('pdo.ID_ONLINE','desc')
                ->paginate($this->Paginate);
        }


        if(count($item)>0){
            foreach ($item as $val){
                $val->Id=$val->Id;
                $val->DateService  =$val->DateService ? with(new Carbon($val->DateService))->format('l, d/m/Y') : '';
                $val->DateBirth  = $val->DateBirth ? with(new Carbon($val->DateBirth))->format('d/m/Y') : '';
            }
            return response()->json(array('Queue'=>$item,'message'=>'Data Tersedia ','erorr'=>false));
        }
        return response()->json(array('Queue'=>null,'message'=>'Data Tidak Ada','erorr'=>true));
    }
    function formatDate($date, $format)
    {
        return Carbon::createFromFormat('d/m/Y', $date)->format($format);
    }
}
