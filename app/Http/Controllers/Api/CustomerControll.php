<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Crypt;
use Ramsey\Uuid\Uuid;
use Cache;

class CustomerControll extends Controller
{
    public function index(Request $request){
        $item=DB::table('CUSTOMER as csr')
            ->select(
                'csr.*',
                'csl.USERNAME',
                'csl.PASSWORD',
                'csl.api_token as ApiToken'
            )
            ->join('CUSTOMER_LOGIN as csl','csl.IDCUSTOMER','csr.ID')
            ->where('csr.CODE',$request->get('Code'))
            ->first();

        if(!empty($item)){
            return response()->json(array('Customer'=>$item,'message'=>'Data Tersedia','erorr'=>false));
        }
        return response()->json(array('Customer'=>null,'message'=>'Data Tidak Ada','erorr'=>true));
    }

    public function registerDefault(Request $request){

        $idCustomer=0;
        if($request->get('Code')=='ARAFAH1002'){
            DB::begintransaction();
            try {

                $idCustomer=DB::table('PENDAFTAR_LOGIN')
                    ->insertGetId([
                        'ID_USER'=>0,
                        'USERNAME'=>Uuid::uuid1()->toString(),
                        'PASSWORD'=>Crypt::encrypt(str_random(5)),
                        'api_token'=>Crypt::encrypt(str_random(5)),
                    ]);

                DB::commit();
            } catch (Exception $e) {
                DB::rollback();
                return response()->json(array('CustomerLogin'=>null,'message'=>'Gagal Menyimpan ','error'=>true));

            }
        }else{
            return response()->json(array('CustomerLogin'=>null,'message'=>'Gagal Menyimpan ','error'=>true));
        }

        return response()->json(array('CustomerLogin'=>$this->getAfterInsertDefaul($idCustomer),'message'=>'Gagal Menyimpan ','error'=>false));
    }

    public function register(Request $request){
        $IdUser=0;
        $checkUserName=DB::table('PENDAFTAR_LOGIN')
            ->where('USERNAME',$request->input('UserName'))
            ->first();

        if(!empty($checkUserName)){
            return response()->json(array('CustomerLogin'=>null,'message'=>'UserName Sudah Ada','error'=>true));
        }

        $checkMail=DB::table('PENDAFTAR_LOGIN')
            ->where('EMAIL',$request->input('Email'))
            ->first();
        if(!empty($checkMail)){
            return response()->json(array('CustomerLogin'=>null,'message'=>'Email Sudah Ada','error'=>true));
        }

        if($request->input('UserName'))
        DB::begintransaction();
        try {
            $IdUser=Carbon::parse(Carbon::now())->format('YmdHis');
            DB::table('PENDAFTAR_LOGIN')
                ->insert([
                    'ID_USER'=>$IdUser,
                    'USERNAME'=>$request->input('UserName'),
                    'NAMA'=>$request->input('Name'),
                    'EMAIL'=>$request->input('Email'),
                    'NO_HP'=>$request->input('Phone'),
                    'PASSWORD'=>Crypt::encrypt($request->input('Password')),
                    'api_token'=>Crypt::encrypt($request->input('Password')),
                ]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(array('CustomerLogin'=>null,'message'=>'Gagal Menyimpan ','error'=>true));

        }

        return response()->json(array('CustomerLogin'=>$this->getAfterRegister($IdUser),'message'=>'Berhasil Terdaftar','error'=>false));
    }

    public function login(Request $request){
        $username   = $request->input('UserName');
        $password   = $request->input('Password');
        DB::begintransaction();
        $user=DB::table('PENDAFTARAN_ONLINE as csr')
            ->select(
                'csr.ID_USER as Id',
                'csr.NAMA_PASIEN as Name',
                'csr.NO_HP as Phone',
                'csr.NIK as Nik',
                'csr.EMAIL as Email',
                'csr.CODE as Code',
                'csl.USERNAME as UserName',
                'csl.PASSWORD as Password',
                'csl.api_token as ApiToken'
            )
            ->join('PENDAFTAR_LOGIN as csl','csl.ID_USER','csr.ID_USER')
            ->where('csl.USERNAME',$username)
            ->first();
        if(!empty($user)){
            if (Crypt::decrypt($user->Password) == $password) {

                return response()->json(array('CustomerLogin' => $this->getAfterLogin($user->Id), 'message' => 'Berhasil Login', 'error' => false));
            }
        }
        return response()->json(array('CustomerLogin'=>null,'message'=>'Gagal Login','error'=>true));

    }


    function getAfterInsertDefaul($id){
        $item=DB::table('PENDAFTAR_LOGIN as csl')
            ->select(
                'csl.ID_USER as Id',
                'csl.USERNAME as UserName',
                'csl.PASSWORD as Password',
                'csl.api_token as ApiToken',
                'csl.ACTIVED as Actived'
            )
            ->where('csl.ID',$id)
            ->where('csl.ACTIVED','>',0)
            ->first();
        return $item;

    }

    function getAfterRegister($id){
        $item=DB::table('PENDAFTAR_LOGIN as csr')
            ->select(
                'csr.ID_USER as Id',
                'csr.NAMA as Name',
                'csr.NO_HP as Phone',
                'csr.EMAIL as Email',
                'csr.USERNAME as UserName',
                'csr.PASSWORD as Password',
                'csr.api_token as ApiToken'
            )
            ->where('csr.ID_USER',$id)
            ->first();
        return $item;

    }

    function getAfterLogin($id){
        $item=DB::table('PENDAFTARAN_ONLINE as pdo')
            ->select(
                'pdo.ID_ONLINE as Id',
                'pdo.ID_USER as IdUser',
                'pdo.NO_ANTRIAN as OrdinalQueue',
                'pdo.ID_PASIEN as IdPatien',
                'pdo.NAMA_PASIEN as Name',
                'pdo.NIK as Nik',
                'pdo.TGLLAHIR_PASIEN as DateBirth',
                'pdo.ALAMAT_PASIEN as Addres',
                'pdo.KOTA_PASIEN as IdCity',
                'pdo.PROVINSI_PASIEN as IdProvince',
                'pdo.KECAMATAN_PASIEN as IdDistricts',
                'pdo.JK_PASIEN as Sex',
                'pdo.AGAMA as Religion',
                'pdo.STATUS_PERKAWINAN as Married',
                'pdo.ID_LAYANAN_RS as IdService',
                'pdo.ID_PGW as IdEmployee',
                'pdo.TGL_PELAYANAN as DateService',
                'pdo.KONFIRMASI_KEDATANGAN as Confirm',
                'pdo.KETERANGAN as Remarks',
                'pdo.NO_HP as Phone',
                'pdo.EMAIL as Email',
                'pdo.CARA_BAYAR as MethodePay',
                'pdo.CREATEAT as CreateAt',
                'pvc.NAMA_KEPENDUDUKAN as Province',
                'cty.NAMA_KEPENDUDUKAN as City',
                'dst.NAMA_KEPENDUDUKAN as Districts',
                'ply.NAMA_LAYANAN_RS as RoomService',
                'csr.USERNAME as UserName',
                'csr.api_token as ApiToken',
                'csr.PASSWORD as Password'

            )
            ->leftjoin('KEPENDUDUKAN as pvc','pvc.ID_KEPENDUDUKAN','pdo.KOTA_PASIEN')
            ->leftjoin('KEPENDUDUKAN as cty','cty.ID_KEPENDUDUKAN','pdo.PROVINSI_PASIEN')
            ->leftjoin('KEPENDUDUKAN as dst','dst.ID_KEPENDUDUKAN','pdo.KECAMATAN_PASIEN')
            ->leftjoin('JENIS_LAYANAN_RS as ply','ply.ID_LAYANAN_RS','pdo.ID_LAYANAN_RS')
            ->join('PENDAFTAR_LOGIN as csr','csr.ID_USER','pdo.ID_USER')
            ->where('pdo.ID_USER','<>','0')
            ->where('csr.ID_USER',$id)
            ->first();
        if(!empty($item)){
            $item->RoomService=$item->RoomService==null?'':$item->RoomService;
            $item->Districts=$item->Districts==null?'':$item->Districts;
            $item->City=$item->City==null?'':$item->City;
            $item->Province=$item->Province==null?'':$item->Province;
            $item->MethodePay=$item->MethodePay==null?'':$item->MethodePay;
            $item->DateService=$item->DateService==null?'':$item->DateService;
            $item->IdService=$item->IdService==null?'':$item->IdService;
            $item->Religion=$item->Religion==null?'':$item->Religion;
            $item->Sex=$item->Sex==null?'':$item->Sex;
            $item->IdDistricts=$item->IdDistricts==null?'':$item->IdDistricts;
            $item->IdProvince=$item->IdProvince==null?'':$item->IdProvince;
            $item->IdCity=$item->IdCity==null?'':$item->IdCity;
            $item->Addres=$item->Addres==null?'':$item->Addres;
            $item->IdPatien=$item->IdPatien==null?'':$item->IdPatien;
            $item->DateBirth=$item->DateBirth==null?'':$item->DateBirth;
            $item->OrdinalQueue=$item->OrdinalQueue==null?'':$item->OrdinalQueue;
            $item->Password     =Crypt::decrypt($item->Password);
            $item->DateBirth    =Carbon::parse($item->DateBirth)->format('d/m/Y');
        }
        return $item;

    }

    public function updateMyProfile(Request $request){
        $username   = $request->input('UserName');
        $password   = $request->input('Password');
        $user=DB::table('PENDAFTARAN_ONLINE as csr')
            ->select(
                'csr.ID_USER as Id',
                'csr.NAMA_PASIEN as Name',
                'csr.NO_HP as Phone',
                'csr.NIK as Nik',
                'csr.EMAIL as Email',
                'csr.CODE as Code',
                'csl.USERNAME as UserName',
                'csl.PASSWORD as Password',
                'csl.api_token as ApiToken'
            )
            ->join('PENDAFTAR_LOGIN as csl','csl.ID_USER','csr.ID_USER')
            ->where('csl.USERNAME',$username)
            ->first();
        if(!empty($user)){
            if (Crypt::decrypt($user->Password) == $password) {
                DB::begintransaction();
                try {
                    DB::table('PENDAFTAR_LOGIN')->where('ID_USER',$request->input('IdUser'))
                        ->update([
                            'USERNAME'=>$request->input('UserName'),
                            'NAMA'=>$request->input('Name'),
                            'EMAIL'=>$request->input('Email'),
                            'NO_HP'=>$request->input('Phone'),
                            'PASSWORD'=>Crypt::encrypt($request->input('Password')),
                            'api_token'=>Crypt::encrypt($request->input('Password')),
                        ]);

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollback();
                    return response()->json(array('MyProfile'=>null,'message'=>'Gagal Menyimpan ','error'=>true));

                }
                return response()->json(array('MyProfile' => $this->getAfterLogin($user->Id), 'message' => 'Berhasil Login', 'error' => false));
            }else{
                return response()->json(array('MyProfile'=>null,'message'=>'Maaf Password Yang Anda Masukkan Tidak Sama Dengan Password Lama','error'=>true));

            }
        }
        return response()->json(array('MyProfile'=>null,'message'=>'Gagal Login','error'=>true));
    }
}
