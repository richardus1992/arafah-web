<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BackEnd\HistoryControll;
use App\Http\Controllers\Utils\PathControll;
use App\Http\Controllers\Utils\UploadFileControll;
use App\Http\Controllers\Utils\ViewControll;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ApiComplainControll extends Controller
{
    protected $Paginate=20;
    protected $History;
    protected $ActElement;
    protected $UploadFile;
    protected $PathImage;


    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();
        $this->UploadFile = new UploadFileControll();
        $this->PathImage = new PathControll();


    }

    public function index($idPasien,Request $request){
        $search =$request->get('search');

        if(!empty($search)){
            $item =DB::table('KOMPLAIN_PASIEN as kpn')
                ->select(
                    'kpn.ID_KOMPLAIN_PASIEN AS Id',
                    'kpn.ID_PASIEN AS IdPatient',
                    'kpn.TANGGAL_KOMPLAIN as DateComplain',
                    'kpn.ID_PARENT as ParentId',
                    'kpn.TANGGAL_DILIHAT as ReadComplain',
                    'kpn.TANGGAL_DILIHAT as StatusReadComplain',
                    'kpn.FLAG_ENTRY as FlagEntry',
                    'kpn.ISI_KOMPLAIN_PASIEN as ContentComplain',
                    'jpn.JENIS as ComplainType',
                    'psn.NAMA_PASIEN as PatientName',
                    'pgw.NAMA_PGW as EmpNameGetComplain',
                    'kpn.FILE_GAMBAR as FileImage',
                    'pgwr.NAMA_PGW as EmpRead',
                    'kpn.FLAG_ENTRY as CountMessage'
                )
                ->join('JENIS_COMPLAIN as jpn','jpn.ID','kpn.ID_JENIS_KOMPLAIN')
                ->join('PASIEN as psn','psn.ID_PASIEN','kpn.ID_PASIEN')
                ->leftjoin('PEGAWAI as pgw','pgw.ID_PGW','kpn.ID_PGW')
                ->leftjoin('PEGAWAI as pgwr','pgwr.ID_PGW','kpn.ID_PGW_READ')
                ->where('kpn.ID_PASIEN',$idPasien)
                ->where('kpn.ACTIVED','>',0)
                ->where('kpn.FLAG_ENTRY',1)
                ->orderBy('Id','desc')
                ->paginate($this->Paginate);
        }else{
            $item =DB::table('KOMPLAIN_PASIEN as kpn')
                ->select(
                    'kpn.ID_KOMPLAIN_PASIEN AS Id',
                    'kpn.ID_PASIEN AS IdPatient',
                    'kpn.TANGGAL_KOMPLAIN as DateComplain',
                    'kpn.ID_PARENT as ParentId',
                    'kpn.TANGGAL_DILIHAT as ReadComplain',
                    'kpn.TANGGAL_DILIHAT as StatusReadComplain',
                    'kpn.FLAG_ENTRY as FlagEntry',
                    'kpn.ISI_KOMPLAIN_PASIEN as ContentComplain',
                    'jpn.JENIS as ComplainType',
                    'psn.NAMA_PASIEN as PatientName',
                    'pgw.NAMA_PGW as EmpNameGetComplain',
                    'kpn.FILE_GAMBAR as FileImage',
                    'pgwr.NAMA_PGW as EmpRead',
                    'kpn.FLAG_ENTRY as CountMessage'
                )
                ->join('JENIS_COMPLAIN as jpn','jpn.ID','kpn.ID_JENIS_KOMPLAIN')
                ->join('PASIEN as psn','psn.ID_PASIEN','kpn.ID_PASIEN')
                ->leftjoin('PEGAWAI as pgw','pgw.ID_PGW','kpn.ID_PGW')
                ->leftjoin('PEGAWAI as pgwr','pgwr.ID_PGW','kpn.ID_PGW_READ')
                ->where('kpn.ID_PASIEN',$idPasien)
                ->where('kpn.ACTIVED','>',0)
                ->where('kpn.FLAG_ENTRY',1)
                ->orderBy('Id','asc')
                ->paginate($this->Paginate);
        }


        if(count($item)>0){
            foreach ($item as $val){
                $val->CountMessage=0;
                $val->StatusReadComplain=($val->StatusReadComplain==null)?0:1;
                $val->ReadComplain=$val->ReadComplain ? 'Di Lihat '.with(new Carbon($val->ReadComplain))->format('d/m/Y') : 'Belum Dilihat';
                $val->DateComplain=$val->DateComplain ? with(new Carbon($val->DateComplain))->format('d/m/Y H:i:s') : '';
                $val->FileImage =!empty($val->FileImage)?$this->PathImage->ApiFileDocument(1).$val->FileImage:'';
            }
            return response()->json(array('Complain'=>$item,'message'=>'Data Tersedia','error'=>false));
        }
        return response()->json(array('Complain'=>null,'message'=>'Data Tidak Ada','error'=>true));


    }


    public function indexDetail($idParent){

        if(!empty($idParent)){
            $item =DB::table('KOMPLAIN_PASIEN as kpn')
                ->select(
                    'kpn.ID_KOMPLAIN_PASIEN AS Id',
                    'kpn.ID_PASIEN AS IdPatient',
                    'kpn.TANGGAL_KOMPLAIN as DateComplain',
                    'kpn.ID_PARENT as ParentId',
                    'kpn.TANGGAL_DILIHAT as ReadComplain',
                    'kpn.TANGGAL_DILIHAT as StatusReadComplain',
                    'kpn.FLAG_ENTRY as FlagEntry',
                    'kpn.ISI_KOMPLAIN_PASIEN as ContentComplain',
                    'jpn.JENIS as ComplainType',
                    'psn.NAMA_PASIEN as PatientName',
                    'pgw.NAMA_PGW as EmpNameGetComplain',
                    'kpn.FILE_GAMBAR as FileImage',
                    'pgwr.NAMA_PGW as EmpRead'
                )
                ->join('JENIS_COMPLAIN as jpn','jpn.ID','kpn.ID_JENIS_KOMPLAIN')
                ->join('PASIEN as psn','psn.ID_PASIEN','kpn.ID_PASIEN')
                ->leftjoin('PEGAWAI as pgw','pgw.ID_PGW','kpn.ID_PGW')
                ->leftjoin('PEGAWAI as pgwr','pgwr.ID_PGW','kpn.ID_PGW_READ')
                ->where('kpn.ACTIVED','>',0)
                ->where('kpn.ID_PARENT',$idParent)
                ->Orwhere('kpn.ID_KOMPLAIN_PASIEN',$idParent)
                ->orderBy('Id','asc')
                ->paginate($this->Paginate);
        }


        if(count($item)>0){
            foreach ($item as $val){
                $val->StatusReadComplain=($val->StatusReadComplain==null)?0:1;
                $val->ReadComplain=$val->ReadComplain ? 'Di Lihat '.with(new Carbon($val->ReadComplain))->format('d/m/Y') : 'Belum Dilihat';
                $val->DateComplain=$val->DateComplain ? with(new Carbon($val->DateComplain))->format('d/m/Y H:i:s') : '';
                $val->FileImage =!empty($val->FileImage)?$this->PathImage->ApiFileDocument(1).$val->FileImage:'';
            }
            return response()->json(array('Complain'=>$item,'message'=>'Data Tersedia','error'=>false));
        }
        return response()->json(array('Complain'=>null,'message'=>'Data Tidak Ada','error'=>true));


    }



    public function store(Request $request){
        $filepath='';
        if($request->has('FileAttachment')){
            $filepath  =$this->UploadFile->uploadFile($request->file('FileAttachment'));
        }
        DB::begintransaction();
        try{
            $idComplain =$this->getLastIdComplain();
            DB::table('KOMPLAIN_PASIEN')
                ->insert([
                    'ID_KOMPLAIN_PASIEN'=>$idComplain,
                    'ID_PASIEN'=>$request->input('IdPatient'),
                    'ISI_KOMPLAIN_PASIEN'=>$request->input('Complain'),
                    'ID_JENIS_KOMPLAIN'=>$request->input('TypeComplain'),
                    'FILE_GAMBAR'=>$filepath,
                    'TANGGAL_KOMPLAIN'=>Carbon::parse(Carbon::now())->format('Y-m-d')
                ]);

            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return response()->json(array('Complain'=>null,'message'=>'Data Tidak Ada','error'=>true));

        }
        return response()->json(array('Complain'=>$this->getDataAfterInsert($idComplain),'message'=>'Data Tersedia','error'=>false));

    }


    function getDataAfterInsert($id){
        $item =DB::table('KOMPLAIN_PASIEN as kpn')
            ->select(
                'kpn.ID_KOMPLAIN_PASIEN AS Id',
                'kpn.ID_PASIEN AS IdPatient',
                'kpn.TANGGAL_KOMPLAIN as DateComplain',
                'kpn.ID_PARENT as ParentId',
                'kpn.TANGGAL_DILIHAT as ReadComplain',
                'kpn.TANGGAL_DILIHAT as StatusReadComplain',
                'kpn.FLAG_ENTRY as FlagEntry',
                'kpn.ISI_KOMPLAIN_PASIEN as ContentComplain',
                'jpn.JENIS as ComplainType',
                'psn.NAMA_PASIEN as PatientName',
                'pgw.NAMA_PGW as EmpNameGetComplain',
                'kpn.FILE_GAMBAR as FileImage',
                'pgwr.NAMA_PGW as EmpRead'
            )
            ->join('JENIS_COMPLAIN as jpn','jpn.ID','kpn.ID_JENIS_KOMPLAIN')
            ->join('PASIEN as psn','psn.ID_PASIEN','kpn.ID_PASIEN')
            ->leftjoin('PEGAWAI as pgw','pgw.ID_PGW','kpn.ID_PGW')
            ->leftjoin('PEGAWAI as pgwr','pgwr.ID_PGW','kpn.ID_PGW_READ')
            ->where('kpn.ID_KOMPLAIN_PASIEN',$id)
            ->paginate($this->Paginate);

        if(count($item)>0){
            foreach ($item as $val){
                $val->StatusReadComplain=($val->StatusReadComplain==null)?0:1;
                $val->ReadComplain=$val->ReadComplain ? 'Di Lihat '.with(new Carbon($val->ReadComplain))->format('d/m/Y') : 'Belum Dilihat';
                $val->DateComplain=$val->DateComplain ? with(new Carbon($val->DateComplain))->format('d/m/Y H:i:s') : '';
                $val->FileImage =!empty($val->FileImage)?$this->PathImage->ApiFileDocument(1).$val->FileImage:'';
            }
        }

        return $item;


    }

    function getLastIdComplain(){
        $id=DB::table('KOMPLAIN_PASIEN')
            ->select('ID_KOMPLAIN_PASIEN')
            ->orderBy('ID_KOMPLAIN_PASIEN','desc')
            ->first();

        if(empty($id)){
            return 1;
        }

        return ($id->ID_KOMPLAIN_PASIEN * 1) + 1;
    }

}
