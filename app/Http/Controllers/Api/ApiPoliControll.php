<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BackEnd\HistoryControll;
use App\Http\Controllers\Utils\ViewControll;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Cache;

class ApiPoliControll extends Controller
{
    protected $Paginate=20;
    protected $History;
    protected $ActElement;


    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();

    }
    public function index(Request $request){
        $search =json_decode($request->get('search'));
        if(!empty($search)){
            $item =DB::table('MASTER_JADWAL as msj')
                ->select(
                    'jls.ID_LAYANAN_RS as Id',
                    'jls.NAMA_LAYANAN_RS as Name'
                )
                ->join('JENIS_LAYANAN_RS as jls','msj.ID_LAYANAN_RS','jls.ID_LAYANAN_RS')
                ->where('msj.flag_tampil',1)
                ->where(function ($q)use ($search) {
                    switch ($search->id){
                        case 1:
                            $q->where('jls.NAMA_LAYANAN_RS','LIKE', '%'.$search->value.'%');
                            break;

                    }
                })
                ->groupBy('jls.ID_LAYANAN_RS')
                ->groupBy('jls.NAMA_LAYANAN_RS')
                ->orderBy('jls.ID_LAYANAN_RS',$search->id==2?$search->value:'desc')
                ->paginate($this->Paginate);
        }else{
            $item =DB::table('MASTER_JADWAL as msj')
                ->select(
                    'jls.ID_LAYANAN_RS as Id',
                    'jls.NAMA_LAYANAN_RS as Name'
                )
                ->join('JENIS_LAYANAN_RS as jls','msj.ID_LAYANAN_RS','jls.ID_LAYANAN_RS')
                ->where('msj.flag_tampil',1)
                ->groupBy('jls.ID_LAYANAN_RS')
                ->groupBy('jls.NAMA_LAYANAN_RS')
                ->paginate($this->Paginate);
        }


        if(count($item)>0){
            return response()->json(array('PolyClinic'=>$item,'message'=>'Data Tersedia','erorr'=>false));
        }
        return response()->json(array('PolyClinic'=>null,'message'=>'Data Tidak Ada','erorr'=>true));
    }

    public function indexAll(){
        Cache::flush();
        $item = Cache::rememberForever('poli-clinic-all', function () {
            return DB::table('MASTER_JADWAL as msj')
                ->select(
                    'jls.ID_LAYANAN_RS as Id',
                    'jls.NAMA_LAYANAN_RS as Name'
                )
                ->join('JENIS_LAYANAN_RS as jls','msj.ID_LAYANAN_RS','jls.ID_LAYANAN_RS')
                ->where('msj.flag_tampil',1)
                ->groupBy('jls.ID_LAYANAN_RS')
                ->groupBy('jls.NAMA_LAYANAN_RS')
                ->get();
        });

        if(count($item)>0){
            return response()->json(array('PolyClinicAll'=>$item,'message'=>'Data Tersedia','erorr'=>false));
        }
        return response()->json(array('PolyClinicAll'=>null,'message'=>'Data Tidak Ada','erorr'=>true));
    }

    public function indexScheduleDoctor($id,Request $request){
        $search =json_decode($request->get('search'));
        $item =DB::table('MASTER_JADWAL as msj')
            ->select(
                'msj.HARI_JADWAL as DaySchedule',
                'msj.HARI_JADWAL as DayName',
                'msj.HARI_JADWAL as DetailSchedule'
            )
            ->where('msj.ID_LAYANAN_RS',$id)
            ->where('msj.flag_tampil',1)
            ->groupBy('msj.HARI_JADWAL')
            ->paginate($this->Paginate);
        if(count($item)>0){
            foreach ($item as $va){
                $va->DayName  =ViewControll::DayName($va->DaySchedule);
                $va->DetailSchedule=$this->detailSchedule($id,$va->DaySchedule);
            }
            return response()->json(array('ScheduleDetail'=>$item,'message'=>'Data Tersedia','erorr'=>false));
        }else{
            return response()->json(array('ScheduleDetail'=>null,'message'=>'Data Tidak Ada','erorr'=>true));
        }

    }

    function detailSchedule($idPoly,$idDay){
        $item =DB::table('MASTER_JADWAL as msj')
            ->select(
                'msj.ID_JADWAL as Id',
                'msj.HARI_JADWAL as DayName',
                'msj.JAM_MULAI_JADWAL as DateSchedule',
                'msj.JAM_MULAI_JADWAL as TimeStart',
                'msj.JAM_BERAKHIR_JADWAL as TimeEnd',
                'jlr.NAMA_LAYANAN_RS as RoomService',
                'pgw.NAMA_PGW as EmployeName',
                'mpi.NAMA_M_PROFESI as Profesi'
            )
            ->join('PEGAWAI as pgw','pgw.ID_PGW','msj.ID_PGW')
            ->join('JENIS_LAYANAN_RS as jlr','jlr.ID_LAYANAN_RS','msj.ID_LAYANAN_RS')
            ->join('MASTER_PROFESI as mpi','mpi.ID_M_PROFESI','pgw.ID_PROFESI')
            ->where('msj.HARI_JADWAL',$idDay)
            ->where('msj.ID_LAYANAN_RS',$idPoly)
            ->where('msj.flag_tampil',1)
            ->get();

        if(count($item)>0){
            foreach ($item as $va){
                $va->TimeStart  =Carbon::parse($va->TimeStart)->format('H:i');
                $va->TimeEnd  =Carbon::parse($va->TimeEnd)->format('H:i');
                $va->DateSchedule  =Carbon::parse($va->DateSchedule)->format('l,d-m-y');
            }
        }
        return $item;
    }

    public function indexDetail($id,Request $request){
        $search =json_decode($request->get('search'));
        if(!empty($search)){
            $item =DB::table('MASTER_JADWAL as msj')
                ->select(
                    'msj.ID_JADWAL as Id',
                    'msj.HARI_JADWAL as DayName',
                    'msj.JAM_MULAI_JADWAL as DateSchedule',
                    'msj.JAM_MULAI_JADWAL as TimeStart',
                    'msj.JAM_BERAKHIR_JADWAL as TimeEnd',
                    'jlr.NAMA_LAYANAN_RS as RoomService',
                    'pgw.NAMA_PGW as EmployeName'
                )
                ->join('PEGAWAI as pgw','pgw.ID_PGW','msj.ID_PGW')
                ->join('JENIS_LAYANAN_RS as jlr','jlr.ID_LAYANAN_RS','msj.ID_LAYANAN_RS')
                ->where(function ($q)use ($search) {
                    switch ($search->id){
                        case 1:
                            $q->where('pgw.NAMA_PGW','LIKE', '%'.$search->value.'%');
                            break;
                        case 2:
                            $q->whereDate('msj.JAM_MULAI_JADWAL',$this->formatDate($search->value, 'Y-m-d'));
                            break;

                    }
                })
                ->orderByRaw($search->id==3?'msj.JAM_MULAI_JADWAL '.$search->value:'msj.JAM_MULAI_JADWAL desc')
                ->where('msj.ID_LAYANAN_RS',$id)
                ->paginate($this->Paginate);
        }else{
            $item =DB::table('MASTER_JADWAL as msj')
                ->select(
                    'msj.ID_JADWAL as Id',
                    'msj.HARI_JADWAL as DayName',
                    'msj.JAM_MULAI_JADWAL as DateSchedule',
                    'msj.JAM_MULAI_JADWAL as TimeStart',
                    'msj.JAM_BERAKHIR_JADWAL as TimeEnd',
                    'jlr.NAMA_LAYANAN_RS as RoomService',
                    'pgw.NAMA_PGW as EmployeName'
                )
                ->join('PEGAWAI as pgw','pgw.ID_PGW','msj.ID_PGW')
                ->join('JENIS_LAYANAN_RS as jlr','jlr.ID_LAYANAN_RS','msj.ID_LAYANAN_RS')
                ->where('msj.ID_LAYANAN_RS',$id)
                ->paginate($this->Paginate);
        }

        if(count($item)>0){
            foreach ($item as $va){
                $va->TimeStart  =Carbon::parse($va->TimeStart)->format('H:i');
                $va->TimeEnd  =Carbon::parse($va->TimeEnd)->format('H:i');
                $va->DateSchedule  =Carbon::parse($va->DateSchedule)->format('l,d-m-y');
            }
        }

        if(count($item)>0){
            return response()->json(array('PolyClinicDetail'=>$item,'message'=>'Data Tersedia','erorr'=>false));
        }
        return response()->json(array('PolyClinicDetail'=>null,'message'=>'Data Tidak Ada','erorr'=>true));

    }

    function formatDate($date, $format)
    {
        return Carbon::createFromFormat('d/m/Y', $date)->format($format);
    }


}
