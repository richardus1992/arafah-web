<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Utils\ViewControll;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Cache;

class ApiScheduleTodayControll extends Controller{
    protected $Paginate=30;
    public function index(Request $request){
        $today=Carbon::now()->dayOfWeek;
        $search =json_decode($request->get('search'));
        if(!empty($search)){
            $item =DB::table('MASTER_JADWAL as msj')
                ->select(
                    'jls.ID_LAYANAN_RS as Id',
                    'jls.NAMA_LAYANAN_RS as Name',
                    'pgw.NAMA_PGW as Employee',
                    'mpi.NAMA_M_PROFESI as Profesi',
                    'msj.HARI_JADWAL as DayName',
                    'msj.HARI_JADWAL as DaySchedule',
                    'msj.JAM_MULAI_JADWAL as TimeIn',
                    'msj.JAM_BERAKHIR_JADWAL as TimeOut',
                    'msj.JAM_BERAKHIR_JADWAL as NameTime'
                )
                ->join('JENIS_LAYANAN_RS as jls','msj.ID_LAYANAN_RS','jls.ID_LAYANAN_RS')
                ->join('PEGAWAI as pgw','pgw.ID_PGW','msj.ID_PGW')
                ->join('MASTER_PROFESI as mpi','mpi.ID_M_PROFESI','pgw.ID_PROFESI')
                ->where('msj.flag_tampil',1)
                ->where('msj.HARI_JADWAL',$today)
                ->where(function ($q)use ($search) {
                    switch ($search->id){
                        case 1:
                            $q->where('jls.NAMA_LAYANAN_RS','LIKE', '%'.$search->value.'%');
                            break;
                        case 2:
                            $q->where('pgw.NAMA_PGW','LIKE', '%'.$search->value.'%');
                            break;

                    }
                })
                ->orderBy('jls.ID_LAYANAN_RS',$search->id==3?$search->value:'desc')
                ->paginate($this->Paginate);
        }else{
            $item =DB::table('MASTER_JADWAL as msj')
                ->select(
                    'jls.ID_LAYANAN_RS as Id',
                    'jls.NAMA_LAYANAN_RS as Name',
                    'pgw.NAMA_PGW as Employee',
                    'mpi.NAMA_M_PROFESI as Profesi',
                    'msj.HARI_JADWAL as DayName',
                    'msj.HARI_JADWAL as DaySchedule',
                    'msj.JAM_MULAI_JADWAL as TimeIn',
                    'msj.JAM_BERAKHIR_JADWAL as TimeOut',
                    'msj.JAM_BERAKHIR_JADWAL as NameTime'
                )
                ->join('JENIS_LAYANAN_RS as jls','msj.ID_LAYANAN_RS','jls.ID_LAYANAN_RS')
                ->join('PEGAWAI as pgw','pgw.ID_PGW','msj.ID_PGW')
                ->join('MASTER_PROFESI as mpi','mpi.ID_M_PROFESI','pgw.ID_PROFESI')
                ->where('msj.flag_tampil',1)
                ->where('msj.HARI_JADWAL',$today)
                ->paginate($this->Paginate);
        }

        if(count($item)>0){
            foreach ($item as $val){
                $val->DayName=ViewControll::DayName($val->DaySchedule);
                $val->TimeIn=Carbon::parse($val->TimeIn)->format('H:i');
                $val->TimeOut=Carbon::parse($val->TimeOut)->format('H:i');
                $val->NameTime=$val->DayName.', '.$val->TimeIn.' s.d '.$val->TimeOut;
            }
            return response()->json(array('ScheduleToday'=>$item,'message'=>'Data Tersedia','erorr'=>false));
        }
        return response()->json(array('ScheduleToday'=>null,'message'=>'Data Tidak Ada '.$search->value,'erorr'=>true));
    }
}
