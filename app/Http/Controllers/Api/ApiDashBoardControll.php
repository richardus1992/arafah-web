<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ApiDashBoardControll extends Controller
{
    public function index($id){
        $item =DB::table('PENDAFTARAN_ONLINE as pdo')
            ->select(
                'pdo.ID_ONLINE as Id',
                'pdo.NO_ANTRIAN as OrdinalQueue',
                'pdo.ID_PASIEN as IdPatien',
                'pdo.NAMA_PASIEN as Name',
                'pdo.NIK as Nik',
                'pdo.TGLLAHIR_PASIEN as DateBirth',
                'pdo.ALAMAT_PASIEN as Addres',
                'pdo.KOTA_PASIEN as IdCity',
                'pdo.PROVINSI_PASIEN as IdProvince',
                'pdo.JK_PASIEN as Sex',
                'pdo.AGAMA as Religion',
                'pdo.STATUS_PERKAWINAN as Married',
                'pdo.ID_LAYANAN_RS as IdService',
                'pdo.ID_PGW as IdEmployee',
                'pdo.TGL_PELAYANAN as DateService',
                'pdo.KONFIRMASI_KEDATANGAN as Confirm',
                'pdo.KONFIRMASI_KEDATANGAN as CountVisit',
                'pdo.KONFIRMASI_KEDATANGAN as CountOutPatient',
                'pdo.KONFIRMASI_KEDATANGAN as CountIntPatient',
                'pdo.KONFIRMASI_KEDATANGAN as CountMonthPatient',
                'pdo.KETERANGAN as Remarks',
                'pdo.NO_HP as Phone',
                'mpy.NAMA_METODE as MethodePay',
                'pvc.NAMA_KEPENDUDUKAN as Province',
                'cty.NAMA_KEPENDUDUKAN as City',
                'ply.NAMA_LAYANAN_RS as RoomService',
                'emp.NAMA_PGW as DoctorVisit'
            )
            ->join('KEPENDUDUKAN as pvc','pvc.ID_KEPENDUDUKAN','pdo.KOTA_PASIEN')
            ->join('KEPENDUDUKAN as cty','cty.ID_KEPENDUDUKAN','pdo.PROVINSI_PASIEN')
            ->join('JENIS_LAYANAN_RS as ply','ply.ID_LAYANAN_RS','pdo.ID_LAYANAN_RS')
            ->join('PEGAWAI as emp','emp.ID_PGW','pdo.ID_PGW')
            ->join('METODE_BAYAR as mpy','mpy.ID','pdo.CARA_BAYAR')
            ->where('pdo.ID_USER',$id)
            ->where('pdo.KONFIRMASI_KEDATANGAN','0')
            ->orderBy('pdo.ID_ONLINE','desc')
            ->first();
        if(!empty($item)){
            $idPatient=$item->IdPatien==null?'0':$item->IdPatien;
            $regisOnline   =DB::table('ANTRIAN')
                ->where('ID_PASIEN',$idPatient)
                ->get();

            if(count($regisOnline)>0){
                $item->CountVisit   = ''.count($regisOnline);
                $item->CountOutPatient   ='0';
                $item->CountIntPatient   ='0';
                $item->CountMonthPatient ='0';
            } else{
                $item->CountVisit   ='0';
                $item->CountOutPatient   ='0';
                $item->CountIntPatient   ='0';
                $item->CountMonthPatient  ='0';

            }
            $item->DateService  =$item->DateService ? with(new Carbon($item->DateService))->format('d/m/Y') : '';
            $item->DateBirth  = $item->DateBirth ? with(new Carbon($item->DateBirth))->format('d/m/Y') : '';
            return response()->json(array('DashBoard'=>$item,'message'=>'Data Tersedia','erorr'=>false));
        }
        return response()->json(array('DashBoard'=>null,'message'=>'Data Tidak Ada','erorr'=>true));
    }
}
