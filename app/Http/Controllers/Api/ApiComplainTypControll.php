<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ApiComplainTypControll extends Controller
{
    protected $Paginate=20;
    public function index(){
        $item =DB::table('JENIS_COMPLAIN as jpn')
            ->select(
                'jpn.ID AS Id',
                'jpn.JENIS as Jenis',
                'jpn.CREATEAT as CreateAt',
                'jpn.UPDATEAT as UpdateAt'
            )
            ->where('jpn.ACTIVED','>',0)
            ->get();

        if(count($item)>0){
            return response()->json(array('ComplainType'=>$item,'message'=>'Data Tersedia','erorr'=>false));
        }
        return response()->json(array('ComplainType'=>null,'message'=>'Data Tidak Ada','erorr'=>true));


    }
}
