<?php

namespace App\Http\Controllers\Api;

use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Cache;

class ApiLocationControll extends Controller
{
    protected $Paginate=20;
    public function indexProvince(){
        $item = Cache::rememberForever('location-province', function () {
            return DB::table('KEPENDUDUKAN as pvc')
                ->select(
                    'pvc.ID_KEPENDUDUKAN as Id',
                    'pvc.NAMA_KEPENDUDUKAN as Name',
                    'pvc.ATASAN_KEPENDUDUKAN as Parent',
                    'pvc.JENIS_KEPENDUDUKAN as Type',
                    'pvc.ATASAN_KEPENDUDUKAN'
                )
                ->where('pvc.ATASAN_KEPENDUDUKAN',null)
                ->get();
        });

        if(count($item)>0){
            return response()->json(array('Province'=>$item,'message'=>'Data Tersedia','erorr'=>false));
        }
        return response()->json(array('Province'=>null,'message'=>'Data Tidak Ada','erorr'=>true));
    }

    public function indexCityProvince($id){
        $id=empty($id)?0:$id;

        $item = Cache::rememberForever('location-city-'.$id, function () use($id){
            return DB::table('KEPENDUDUKAN as pvc')
                ->select(
                    'pvc.ID_KEPENDUDUKAN as Id',
                    'pvc.NAMA_KEPENDUDUKAN as Name',
                    'pvc.ATASAN_KEPENDUDUKAN as Parent',
                    'pvc.JENIS_KEPENDUDUKAN as Type'
                )
                ->where('pvc.ATASAN_KEPENDUDUKAN',$id)
                ->get();
        });




        if(count($item)>0){
            return response()->json(array('City'=>$item,'message'=>'Data Tersedia','erorr'=>false));
        }
        return response()->json(array('City'=>null,'message'=>'Data Tidak Ada','erorr'=>true));
    }

    public function indexDistrictCity($id){
        $id=empty($id)?0:$id;

        $item = Cache::rememberForever('location-district-'.$id, function () use($id){
            return DB::table('KEPENDUDUKAN as pvc')
                ->select(
                    'pvc.ID_KEPENDUDUKAN as Id',
                    'pvc.NAMA_KEPENDUDUKAN as Name',
                    'pvc.ATASAN_KEPENDUDUKAN as Parent',
                    'pvc.JENIS_KEPENDUDUKAN as Type'
                )
                ->where('pvc.ATASAN_KEPENDUDUKAN',$id)
                ->get();
        });




        if(count($item)>0){
            return response()->json(array('Districts'=>$item,'message'=>'Data Tersedia','erorr'=>false));
        }
        return response()->json(array('Districts'=>null,'message'=>'Data Tidak Ada','erorr'=>true));
    }
}
