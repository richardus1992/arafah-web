<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use App\MasterModule;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Http\Controllers\Utils\ViewControll;
use Validator;
use Crypt;

class MyProfileControll extends Controller
{
    protected $History;
    protected $ActElement;


    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();

    }

    public function index()
    {

        
        return view('backend/.my-profile.index');
    }

    public function getData(Request $request)
    {


    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $parent = DB::table('module as mdl')
            ->select('mdl.Name', 'mdl.Id')
            ->where('mdl.Actived', '>', 0)
            ->pluck('Name', 'Id');
        return view('backend/.my-profile.create', compact('parent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();


        $validation = Validator::make($request->all(), $this->validation());

        if ($validation->fails()) {
            return redirect('HomeAdmin/my-profile/create')->withInput()->withErrors($validation->errors());
        }

        DB::begintransaction();
        try {
            DB::table('module')
                ->insert([
                    'Parent' => $request->input('Parent'),
                    'Name' => $request->input('Name'),
                    'Description' => $request->input('Description'),
                    'UserEntry' => session('admin')->Id,

                ]);

            $this->History->store(58, 1, json_encode($requestData));
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            return redirect('HomeAdmin/my-profile/create')->withInput()->withErrors($validation->errors());

        }


        return redirect('HomeAdmin/my-profile')->with('flash_message', 'MasterModule added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $mastermodule = DB::table('module as mdl')
            ->select(
                'mdl.Id',
                'mdl.Parent',
                'mdl.Name',
                'mdl.Description',
                'mdl.CreateAt',
                'mdl.UpdateAt'
            )
            ->where('mdl.Actived', 1)
            ->where('mdl.Id', $id)
            ->first();

        if (count($mastermodule) > 0) {
            $mastermodule->CreateAt = Carbon::parse($mastermodule->CreateAt)->format('l, d-m-Y H:i:s');
            $mastermodule->UpdateAt = Carbon::parse($mastermodule->UpdateAt)->format('l, d-m-Y H:i:s');
        }
        return view('backend/.my-profile.show', compact('mastermodule'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {

        $myprofile=DB::table('users as usr')
            ->select(
                'usr.*',
                'emp.Name as EmployeName'
            )
            ->join('employee as emp','emp.Id','usr.IdEmploye')
            ->where('usr.Id',$id)
            ->where('usr.Actived','>',0)
            ->where('emp.Actived','>',0)
            ->first();


        return view('backend/.my-profile.edit',compact('myprofile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();
        $validation = Validator::make($request->all(), $this->validation());

        $myprofile=DB::table('users')
            ->where('Id',$id)
            ->first();

        if ($validation->fails()) {
            return redirect('HomeAdmin/my-profile/' . $id . '/edit')->withInput()->withErrors($validation->errors());
        }

        if (Crypt::decrypt($myprofile->Password) ==  $request->input('PasswordOld')) {
            DB::table('users')->where('Id', $id)
                ->update([
                    'Password' => Crypt::encrypt($request->input('PasswordNew')),
                ]);
            $this->History->store(58, 2, json_encode($requestData));
        }else{
            return  redirect('HomeAdmin/my-profile/' . $id . '/edit')->withInput()->withErrors(['errmsg'=>'Maaf Password  '.$request->input('PasswordOld').' Dengan Password Lama']);


        }



        return redirect('HomeAdmin/my-profile')->with('flash_message', 'MasterModule updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {


        return redirect('HomeAdmin/my-profile')->with('error_message', 'Master Module Sudah Di Hapus Sebelumnnya!');


    }

    function validation()
    {
        return [
            'PasswordOld' => 'required',
            'PasswordNew' => 'min:3|same:PasswordRep',
            'PasswordRep' => 'min:3'
        ];

    }
}
