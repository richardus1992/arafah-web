<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use App\MasterModule;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\Utils\ViewControll;
use Validator;
use Yajra\DataTables\DataTables;

class PatientControll extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */


    protected $History;
    protected $ActElement;


    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();

    }

    public function index()
    {


        return view('backend.patient.index');
    }

    public function getData(Request $request){

        $item =DB::table('PASIEN as psn')
            ->select(
                'psn.ID_PASIEN as Id',
                'psn.NAMA_PASIEN as Name',
                'psn.JK_PASIEN as Sex',
                'psn.TGLLAHIR_PASIEN as DateBirth',
                'psn.STATUS_PERKAWINAN as Married',
                'psn.ALAMAT_PASIEN as Address',
                'psn.TELP_PASIEN as Phone',
                'psn.PENDIDIKAN as Studying',
                'pvc.NAMA_KEPENDUDUKAN as Province',
                'cty.NAMA_KEPENDUDUKAN as City',
                'dst.NAMA_KEPENDUDUKAN as Districts',
                'psn.TGL_INPUT as CreateAt'
            )
            ->join('KEPENDUDUKAN as pvc','pvc.ID_KEPENDUDUKAN','=','psn.propinsi_pasien')
            ->join('KEPENDUDUKAN as cty','cty.ID_KEPENDUDUKAN','=','psn.KABUPATEN_PASIEN')
            ->join('KEPENDUDUKAN as dst','dst.ID_KEPENDUDUKAN','=','psn.KECAMATAN_PASIEN');

        return DataTables::of($item)

            ->addColumn('action', function ($item) {
                return $this->ActElement->genAction('master-module',$item->Id);
            })
            ->editColumn('Id', '{{$Id}}')
            ->editColumn('Id', function ($item) {
                return $this->ActElement->addButton('HomeAdmin/patient/'.$item->Id.'?view=detail','btn-primary view-detail','fa-bars','Lihat Detail '.$item->Id);
            })
            ->editColumn('CreateAt', function ($item) {
                return $item->CreateAt ? with(new Carbon($item->CreateAt))->format('d/m/Y') : '';
            })
            ->editColumn('DateBirth', function ($item) {
                return $item->DateBirth ? with(new Carbon($item->DateBirth))->format('d/m/Y') : '';
            })
            ->escapeColumns(['*'])
            ->make(true);

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(){
        $parent =DB::table('MODULE as mdl')
            ->select(
                'mdl.NAME',
                'mdl.ID'
            )
            ->where('mdl.ACTIVED','>',0)
            ->pluck('NAME','ID');
        return view('backend/.master-module.create',compact('parent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request){

        $requestData = $request->all();


        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails()) {
            return  redirect('HomeAdmin/master-module/create')->withInput()->withErrors($validation->errors());
        }

        DB::begintransaction();
        try{
            DB::table('MODULE')
                ->insert([
                    'PARENT'=>$request->input('Parent'),
                    'NAME'=>$request->input('Name'),
                    'DESCRIPTION'=>$request->input('Description'),
                    'USERENTRY'=>session('admin')->ID,
                    'CREATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s'),
                    'UPDATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s')

                ]);

            $this->History->store(2,1,json_encode($requestData));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return  redirect('HomeAdmin/master-module/create')->withInput()->withErrors($validation->errors());

        }


        return redirect('HomeAdmin/master-module')->with('flash_message', 'MasterModule added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $item =DB::table('PASIEN as psn')
            ->select(
                'psn.ID_PASIEN as Id',
                'psn.NAMA_PASIEN as Name',
                'psn.JK_PASIEN as Sex',
                'psn.TGLLAHIR_PASIEN as DateBirth',
                'psn.STATUS_PERKAWINAN as Married',
                'psn.ALAMAT_PASIEN as Address',
                'psn.TELP_PASIEN as Phone',
                'psn.PENDIDIKAN as Studying',
                'pvc.NAMA_KEPENDUDUKAN as Province',
                'cty.NAMA_KEPENDUDUKAN as City',
                'dst.NAMA_KEPENDUDUKAN as Districts',
                'psn.TGL_INPUT as CreateAt'
            )
            ->join('KEPENDUDUKAN as pvc','pvc.ID_KEPENDUDUKAN','=','psn.propinsi_pasien')
            ->join('KEPENDUDUKAN as cty','cty.ID_KEPENDUDUKAN','=','psn.KABUPATEN_PASIEN')
            ->join('KEPENDUDUKAN as dst','dst.ID_KEPENDUDUKAN','=','psn.KECAMATAN_PASIEN')
            ->where('psn.ID_PASIEN',$id)
            ->first();


        if(request()->exists('view')) {
            $view = request()->get('view');
            if ($view == 'detail') {

                return view('backend.patient.show-detail', compact('item','detail'));
            }
        }
        return view('backend.patient.index', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $parent =DB::table('MODULE as mdl')
            ->select(
                'mdl.NAME as Name',
                'mdl.ID as Id')
            ->where('mdl.ACTIVED','>',0)
            ->pluck('Name','Id');
        $mastermodule = DB::table('module as mdl')
            ->select(
                'mdl.ID as Id',
                'mdl.PARENT as Parent',
                'mdl.NAME as Name',
                'mdl.DESCRIPTION as Description',
                'mdl.CREATEAT as CreateAt',
                'mdl.UPDATEAT as UpdateAt'
            )
            ->where('mdl.ACTIVED','>',0)
            ->where('mdl.ID',$id)
            ->first();
        return view('backend/.master-module.edit', compact('mastermodule','parent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();
        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails()) {
            return  redirect('HomeAdmin/master-module/'.$id.'/edit')->withInput()->withErrors($validation->errors());
        }

        DB::table('MODULE')->where('ID',$id)
            ->update([
                'PARENT'=>$request->input('Parent'),
                'NAME'=>$request->input('Name'),
                'DESCRIPTION'=>$request->input('Description'),
                'CREATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s'),
                'UPDATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s')
            ]);
        $this->History->store(2,2,json_encode($requestData));

        return redirect('HomeAdmin/master-module')->with('flash_message', 'MasterModule updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id){

        $module = DB::table('MODULE as mdl')
            ->select('*')
            ->where('mdl.ACTIVED',1)
            ->where('mdl.ID',$id)
            ->first();
        if(count($module)>0){
            DB::table('MODULE')->where('ID',$id)
                ->update([
                    'ACTIVED'=>0
                ]);
            $this->History->store(2,3,json_encode($id));
            return redirect('HomeAdmin/master-module')->with('flash_message', 'Master Module Sudah Di Hapus');

        }

        return redirect('HomeAdmin/master-module')->with('error_message', 'Master Module Sudah Di Hapus Sebelumnnya!');



    }

    function validation(){
        return [
            'Name'=>'required',
            'Description'=>'required',
        ];

    }
}
