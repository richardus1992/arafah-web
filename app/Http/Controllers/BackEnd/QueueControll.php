<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Controllers\Utils\ViewControll;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use DB;
use Carbon\Carbon;

class QueueControll extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $History;
    protected $ActElement;


    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();

    }
    public function index()
    {
        return view('backend.queue.index');
    }

    public function getData(){
        $item =DB::table('PENDAFTARAN_ONLINE as pdo')
            ->select(
                'pdo.ID_ONLINE as Id',
                'pdo.NO_ANTRIAN as OrdinalQueue',
                'pdo.ID_PASIEN as IdPatien',
                'pdo.NAMA_PASIEN as Name',
                'pdo.NIK as Nik',
                'pdo.TGLLAHIR_PASIEN as DateBirth',
                'pdo.ALAMAT_PASIEN as Addres',
                'pdo.KOTA_PASIEN as IdCity',
                'pdo.PROVINSI_PASIEN as IdProvince',
                'pdo.JK_PASIEN as Sex',
                'pdo.AGAMA as Religion',
                'pdo.STATUS_PERKAWINAN as Married',
                'pdo.ID_LAYANAN_RS as IdService',
                'pdo.ID_PGW as IdEmployee',
                'pdo.TGL_PELAYANAN as DateService',
                'pdo.KONFIRMASI_KEDATANGAN as Confirm',
                'pdo.KETERANGAN as Remarks',
                'pdo.NO_HP as Phone',
                'pdo.CREATEAT as CreateAt',
                'pdo.TGL_PELAYANAN as DateVisit',
                'pdo.CARA_BAYAR as MethodePay',
                'pvc.NAMA_KEPENDUDUKAN as Province',
                'cty.NAMA_KEPENDUDUKAN as City',
                'ply.NAMA_LAYANAN_RS as RoomService',
                'mtb.NAMA_METODE as MethodePay',
                'pdo.LAMPIRAN_GAMBAR as FileImage',
                'pdo.CARA_BAYAR as IdMethodePay'
            )
            ->join('METODE_BAYAR as mtb','mtb.ID','pdo.CARA_BAYAR')
            ->join('KEPENDUDUKAN as pvc','pvc.ID_KEPENDUDUKAN','pdo.KOTA_PASIEN')
            ->join('KEPENDUDUKAN as cty','cty.ID_KEPENDUDUKAN','pdo.PROVINSI_PASIEN')
            ->join('JENIS_LAYANAN_RS as ply','ply.ID_LAYANAN_RS','pdo.ID_LAYANAN_RS');

        return Datatables::of($item)
            ->addColumn('action', function ($item) {
                return $item->Confirm==0 ?$this->ActElement->genActionConfirmation('queue/1/confirm-visited?Id=',$item->Id):$this->ActElement->genActionDelete('queue',$item->Id);
            })
            ->editColumn('Id', '{{$Id}}')
            ->editColumn('DateBirth', function ($item) {
                return $item->DateBirth ? with(new Carbon($item->DateBirth))->format('d/m/Y') : '';
            })
            ->editColumn('DateVisit', function ($item) {
                return $item->DateVisit ? with(new Carbon($item->DateVisit))->format('d/m/Y') : '';
            })
            ->editColumn('CreateAt', function ($item) {
                return $item->CreateAt ? with(new Carbon($item->CreateAt))->format('d/m/Y') : '';
            })
            ->editColumn('DateService', function ($item) {
                return $item->DateService ? with(new Carbon($item->DateService))->format('d/m/Y') : '';
            })

            ->editColumn('Confirm', function ($item) {
                return $item->Confirm==0 ?$this->ActElement->linkButtonInfo('Belum Datang'):$this->ActElement->linkButtonActived('Suda Datang');
            })

            ->editColumn('MethodePay', function ($item) {
                return $item->IdMethodePay !='5' ? $this->ActElement->genActionDonwload($item->FileImage,'btn-warning','fa fa-download','Lihat Lampiran '.$item->MethodePay):$item->MethodePay;
            })
            ->escapeColumns(['*'])

            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.queue.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){

        DB::begintransaction();
        try{
            DB::table('PENDAFTARAN_ONLINE')
                ->where('ID_ONLINE',$id)
                ->delete();

            $this->History->store(8,3,json_encode($id));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return redirect('HomeAdmin/register-online')->with('error_message', 'Pandfataran Gagal Di Update');

        }
        return redirect('HomeAdmin/register-online')->with('flash_message', 'Pendaftaran Online Berhasil Di Update');
    }

    public function visited(Request $request){
        $id=$request->get('Id');
        $regisOnline = DB::table('PENDAFTARAN_ONLINE')
            ->select('*')
            ->where('ID_ONLINE',$id)
            ->first();

        $noQueue=$this->checkQueue();

        if(empty($noQueue)){
            return redirect('HomeAdmin/register-online')->with('error_message', 'Maaf Slot Pemeriksaan Tidak Tersedia');
        }

        $dataqueue=DB::table('ANTRIAN')
            ->select('*')
            ->orderBy('ID_JADWAL','desc')
            ->paginate(20);
//        dump($dataqueue);
//
//        dump($noQueue);

        if(!empty($regisOnline)){
            DB::begintransaction();
            try{
                $patient = DB::table('PASIEN')
                    ->select('*')
                    ->where('NAMA_PASIEN',$regisOnline->NAMA_PASIEN)
//                    ->where('ALAMAT_PASIEN',$regisOnline->ALAMAT_PASIEN)
                    ->where('TELP_PASIEN',$regisOnline->NO_HP)
                    ->first();


                if(empty($patient)){
                    $idPatient=$this->generatePatientId($regisOnline->NAMA_PASIEN);
                    DB::table('PASIEN')
                        ->insertGetId([
                            'ID_PASIEN'=>$idPatient,
                            'NAMA_PASIEN'=>strtoupper($regisOnline->NAMA_PASIEN),
                            'JK_PASIEN'=>$regisOnline->JK_PASIEN,
                            'TGLLAHIR_PASIEN'=>$regisOnline->TGLLAHIR_PASIEN,
                            'STATUS_PERKAWINAN'=>$regisOnline->STATUS_PERKAWINAN,
                            'ALAMAT_PASIEN'=>strtoupper($regisOnline->ALAMAT_PASIEN),
                            'propinsi_pasien'=>$regisOnline->PROVINSI_PASIEN,
                            'KABUPATEN_PASIEN'=>$regisOnline->KOTA_PASIEN,
                            'TELP_PASIEN'=>$regisOnline->NO_HP,
                            'AGAMA'=>$regisOnline->AGAMA,
                            'PGW_INPUT'=>1,
                            'TGL_INPUT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s'),

                        ]);


                    DB::table('PENDAFTARAN_ONLINE')
                        ->where('ID_ONLINE',$id)
                        ->update([
                            'KONFIRMASI_KEDATANGAN'=>1

                        ]);
                    DB::table('PENDAFTARAN_ONLINE')
                        ->where('ID_USER',$regisOnline->ID_USER)
                        ->update([
                            'ID_PASIEN'=>$idPatient
                        ]);


                    DB::table('ANTRIAN')
                        ->insert([
                            'MAS_ID_PGW'=>$regisOnline->ID_PGW,
                            'ID_LAYANAN_RS'=>$regisOnline->ID_LAYANAN_RS,
                            'TANGGAL_JADWAL'=>$regisOnline->TGL_PELAYANAN,
                            'NO_ANTRIAN'=>$noQueue->NO_ANTRIAN_POLI,
//                            'NO_ANTRIAN'=>'test',
                            'ID_PASIEN'=>$idPatient,
                            'ID_PGW'=>$regisOnline->ID_PGW,
                            'KOSONG_ANTRIAN'=>'1',
                            'KONFIRMASI_ANTRIAN'=>'1',
                            'selesai_layanan'=>'0',
                            'selesai_antrian'=>'0',
                            'selesai_jadwal'=>'0',
                            'jam_datang'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s')

                        ]);


                }else{
                    DB::table('PENDAFTARAN_ONLINE')
                        ->where('ID_ONLINE',$id)
                        ->update([
                            'KONFIRMASI_KEDATANGAN'=>1

                        ]);

                    DB::table('ANTRIAN')
                        ->insert([
                            'MAS_ID_PGW'=>$regisOnline->ID_PGW,
                            'ID_LAYANAN_RS'=>$regisOnline->ID_LAYANAN_RS,
                            'TANGGAL_JADWAL'=>$regisOnline->TGL_PELAYANAN,
                            'NO_ANTRIAN'=>$noQueue->NO_ANTRIAN_POLI,
                            'ID_PASIEN'=>$patient->ID_PASIEN,
                            'ID_PGW'=>$regisOnline->PROVINSI_PASIEN,
                            'KOSONG_ANTRIAN'=>'1',
                            'KONFIRMASI_ANTRIAN'=>'1',
                            'selesai_layanan'=>'0',
                            'selesai_antrian'=>'0',
                            'selesai_jadwal'=>'0',
                            'jam_datang'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s')

                        ]);
                }



                $this->History->store(8,3,json_encode($regisOnline));
                DB::commit();
            }catch (Exception $e){
                DB::rollback();
                return redirect('HomeAdmin/queue')->with('error_message', 'Pandfataran Gagal Di Update');

            }
        }


        return redirect('HomeAdmin/queue')->with('flash_message', 'Pendaftaran Online Berhasil Di Update');
    }

    function generatePatientId($Name){
        $patient = DB::table('PASIEN')
            ->select(
                'ID_PASIEN'
            )
            ->where('ID_PASIEN','LIKE',strtoupper($Name[0]).'%')
            ->first();


        if(empty($patient)){
            return strtoupper($Name[0]).'1000001';
        }else{
//            $numb=preg_replace('/[^0-9]/', '', $patient->ID_PASIEN) * 1 + 1;
            $numb=random_int(10000,99999);
            return strtoupper($Name[0]).$numb;
        }
    }

    function formatDate($date, $format){
        return Carbon::createFromFormat('d/m/Y', $date)->format($format);
    }

    function checkQueue(){


        $queue=DB::table('ANTRIAN_Poli as apl')
            ->select(
                'apl.*',
                'atr.NO_ANTRIAN as AtrNoAntrian'
            )
            ->leftjoin('ANTRIAN as atr', function($join){
                $join->on('atr.NO_ANTRIAN', '=', 'apl.NO_ANTRIAN_POLI')
                    ->wheredate('atr.TGL_PEMERIKSAAN', '=',Carbon::parse(Carbon::now())->format('Y-m-d'))
                    ->where('atr.KOSONG_ANTRIAN',1);
            })
            ->where('atr.NO_ANTRIAN',null)
            ->first();

        return $queue;

    }
}
