<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use App\TypeUser;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Http\Controllers\Utils\ViewControll;
use Validator;
use App\Http\Controllers\Utils\MenuAccess;

class TypeUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */


    protected $History;
    protected $ActElement;


    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();

    }

    public function index()
    {

        return view('backend/.type-user.index');
    }
    
    public function getData(){
    
            $item =DB::table('TYPE_USERS')
                ->select(
                    'ID as Id',
                    'NAME as Name',
                    'DESCRIPTION as Description',
                    'CREATEAT as CreateAt',
                    'UPDATEAT as UpdateAt')
                ->where('ACTIVED','>',0);
                return Datatables::of($item)
                ->addColumn('action', function ($item) {
                    return $this->ActElement->genAction('type-user',$item->Id);
                })
                ->editColumn('Id', '{{$Id}}')
                    ->editColumn('CreateAt', function ($item) {
                        return $item->CreateAt ? with(new Carbon($item->CreateAt))->format('d-m-Y') : '';
                    })
                    ->editColumn('UpdateAt', function ($item) {
                        return $item->UpdateAt ? with(new Carbon($item->UpdateAt))->format('d-m-Y') : '';
                    })
                ->make(true);
    
        }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $treeForm = MenuAccess::buildForm();
        return view('backend/.type-user.create',compact('treeForm'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request){
        $requestData = $request->all();
        $userAccess = [];
        $saveToDB = [];
        if(isset($requestData['UserAccess']) && !empty($requestData['UserAccess']))
        {
          foreach ($requestData['UserAccess'] as $val){
            $action = [];
            $temp = json_decode(str_replace('\'','"',$val),true);
            $saveToDB[$temp['id']]['IDMODULE'] = $temp['id'];
            if(isset($temp['action'])){
              $action = [$temp['action']];
              if (strpos($temp['action'], '.index') !== false) {
                array_push($action,str_replace('.index','.show',$temp['action']));
              }
              if(isset($saveToDB[$temp['id']]['Action'])){
                foreach ($saveToDB[$temp['id']]['Action'] as $val){
                  array_push($action,$val);
                }
              }
              $saveToDB[$temp['id']]['Action'] = $action;
            }
            array_push($userAccess,$temp);
          }
        }

        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails())
        {
            return  redirect('HomeAdmin/type-user/create')->withInput()
              ->withErrors($validation->errors())
              ->with('userAccess',json_encode($userAccess));
        }

        $idTypeUser=DB::table('TYPE_USERS')
            ->insertGetId([
                'NAME'=>$request->input('Name'),
                'DESCRIPTION'=>$request->input('Description'),
                'CREATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s'),
                'UPDATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s'),
                'USERENTRY'=>session('admin')->ID,
            ]);



        if(!empty($saveToDB)){
//            dd($saveToDB);

            foreach ($saveToDB as $i => $v){
            $saveToDB[$i]['IDTYPEUSER'] = $idTypeUser;
            if(isset($v['Action'])){
              $saveToDB[$i]['Action'] = json_encode($v['Action']);
                DB::table('USER_ACCES') ->insert([
                    'IDTYPEUSER'=>$idTypeUser,
                    'IDMODULE'=>$v['IDMODULE'],
                    'ACTION'=>json_encode($v['Action']),
                    'CREATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s'),
                    'UPDATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s'),
                ]);
            }else{
              $saveToDB[$i]['Action'] = null;
                DB::table('USER_ACCES') ->insert([
                    'IDTYPEUSER'=>$idTypeUser,
                    'IDMODULE'=>$v['IDMODULE'],
                    'ACTION'=>'',
                    'CREATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s'),
                    'UPDATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s'),
                ]);
            }
//              dd($saveToDB);

          }
        }




        $this->History->store(11,1,json_encode($requestData));

        return redirect('HomeAdmin/type-user')->with('flash_message', 'TypeUser added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $typeuser = DB::table('type_user as tyu')
                    ->select(
                        'tyu.Id',
                        'tyu.Name',
                        'tyu.Description',
                        'tyu.CreateAt',
                        'tyu.UpdateAt',
                        'usr.UserName'
                    )
                    ->leftJoin('users as usr','usr.Id','=','tyu.UserEntry')
                    ->where('tyu.Actived','>',0)
                    ->where('tyu.Id',$id)
                    ->first();

        if(!empty($typeuser)>0){
            $typeuser->CreateAt = Carbon::parse($typeuser->CreateAt)->format('l, d-m-Y H:i:s');
            $typeuser->UpdateAt = Carbon::parse($typeuser->UpdateAt)->format('l, d-m-Y H:i:s');
        }

        return view('backend/.type-user.show', compact('typeuser'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $typeuser = DB::table('TYPE_USERS')
                    ->select(
                        'ID as Id',
                        'NAME as Name',
                        'DESCRIPTION as Description',
                        'CREATEAT as CreateAt',
                        'UPDATEAT as UpdateAt'
                    )
                    ->where('ACTIVED','>',0)
                    ->where('ID',$id)
                    ->first();

        $Access = DB::table('USER_ACCES')
          ->select('IDMODULE as IdModule','ACTION as Action')
          ->where('IDTYPEUSER',$id)->get();

        $userAccess = [];
        if(sizeof($Access) > 0)
        {
          foreach ($Access as $val){
            if(!empty($val->Action)){
              $action = json_decode($val->Action,true);
              foreach ((array)$action as $act){
                $temp = ['id'=>$val->IdModule,'action'=>$act];
                array_push($userAccess,$temp);
              }
            }else{
              $temp = ['id'=>$val->IdModule];
              array_push($userAccess,$temp);
            }
          }
        }
        $userAccess = json_encode($userAccess);
        $treeForm = MenuAccess::buildForm();

        return view('backend/.type-user.edit', compact('typeuser','userAccess','treeForm'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();
        $userAccess = [];
        $saveToDB = [];
        if(isset($requestData['UserAccess']) && !empty($requestData['UserAccess']))
        {
          foreach ($requestData['UserAccess'] as $val){
            $action = [];
            $temp = json_decode(str_replace('\'','"',$val),true);
            $saveToDB[$temp['id']]['IDMODULE'] = $temp['id'];
            if(isset($temp['action'])){
              $action = [$temp['action']];
              if (strpos($temp['action'], '.index') !== false) {
                array_push($action,str_replace('.index','.show',$temp['action']));
              }
              if(isset($saveToDB[$temp['id']]['Action'])){
                foreach ($saveToDB[$temp['id']]['Action'] as $val){
                  array_push($action,$val);
                }
              }
              $saveToDB[$temp['id']]['Action'] = $action;
            }
            array_push($userAccess,$temp);
          }
        }

        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails())
        {
            return  redirect('HomeAdmin/type-user/'.$id.'/edit')->withInput()
              ->withErrors($validation->errors())
              ->with('userAccess',json_encode($userAccess));
        }



        DB::table('TYPE_USERS')->where('ID',$id)
         ->update([
                'NAME'=>$request->input('Name'),
                'DESCRIPTION'=>$request->input('Description'),
                
                ]);

        DB::table('USER_ACCES')->where('IDTYPEUSER',$id)->delete();
        if(!empty($saveToDB)){
          foreach ($saveToDB as $i => $v){
            $saveToDB[$i]['IDTYPEUSER'] = $id;
              if(isset($v['Action'])){
                  $saveToDB[$i]['Action'] = json_encode($v['Action']);
                  DB::table('USER_ACCES') ->insert([
                      'IDTYPEUSER'=>$id,
                      'IDMODULE'=>$v['IDMODULE'],
                      'ACTION'=>json_encode($v['Action']),
                      'CREATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s'),
                      'UPDATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s'),
                  ]);
              }else{
                  $saveToDB[$i]['Action'] = null;
                  DB::table('USER_ACCES') ->insert([
                      'IDTYPEUSER'=>$id,
                      'IDMODULE'=>$v['IDMODULE'],
                      'ACTION'=>'',
                      'CREATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s'),
                      'UPDATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s'),
                  ]);
              }
          }
        }


        $this->History->store(11,2,json_encode($requestData));

        return redirect('HomeAdmin/type-user')->with('flash_message', 'TypeUser updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id){

        $typeuser   =DB::table('TYPE_USERS')->where('ID',$id)->first();

        DB::table('TYPE_USERS')->where('ID',$id)
            ->update([
                'ACTIVED'=>0
            ]);

        $this->History->store(11,3,json_encode($typeuser));

        return redirect('HomeAdmin/type-user')->with('flash_message', 'TypeUser deleted!');
    }


    function validation(){
        return [
            'Name'=>'required',
        ];

    }
}
