<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Controllers\Utils\ViewControll;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use DB;
use Validator;

class ScheduleDoctorControll extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $History;
    protected $ActElement;


    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();

    }

    public function index()
    {
        $dayname=$this->ActElement->arrDayName();
        return view('backend.schedule-doctor.index',compact('dayname'));
    }

    public function getData(Request $request){
        $item =DB::table('MASTER_JADWAL as msj')
            ->select(
                'msj.ID_JADWAL as Id',
                'msj.HARI_JADWAL as DayName',
                'msj.JAM_MULAI_JADWAL as DateSchedule',
                'msj.JAM_MULAI_JADWAL as TimeStart',
                'msj.JAM_BERAKHIR_JADWAL as TimeEnd',
                'jlr.NAMA_LAYANAN_RS as RoomService',
                'msj.flag_tampil as Status',
                'pgw.NAMA_PGW as EmployeName'
            )
            ->join('PEGAWAI as pgw','pgw.ID_PGW','msj.ID_PGW')
            ->join('JENIS_LAYANAN_RS as jlr','jlr.ID_LAYANAN_RS','msj.ID_LAYANAN_RS');
        return DataTables::of($item)

            ->addColumn('action', function ($item) {
                return $this->ActElement->genActionEdit('schedule-doctor',$item->Id);
            })
            ->editColumn('Id', '{{$Id}}')
            ->editColumn('TimeStart', function ($item) {
                return $item->TimeStart ? with(new Carbon($item->TimeStart))->format('H:i:s') : '';
            })
            ->editColumn('TimeEnd', function ($item) {
                return $item->TimeEnd ? with(new Carbon($item->TimeEnd))->format('H:i:s') : '';
            })
            ->editColumn('Status', function ($item) {
                $statusview=$item->Status==1?'checked':'';
                return $this->ActElement->genActionSwitch($item->Id,$statusview);
            })
            ->editColumn('DayName', function ($item) {
                return ViewControll::DayName($item->DayName);
            })
            ->escapeColumns(['*'])
            ->make(true);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Employe=DB::table('PEGAWAI')
            ->select(
                'ID_PGW as Id',
                'NAMA_PGW as Name'
            )
            ->pluck('Name','Id');
        $ServiceType=DB::table('JENIS_LAYANAN_RS')
            ->select(
                'ID_LAYANAN_RS as Id',
                'NAMA_LAYANAN_RS as Name'
            )
            ->pluck('Name','Id');

        return view('backend.schedule-doctor.create',compact('Employe','ServiceType'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails()) {
            return  redirect('HomeAdmin/schedule-doctor/create')->withInput()->withErrors($validation->errors());
        }

        $timeIn=$this->formatDate($request->input('DateIn'),'Y-m-d').' '.$request->input('TimeIn').':00';
        $timeOut=$this->formatDate($request->input('DateOut'), 'Y-m-d').' '.$request->input('TimeEnd').':00';
        $timeOutDate = Carbon::createFromFormat('Y-m-d H:i:s',$timeOut);
        $timeInDate = Carbon::createFromFormat('Y-m-d H:i:s',$timeIn);
        $duration=$timeInDate->diffInHours($timeOutDate);
        $dayOfWeek=Carbon::createFromFormat('Y-m-d H:i:s',$timeIn)->dayOfWeek;
        DB::begintransaction();
        try{
            DB::table('MASTER_JADWAL')
                ->insert([
                    'ID_JADWAL'=>$this->getLastId(),
                    'ID_PGW'=>$request->input('IdEmploye'),
                    'ID_LAYANAN_RS'=>$request->input('IdServiceType'),
                    'HARI_JADWAL'=>$dayOfWeek,
                    'JAM_MULAI_JADWAL'=>$timeIn,
                    'JAM_BERAKHIR_JADWAL'=>$timeOut,
                    'DURASI_PER_PERIKSA_JADWAL'=>$duration,
                ]);
            $this->History->store(25,1,json_encode($requestData));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return  redirect('HomeAdmin/schedule-doctor/create')->withInput()->withErrors($validation->errors());

        }


        return redirect('HomeAdmin/schedule-doctor')->with('flash_message', 'Methode Bayar added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item=DB::table('MASTER_JADWAL as msj')
            ->select(
                'msj.ID_JADWAL as Id',
                'msj.HARI_JADWAL as DayName',
                'msj.JAM_MULAI_JADWAL as DateSchedule',
                'msj.JAM_MULAI_JADWAL as DateOut',
                'msj.JAM_MULAI_JADWAL as DateIn',
                'msj.JAM_MULAI_JADWAL as TimeIn',
                'msj.JAM_BERAKHIR_JADWAL as TimeEnd',
                'msj.ID_PGW as IdEmploye',
                'msj.ID_LAYANAN_RS as IdServiceType',
                'jlr.NAMA_LAYANAN_RS as RoomService',
                'msj.flag_tampil as Status',
                'pgw.NAMA_PGW as EmployeName'
            )
            ->join('PEGAWAI as pgw','pgw.ID_PGW','msj.ID_PGW')
            ->join('JENIS_LAYANAN_RS as jlr','jlr.ID_LAYANAN_RS','msj.ID_LAYANAN_RS')
            ->where('msj.ID_JADWAL',$id)
            ->first();

        $item->DateIn=Carbon::parse($item->DateIn)->format('d/m/Y');
        $item->DateOut=Carbon::parse($item->DateOut)->format('d/m/Y');
        $item->TimeIn=Carbon::parse($item->TimeIn)->format('H:i');
        $item->TimeEnd=Carbon::parse($item->TimeEnd)->format('H:i');

        $Employe=DB::table('PEGAWAI')
            ->select(
                'ID_PGW as Id',
                'NAMA_PGW as Name'
            )
            ->pluck('Name','Id');

        $ServiceType=DB::table('JENIS_LAYANAN_RS')
            ->select(
                'ID_LAYANAN_RS as Id',
                'NAMA_LAYANAN_RS as Name'
            )
            ->pluck('Name','Id');

        return view('backend.schedule-doctor.edit', compact('Employe','item','ServiceType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();

        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails()) {
            return  redirect('HomeAdmin/schedule-doctor/'.$id.'/edit')->withInput()->withErrors($validation->errors());
        }

        $timeIn=$this->formatDate($request->input('DateIn'),'Y-m-d').' '.$request->input('TimeIn').':00';
        $timeOut=$this->formatDate($request->input('DateOut'), 'Y-m-d').' '.$request->input('TimeEnd').':00';
        $timeOutDate = Carbon::createFromFormat('Y-m-d H:i:s',$timeOut);
        $timeInDate = Carbon::createFromFormat('Y-m-d H:i:s',$timeIn);
        $duration=$timeInDate->diffInHours($timeOutDate);
        $dayOfWeek=Carbon::createFromFormat('Y-m-d H:i:s',$timeIn)->dayOfWeek;
        DB::begintransaction();
        try{
            DB::table('MASTER_JADWAL')->where('ID_JADWAL',$id)
                ->update([
                    'ID_PGW'=>$request->input('IdEmploye'),
                    'ID_LAYANAN_RS'=>$request->input('IdServiceType'),
                    'HARI_JADWAL'=>$dayOfWeek,
                    'JAM_MULAI_JADWAL'=>$timeIn,
                    'JAM_BERAKHIR_JADWAL'=>$timeOut,
                    'DURASI_PER_PERIKSA_JADWAL'=>$duration,
                ]);
            $this->History->store(25,2,json_encode($requestData));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return  redirect('HomeAdmin/schedule-doctor/'.$id.'/edit')->withInput()->withErrors($validation->errors());

        }


        return redirect('HomeAdmin/schedule-doctor')->with('flash_message', 'Methode Bayar Update!');
    }

    public function updateStatus(Request $request){
        $satatus=$request->get('Status')=='checked'?0:1;
        DB::begintransaction();
        try{
            DB::table('MASTER_JADWAL')
                ->where('ID_JADWAL',$request->get('Id'))
                ->update([
                    'flag_tampil'=>$satatus
                ]);

            $this->History->store(8,3,json_encode($request->get('Id')));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return array('error'=>true,'message'=>$e->getMessage());

        }
        return array('error'=>false);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    function validation(){
        return [
            'IdEmploye'=>'required',
            'IdServiceType'=>'required',
            'DateIn'=>'required',
            'DateOut'=>'required',
            'TimeIn'=>'required',
            'TimeEnd'=>'required',
        ];

    }
    function formatDate($date, $format)
    {
        return Carbon::createFromFormat('d/m/Y', $date)->format($format);
    }

    function getLastId(){
        $location   =DB::table('MASTER_JADWAL')
            ->select('ID_JADWAL')
            ->orderBy('ID_JADWAL','desc')
            ->first();


        return $location->ID_JADWAL + 1;
    }
}
