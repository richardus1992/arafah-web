<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use App\MasterModule;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Http\Controllers\Utils\ViewControll;
use Validator;

class CityControll extends Controller
{
    protected $History;
    protected $ActElement;


    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();

    }

    public function index()
    {
        return view('backend/.city.index');
    }
    
    public function getData(){
    
        $item =DB::table('KEPENDUDUKAN as cty')
            ->select(
                'cty.ID_KEPENDUDUKAN as Id',
                'cty.NAMA_KEPENDUDUKAN as City',
                'pvc.NAMA_KEPENDUDUKAN as Province'
            )
            ->join('KEPENDUDUKAN as pvc','pvc.ID_KEPENDUDUKAN','cty.ATASAN_KEPENDUDUKAN')
            ->where('cty.JENIS_KEPENDUDUKAN',2);


            return Datatables::of($item)
        
            ->addColumn('action', function ($item) {
                return $this->ActElement->genAction('city',$item->Id);
            })
            ->make(true);
    
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $province =DB::table('KEPENDUDUKAN as pvc')
            ->select(
                'pvc.NAMA_KEPENDUDUKAN as Name',
                'pvc.ID_KEPENDUDUKAN as Id'
            )
            ->where('pvc.JENIS_KEPENDUDUKAN',1)
            ->pluck('Name','Id');
        return view('backend/.city.create',compact('province'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request){

        $requestData = $request->all();


        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails()) {
            return  redirect('HomeAdmin/city/create')->withInput()->withErrors($validation->errors());
        }

        DB::begintransaction();
        try{
            DB::table('KEPENDUDUKAN')
                ->insert([
                    'ATASAN_KEPENDUDUKAN'=>$request->input('IdProvince'),
                    'NAMA_KEPENDUDUKAN'=>$request->input('Name'),
                    'JENIS_KEPENDUDUKAN'=>2,
                    'ID_KEPENDUDUKAN'=>$this->getLastId()
                ]);

            $this->History->store(22,1,json_encode($requestData));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return  redirect('HomeAdmin/city/create')->withInput()->withErrors($validation->errors());

        }


        return redirect('HomeAdmin/city')->with('flash_message', 'Kota added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $city = DB::table('KEPENDUDUKAN as cty')
            ->select(
                'cty.ID_KEPENDUDUKAN as Id',
                'cty.NAMA_KEPENDUDUKAN as Name',
                'pvc.NAMA_KEPENDUDUKAN as Province'
            )
            ->join('KEPENDUDUKAN as pvc','pvc.ID_KEPENDUDUKAN','cty.ATASAN_KEPENDUDUKAN')
            ->where('cty.ID_KEPENDUDUKAN',$id)
            ->first();

        return view('backend/.city.show', compact('city'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $province =DB::table('KEPENDUDUKAN as pvc')
            ->select(
                'pvc.NAMA_KEPENDUDUKAN as Name',
                'pvc.ID_KEPENDUDUKAN as Id'
            )
            ->where('pvc.JENIS_KEPENDUDUKAN',1)
            ->pluck('Name','Id');
        $city =DB::table('KEPENDUDUKAN as cty')
            ->select(
                'cty.ID_KEPENDUDUKAN as Id',
                'cty.NAMA_KEPENDUDUKAN as Name',
                'cty.ATASAN_KEPENDUDUKAN as IdProvince'
            )
            ->where('cty.ID_KEPENDUDUKAN',$id)
            ->first();
        return view('backend/.city.edit', compact('province','city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();
        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails()) {
            return  redirect('HomeAdmin/city/'.$id.'/edit')->withInput()->withErrors($validation->errors());
        }

        DB::begintransaction();
        try{

            DB::table('KEPENDUDUKAN')
                ->where('ID_KEPENDUDUKAN',$id)
                ->update([
                    'ATASAN_KEPENDUDUKAN'=>$request->input('IdProvince'),
                    'NAMA_KEPENDUDUKAN'=>$request->input('Name'),
                    'JENIS_KEPENDUDUKAN'=>2,
                ]);

            $this->History->store(22,2,json_encode($requestData));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return  redirect('HomeAdmin/city/'.$id.'/edit')->withInput()->withErrors($validation->errors());

        }

        return redirect('HomeAdmin/city')->with('flash_message', 'Kota updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id){

        $city = DB::table('KEPENDUDUKAN')
            ->select('*')
            ->where('ID_KEPENDUDUKAN',$id)
            ->first();

            DB::begintransaction();
        try{
            DB::table('KEPENDUDUKAN')
                ->where('ID_KEPENDUDUKAN',$id)
                ->delete();

            $this->History->store(22,3,json_encode($city));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return redirect('HomeAdmin/city')->with('error_message', 'Kota Gagal Dihapus');

        }
        return redirect('HomeAdmin/city')->with('flash_message', 'Kota Berhasil Dihapus');


    }

    function validation(){
        return [
            'Name'=>'required',
            'IdProvince'=>'required',
        ];

    }

    function getLastId(){
        $location   =DB::table('KEPENDUDUKAN')
            ->select('ID_KEPENDUDUKAN')
            ->orderBy('ID_KEPENDUDUKAN','desc')
            ->first();


        return $location->ID_KEPENDUDUKAN + 1;
    }


}
