<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Controllers\Utils\UploadFileControll;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Http\Controllers\Utils\ViewControll;
use Validator;

class ComplainControll extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */


    protected $History;
    protected $ActElement;
    protected $UploadFile;


    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();
        $this->UploadFile = new UploadFileControll();


    }

    public function index()
    {

        return view('backend.complain.index');
    }

    public function getData(){

        $item =DB::table('KOMPLAIN_PASIEN as kpn')
            ->select(
                'kpn.ID_KOMPLAIN_PASIEN AS Id',
                'kpn.TANGGAL_KOMPLAIN as DateComplain',
                'kpn.ID_PARENT as ParentId',
                'kpn.TANGGAL_DILIHAT as ReadComplain',
                'jpn.JENIS as ComplainType',
                'psn.NAMA_PASIEN as PatientName',
                'pgw.NAMA_PGW as EmpNameGetComplain',
                'epy.Complain',
                'pgwr.NAMA_PGW as EmpRead'
            )
            ->join('JENIS_COMPLAIN as jpn','jpn.ID','kpn.ID_JENIS_KOMPLAIN')
            ->join('PASIEN as psn','psn.ID_PASIEN','kpn.ID_PASIEN')
            ->leftjoin('PEGAWAI as pgw','pgw.ID_PGW','kpn.ID_PGW')
            ->join('PEGAWAI as pgwr','pgwr.ID_PGW','kpn.ID_PGW_READ')
            ->leftjoin(
                DB::raw('(SELECT ID_PARENT,COUNT(ID_KOMPLAIN_PASIEN) AS Complain FROM KOMPLAIN_PASIEN where Actived > 0 GROUP BY ID_PARENT) AS epy'),
                'kpn.ID_KOMPLAIN_PASIEN','=','epy.ID_PARENT'
            )
            ->where('kpn.FLAG_ENTRY',1);

        return Datatables::of($item)
            ->addColumn('action', function ($item) {
                return $this->ActElement->genActionShowReplay('complain',$item->Id);
            })
            ->editColumn('Id', '{{$Id}}')
            ->editColumn('Id', function ($item) {
                if($item->Complain > 0){
                    return $this->ActElement->addButton('HomeAdmin/complain/'.$item->Id.'?view=detail','btn-primary view-detail','fa-bars','Lihat Balasan');
                }else{
                    return $item->Id;
                }

            })
            ->editColumn('DateComplain', function ($item) {
                return $item->DateComplain ? with(new Carbon($item->DateComplain))->format('d/m/Y') : '';
            })
            ->editColumn('ReadComplain', function ($item) {
                return $item->ReadComplain ? $this->ActElement->linkButtonActived('Di Lihat '.with(new Carbon($item->ReadComplain))->format('d/m/Y')) : $this->ActElement->linkButtonNotVerified('Belum Dilihat');
            })
            ->escapeColumns(['*'])
            ->make(true);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {

        return view('backend/.complain.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request){

        $requestData = $request->all();
        $this->History->store(19,1,json_encode($requestData));

        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails())
        {
            return  redirect('HomeAdmin/complain-type/create')->withInput()->withErrors($validation->errors());
        }
        DB::table('JENIS_COMPLAIN')
            ->insert([
                'JENIS'=>$request->input('Jenis'),
                'CREATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s'),
                'UPDATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s'),
                'UserEntry'=>session('admin')->ID,
            ]);


        return redirect('HomeAdmin/complain')->with('flash_message', 'Jenis Complain added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $item =DB::table('KOMPLAIN_PASIEN as kpn')
            ->select(
                'kpn.ID_KOMPLAIN_PASIEN AS Id',
                'kpn.ID_PASIEN AS IdPatient',
                'kpn.TANGGAL_KOMPLAIN as DateComplain',
                'kpn.ID_PARENT as ParentId',
                'kpn.TANGGAL_DILIHAT as ReadComplain',
                'kpn.ID_JENIS_KOMPLAIN as IdTypeComplain',
                'jpn.JENIS as ComplainType',
                'psn.NAMA_PASIEN as PatientName',
                'psn.ALAMAT_PASIEN as AddPatient',
                'psn.TELP_PASIEN as PhonePatient',
                'pgw.NAMA_PGW as EmpNameGetComplain',
                'pgwr.NAMA_PGW as EmpRead'
            )
            ->leftjoin('JENIS_COMPLAIN as jpn','jpn.ID','kpn.ID_JENIS_KOMPLAIN')
            ->leftjoin('PASIEN as psn','psn.ID_PASIEN','kpn.ID_PASIEN')
            ->leftjoin('PEGAWAI as pgw','pgw.ID_PGW','kpn.ID_PGW')
            ->leftjoin('PEGAWAI as pgwr','pgwr.ID_PGW','kpn.ID_PGW_READ')
            ->where('kpn.ID_KOMPLAIN_PASIEN',$id)
            ->first();



        $isDetail = request()->exists('view')?true:false;
        if($isDetail){
            $itemDetail =DB::table('KOMPLAIN_PASIEN as kpn')
                ->select(
                    'kpn.ID_KOMPLAIN_PASIEN AS Id',
                    'kpn.ID_PASIEN AS IdPatient',
                    'kpn.TANGGAL_KOMPLAIN as DateComplain',
                    'kpn.ID_PARENT as ParentId',
                    'kpn.TANGGAL_DILIHAT as ReadComplain',
                    'kpn.ID_JENIS_KOMPLAIN as IdTypeComplain',
                    'kpn.ISI_KOMPLAIN_PASIEN as ContentComplain',
                    'kpn.FILE_GAMBAR as FileImage',
                    'jpn.JENIS as ComplainType',
                    'psn.NAMA_PASIEN as PatientName',
                    'psn.ALAMAT_PASIEN as AddPatient',
                    'psn.TELP_PASIEN as PhonePatient',
                    'pgw.NAMA_PGW as EmpNameGetComplain',
                    'pgwr.NAMA_PGW as EmpRead'
                )
                ->leftjoin('JENIS_COMPLAIN as jpn','jpn.ID','kpn.ID_JENIS_KOMPLAIN')
                ->leftjoin('PASIEN as psn','psn.ID_PASIEN','kpn.ID_PASIEN')
                ->leftjoin('PEGAWAI as pgw','pgw.ID_PGW','kpn.ID_PGW')
                ->leftjoin('PEGAWAI as pgwr','pgwr.ID_PGW','kpn.ID_PGW_READ')
                ->where('kpn.ID_PARENT',$id)
                ->where('kpn.FLAG_ENTRY',2)
                ->get();
            return view('backend.complain.show-detail', compact('item','itemDetail'));
        }

        $itemDetail =DB::table('KOMPLAIN_PASIEN as kpn')
            ->select(
                'kpn.ID_KOMPLAIN_PASIEN AS Id',
                'kpn.ID_PASIEN AS IdPatient',
                'kpn.TANGGAL_KOMPLAIN as DateComplain',
                'kpn.ID_PARENT as ParentId',
                'kpn.TANGGAL_DILIHAT as ReadComplain',
                'kpn.ID_JENIS_KOMPLAIN as IdTypeComplain',
                'kpn.ISI_KOMPLAIN_PASIEN as ContentComplain',
                'kpn.FILE_GAMBAR as FileImage',
                'jpn.JENIS as ComplainType',
                'psn.NAMA_PASIEN as PatientName',
                'psn.ALAMAT_PASIEN as AddPatient',
                'psn.TELP_PASIEN as PhonePatient',
                'pgw.NAMA_PGW as EmpNameGetComplain',
                'pgwr.NAMA_PGW as EmpRead'
            )
            ->leftjoin('JENIS_COMPLAIN as jpn','jpn.ID','kpn.ID_JENIS_KOMPLAIN')
            ->leftjoin('PASIEN as psn','psn.ID_PASIEN','kpn.ID_PASIEN')
            ->leftjoin('PEGAWAI as pgw','pgw.ID_PGW','kpn.ID_PGW')
            ->leftjoin('PEGAWAI as pgwr','pgwr.ID_PGW','kpn.ID_PGW_READ')
            ->where('kpn.ID_KOMPLAIN_PASIEN',$id)
            ->get();

        return view('backend.complain.show', compact('item','itemDetail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {

        $item =DB::table('KOMPLAIN_PASIEN as kpn')
            ->select(
                'kpn.ID_KOMPLAIN_PASIEN AS Id',
                'kpn.ID_PASIEN AS IdPatient',
                'kpn.TANGGAL_KOMPLAIN as DateComplain',
                'kpn.ID_PARENT as ParentId',
                'kpn.TANGGAL_DILIHAT as ReadComplain',
                'kpn.ID_JENIS_KOMPLAIN as IdTypeComplain',
                'kpn.ISI_KOMPLAIN_PASIEN as ContentComplain',
                'kpn.FILE_GAMBAR as FileImage',
                'jpn.JENIS as ComplainType',
                'psn.NAMA_PASIEN as PatientName',
                'pgw.NAMA_PGW as EmpNameGetComplain',
                'pgwr.NAMA_PGW as EmpRead'
            )
            ->join('JENIS_COMPLAIN as jpn','jpn.ID','kpn.ID_JENIS_KOMPLAIN')
            ->join('PASIEN as psn','psn.ID_PASIEN','kpn.ID_PASIEN')
            ->leftjoin('PEGAWAI as pgw','pgw.ID_PGW','kpn.ID_PGW')
            ->join('PEGAWAI as pgwr','pgwr.ID_PGW','kpn.ID_PGW_READ')
            ->where('kpn.ID_KOMPLAIN_PASIEN',$id)
            ->first();




        return view('backend/.complain.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id){

        $requestData = $request->all();
        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails()) {
            return  redirect('HomeAdmin/complain-type/'.$id.'/edit')->withInput()->withErrors($validation->errors());
        }
        $filepath='';
        if($request->has('FileAttachment')){
            $filepath  =$this->UploadFile->uploadFile($request->file('FileAttachment'));
        }


        DB::table('KOMPLAIN_PASIEN')
            ->insert([
                'ID_KOMPLAIN_PASIEN'=>$this->getLastIdComplain(),
                'ID_PGW_READ'=>session('admin')->ID,
                'ISI_KOMPLAIN_PASIEN'=>$request->input('Complain'),
                'ID_PASIEN'=>$request->input('IdPatient'),
                'ID_JENIS_KOMPLAIN'=>$request->input('IdTypeComplain'),
                'ID_PARENT'=>$id,
                'FLAG_ENTRY'=>2,
                'FILE_GAMBAR'=>$filepath,
                'TANGGAL_KOMPLAIN'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s'),
                'TANGGAL_DILIHAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s')
            ]);

        DB::table('KOMPLAIN_PASIEN')->where('ID_KOMPLAIN_PASIEN',$id)
            ->Update([
                'ID_PGW_READ'=>session('admin')->ID,
                'TANGGAL_DILIHAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s')
            ]);



        $this->History->store(19,2,json_encode($requestData));

        return redirect('HomeAdmin/complain')->with('flash_message', 'Jenis Complain updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id){

        DB::table('JENIS_COMPLAIN')->where('ID',$id)
            ->update([
                'ACTIVED'=>0
            ]);

        $this->History->store(19,3,json_encode($id));

        return redirect('HomeAdmin/complain-type')->with('flash_message', 'Jenis Complain deleted!');
    }

    function validation(){
        return [
            'Complain'=>'required',
        ];

    }
    function formatDate($date, $format) {
        return Carbon::createFromFormat('d/m/Y', $date)->format($format);
    }

    public function messageview(){


        return view('backend.complain.message-view');
    }
    public function inbox(){


        return view('backend.complain.inbox');
    }

    public function sent(){


        return view('backend.complain.sent');
    }

    public function compose(){


        return view('backend.complain.compose');
    }

    function getLastIdComplain(){
        $id=DB::table('KOMPLAIN_PASIEN')
            ->select('ID_KOMPLAIN_PASIEN')
            ->orderBy('ID_KOMPLAIN_PASIEN','desc')
            ->first();

        return ($id->ID_KOMPLAIN_PASIEN * 1) + 1;
    }

}
