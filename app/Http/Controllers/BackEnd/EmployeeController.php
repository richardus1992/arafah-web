<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use App\Employee;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Http\Controllers\Utils\ViewControll;
use Validator;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */


    protected $History;
    protected $ActElement;


    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();

    }

    public function index()
    {

        return view('backend.employee.index');
    }
    
    public function getData(){

            $item =DB::table('PEGAWAI as emp')
                ->select(
                    'emp.ID_PGW AS Id',
                    'emp.NIP',
                    'emp.NAMA_PGW as Name',
                    'emp.ALAMAT_PGW as Address'
                );
    
                return Datatables::of($item)
                ->editColumn('Id', '{{$Id}}')
                ->make(true);
    
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $kabupaten =DB::table('city as mkb')
            ->select(
                'mkb.Id as Id', 
                'mkb.Name as City')
            ->where('mkb.Actived','>',0)
            ->pluck('City','Id');

        $dept =DB::table('department as ogn')
            ->select(
                'ogn.Id as Id',
                'ogn.Organisation as departemen'
            )
            ->where('ogn.Actived','>',0)
            ->pluck('departemen','Id');

        return view('backend/.employee.create',compact('kabupaten','dept'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request){

        $requestData = $request->all();
        $this->History->store(13,1,json_encode($requestData));

        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails())
        {
            return  redirect('HomeAdmin/employee/create')->withInput()->withErrors($validation->errors());
        }
        DB::table('employee')
            ->insert([
                'IdKab'=>$request->input('IdKab'),
                'IdOgnUnits'=>$request->input('IdOgnUnits'),
                'Name'=>$request->input('Name'),
                'NIP'=>$request->input('NIP'),
                'Married'=>$request->input('Married'),
                'NickName'=>$request->input('NickName'),
                'DateOfBirth'=>$this->formatDate($request->input('DateOfBirth'), 'Y-m-d'),
                'CellPhone'=>$request->input('CellPhone'),
                'HomePhone'=>$request->input('HomePhone'),
                'Address'=>$request->input('Address'),
                'Religion'=>$request->input('Religion'),
                'Status'=>$request->input('Status'),
                'Category'=>$request->input('Category'),

                'UserEntry'=>session('admin')->Id,
            ]);


        return redirect('HomeAdmin/employee')->with('flash_message', 'Employee added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $employee = DB::table('employee as emp')
            ->select('emp.Id',
                'mkb.Name as kabupaten',
                'ogn.Organisation as departemen',
                'emp.Name',
                'emp.Married',
                'emp.NickName',
                'emp.DateOfBirth',
                'emp.CellPhone',
                'emp.HomePhone',
                'emp.Address',
                'emp.Religion',
                'emp.Status',
                'usr.Username',
                'emp.CreateAt',
                'emp.Category',
                'emp.UpdateAt')
            ->leftJoin('city as mkb','mkb.Id','=','emp.IdKab')
            ->leftJoin('department as ogn','ogn.Id','=','emp.IdOgnUnits')
            ->leftJoin('users as usr','usr.Id','=','emp.UserEntry')
            ->where('emp.Actived',1)
            ->where('emp.Id',$id)
            ->first();


        return view('backend/.employee.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $employee = DB::table('employee as emp')
            ->select('emp.Id',
                'mkb.Id as IdKab',
                'ogn.Id as iddepartemen',
                'emp.Name',
                'emp.NIP',
                'emp.Married',
                'emp.NickName',
                'emp.DateOfBirth',
                'emp.CellPhone',
                'emp.HomePhone',
                'emp.Address',
                'emp.Religion',
                'emp.Status',
                'emp.CreateAt',
                'emp.UpdateAt',
                'emp.Category',
                'mkb.Id as idkabupaten',
                'mkb.Name as kabupaten')
            ->leftJoin('city as mkb','mkb.Id','=','emp.IdKab')
            ->leftJoin('department as ogn','ogn.Id','=','emp.IdOgnUnits')
            ->where('emp.Actived','>',0)
            ->where('emp.Id',$id)
            ->first();

        $kabupaten =DB::table('city as mkb')
            ->select(
                'mkb.Id as IdKab',
                'mkb.Name as kabupaten')
            ->where('mkb.Actived','>',0)
            ->pluck('kabupaten','IdKab');

        $dept =DB::table('department as ogn')
            ->select(
                'ogn.Id as iddepartemen',
                'ogn.Organisation as departemen')
            ->where('ogn.Actived','>',0)
            ->pluck('departemen','iddepartemen');
        if(!empty($employee)){
            $employee->DateOfBirth    =Carbon::parse($employee->DateOfBirth)->format('d/m/Y');
        }
        return view('backend/.employee.edit', compact('employee','kabupaten','dept'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id){

        $requestData = $request->all();
        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails()) {
            return  redirect('HomeAdmin/employee/'.$id.'/edit')->withInput()->withErrors($validation->errors());
        }
        $this->History->store(13,2,json_encode($requestData));

        DB::table('employee')->where('Id',$id)
         ->update([
                'IdKab'=>$request->input('IdKab'),
                'NIP'=>$request->input('NIP'),
                'IdOgnUnits'=>$request->input('IdOgnUnits'),
                'Name'=>$request->input('Name'),
                'Married'=>$request->input('Married'),
                'NickName'=>$request->input('NickName'),
                'DateOfBirth'=>$this->formatDate($request->input('DateOfBirth'), 'Y-m-d'),
                'CellPhone'=>$request->input('CellPhone'),
                'HomePhone'=>$request->input('HomePhone'),
                'Address'=>$request->input('Address'),
                'Religion'=>$request->input('Religion'),
                'Status'=>$request->input('Status'),
                'Category'=>$request->input('Category'),
                ]);

        return redirect('HomeAdmin/employee')->with('flash_message', 'Employee updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id){

        DB::table('employee')->where('Id',$id)
            ->update([
                'Actived'=>0
            ]);

        $this->History->store(13,3,json_encode($id));

        return redirect('HomeAdmin/employee')->with('flash_message', 'Employee deleted!');
    }

    function validation(){
        return [
            'IdKab'=>'required',
            'IdOgnUnits'=>'required',
            'Name'=>'required',
            'Married'=>'required',
            'NickName'=>'required',
            'DateOfBirth'=>'required',
            'CellPhone'=>'required',
            'HomePhone'=>'required',
            'Address'=>'required',
            'Religion'=>'required',
            'Status'=>'required',
        ];

    }
    function formatDate($date, $format) {
        return Carbon::createFromFormat('d/m/Y', $date)->format($format);
    }
}
