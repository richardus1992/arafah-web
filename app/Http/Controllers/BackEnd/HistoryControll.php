<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Carbon\Carbon;

class HistoryControll extends Controller
{
    public function store($idmodule,$action,$desc){
        DB::table('HISTORY')
            ->insert([
                'IDMODULE'=>$idmodule,
                'DESCRIPTION'=>$desc,
                'ACTION'=>$action,
                'CREATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s'),
                'USERENTRY'=>session('admin')->ID,
            ]);
    }

    public function storeapi($idmodule,$action,$desc,$userentry){
        DB::table('HISTORY')
            ->insert([
                'IDMODULE'=>$idmodule,
                'DESCRIPTION'=>$desc,
                'ACTION'=>$action,
                'CREATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s'),
                'USERENTRY'=>$userentry,
            ]);
    }

    public static function storePrintReport($idmodule){
      DB::table('printed_history')
        ->insert([
          'IdModule'=>$idmodule,
          'UserEntry'=>session('admin')->Id,
        ]);
    }
}
