<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Controllers\Utils\ViewControll;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Yajra\DataTables\DataTables;

class RegistrationOnlineControll extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $Paginate=20;
    protected $History;
    protected $ActElement;

    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();

    }
    public function index()
    {

        return view('backend.registration-online.index');
    }


    public function getData(){

        $item =DB::table('PENDAFTARAN_ONLINE as pdo')
            ->select(
                'pdo.ID_ONLINE as Id',
                'pdo.ID_USER as IdUser',
                'pdo.NO_ANTRIAN as OrdinalQueue',
                'pdo.ID_PASIEN as IdPatien',
                'pdo.NAMA_PASIEN as Name',
                'pdo.NIK as Nik',
                'pdo.TGLLAHIR_PASIEN as DateBirth',
                'pdo.ALAMAT_PASIEN as Addres',
                'pdo.KOTA_PASIEN as IdCity',
                'pdo.PROVINSI_PASIEN as IdProvince',
                'pdo.JK_PASIEN as Sex',
                'pdo.AGAMA as Religion',
                'pdo.STATUS_PERKAWINAN as Married',
                'pdo.ID_LAYANAN_RS as IdService',
                'pdo.ID_PGW as IdEmployee',
                'pdo.TGL_PELAYANAN as DateService',
                'pdo.KONFIRMASI_KEDATANGAN as Confirm',
                'pdo.KETERANGAN as Remarks',
                'pdo.NO_HP as Phone',
                'pdo.CARA_BAYAR as MethodePay',
                'pvc.NAMA_KEPENDUDUKAN as Province',
                'cty.NAMA_KEPENDUDUKAN as City',
                'ply.NAMA_LAYANAN_RS as RoomService',
                'pdo.CREATEAT as CreateAt'
            )
            ->join('KEPENDUDUKAN as pvc','pvc.ID_KEPENDUDUKAN','pdo.KOTA_PASIEN')
            ->join('KEPENDUDUKAN as cty','cty.ID_KEPENDUDUKAN','pdo.PROVINSI_PASIEN')
            ->join('JENIS_LAYANAN_RS as ply','ply.ID_LAYANAN_RS','pdo.ID_LAYANAN_RS');


        return Datatables::of($item)
            ->addColumn('action', function ($item) {
                return $item->Confirm==0 ?$this->ActElement->genActionConfirmation('register-online/1/confirm-visited?Id=',$item->Id):'';
            })
            ->editColumn('Id', '{{$Id}}')
            ->editColumn('DateBirth', function ($item) {
                return $item->DateBirth ? with(new Carbon($item->DateBirth))->format('d/m/Y') : '';
            })
            ->editColumn('DateService', function ($item) {
                return $item->DateService ? with(new Carbon($item->DateService))->format('d/m/Y') : '';
            })

            ->editColumn('Confirm', function ($item) {
                return $item->Confirm==0 ?$this->ActElement->linkButtonInfo('Belum Datang'):$this->ActElement->linkButtonActived('Suda Datang');
            })
            ->editColumn('CreateAt', function ($item) {
                return $item->CreateAt ? with(new Carbon($item->CreateAt))->format('d/m/Y H:i:s') : '';
            })
            ->escapeColumns(['*'])

            ->make(true);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function visited(Request $request){
        $id=$request->get('Id');
        $regisOnline = DB::table('PENDAFTARAN_ONLINE')
            ->select('*')
            ->where('ID_ONLINE',$id)
            ->first();

        if(!empty($regisOnline)){
            DB::begintransaction();
            try{
                $patient = DB::table('PASIEN')
                    ->select('*')
                    ->where('TGLLAHIR_PASIEN',$regisOnline->TGLLAHIR_PASIEN)
                    ->where('NAMA_PASIEN',$regisOnline->NAMA_PASIEN)
                    ->where('AGAMA',$regisOnline->AGAMA)
                    ->first();

                if(empty($patient)){
                    $idPatient=DB::table('PASIEN')
                        ->insertGetId([
                            'ID_PASIEN'=>$this->generatePatientId($regisOnline->NAMA_PASIEN),
                            'NAMA_PASIEN'=>strtoupper($regisOnline->NAMA_PASIEN),
                            'JK_PASIEN'=>$regisOnline->JK_PASIEN,
                            'TGLLAHIR_PASIEN'=>$regisOnline->TGLLAHIR_PASIEN,
                            'STATUS_PERKAWINAN'=>$regisOnline->STATUS_PERKAWINAN,
                            'ALAMAT_PASIEN'=>strtoupper($regisOnline->ALAMAT_PASIEN),
                            'propinsi_pasien'=>$regisOnline->PROVINSI_PASIEN,
                            'KABUPATEN_PASIEN'=>$regisOnline->KOTA_PASIEN,
                            'TELP_PASIEN'=>$regisOnline->NO_HP,
                            'AGAMA'=>$regisOnline->AGAMA,
                            'PGW_INPUT'=>1,
                            'TGL_INPUT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s'),

                ]);

                    DB::table('PENDAFTARAN_ONLINE')
                        ->where('ID_ONLINE',$id)
                        ->update([
                            'KONFIRMASI_KEDATANGAN'=>1,
                            'ID_PASIEN'=>$idPatient

                        ]);
                    DB::table('ANTRIAN')
                        ->insert([
                            'MAS_ID_PGW'=>$regisOnline->ID_PGW,
                            'ID_LAYANAN_RS'=>$regisOnline->ID_LAYANAN_RS,
                            'TANGGAL_JADWAL'=>$regisOnline->TGL_PELAYANAN,
                            'NO_ANTRIAN'=>random_int(500000,1000000),
                            'ID_PASIEN'=>$idPatient,
                            'ID_PGW'=>$regisOnline->PROVINSI_PASIEN

                    ]);


                }



                $this->History->store(8,3,json_encode($regisOnline));
                DB::commit();
            }catch (Exception $e){
                DB::rollback();
                return redirect('HomeAdmin/register-online')->with('error_message', 'Registrasi Online Gagal Dihapus');

            }
        }


        return redirect('HomeAdmin/register-online')->with('flash_message', 'Registrasi Online Berhasil Dihapus');
    }

    function generatePatientId($Name){
        $patient = DB::table('PASIEN')
            ->select(
                'ID_PASIEN',
                'ID'
            )
            ->where('ID_PASIEN','LIKE',strtoupper($Name[0]).'%')
            ->orderBy('ID','desc')
            ->first();


        if(empty($patient)){
            return strtoupper($Name[0]).'1000001';
        }else{
            $numb=preg_replace('/[^0-9]/', '', $patient->ID_PASIEN) * 1 + 1;
            return strtoupper($Name[0]).$numb;
        }


    }
}
