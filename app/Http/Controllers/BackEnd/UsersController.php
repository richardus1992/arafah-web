<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Ramsey\Uuid\Uuid;
use Session;
use DB;
use App\User;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Http\Controllers\Utils\ViewControll;
use Crypt;
use Validator;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */


    protected $History;
    protected $ActElement;


    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();

    }

    public function index(){
        return view('backend/.users.index');
    }

    public function getData(){

        $item =DB::table('USERS as usr')
            ->select(
                'usr.ID as Id',
                'emp.NAMA_PGW AS Name',
                'usr.USERNAME as UserName',
                'tyu.NAME as typeuser',
                'usr.CREATEAT as CreateAt',
                'usr.UPDATEAT as UpdateAt')
            ->leftJoin('PEGAWAI as emp','emp.ID_PGW','=','usr.IDEMPLOYE')
            ->leftJoin('TYPE_USERS as tyu','tyu.id','=','usr.TYPEUSER')
            ->where('usr.ACTIVED','>',0);

        return Datatables::of($item)
            ->addColumn('action', function ($item) {
                return $this->ActElement->genAction('users',$item->Id);
            })
            ->editColumn('Id', '{{$Id}}')
            ->editColumn('CreateAt', function ($item) {
                return $item->CreateAt ? with(new Carbon($item->CreateAt))->format('d-m-Y') : '';
            })
            ->editColumn('UpdateAt', function ($item) {
                return $item->UpdateAt ? with(new Carbon($item->UpdateAt))->format('d-m-Y') : '';
            })
            ->make(true);

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $employee =DB::table('PEGAWAI as emp')
            ->select('emp.ID_PGW as Id', 'emp.NAMA_PGW as employee')
            ->pluck('employee','Id');

        $typeuser =DB::table('TYPE_USERS as tyu')
            ->select('tyu.ID as Idtyu', 'tyu.NAME as typeuser')
            ->where('tyu.ACTIVED','>',0)
            ->pluck('typeuser','Idtyu');

        return view('backend/.users.create',compact('employee','typeuser'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request){

        $requestData = $request->all();

        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails())
        {
            return  redirect('users/create')->withInput()->withErrors($validation->errors());
        }
        DB::table('USERS')
            ->insert([
                'IDEMPLOYE'=>$request->input('IdEmploye'),
                'CODE'=>Uuid::uuid1()->toString(),
                'USERNAME'=>$request->input('UserName'),
                'PASSWORD'=>Crypt::encrypt($request->input('Password')),
                'REMEMBERTOKEN'=>$request->input('Remember_Token'),
                'TYPEUSER'=>$request->input('TypeUser'),
                'api_token'=>Crypt::encrypt($request->input('Password')),
                'USERENTRY'=>session('admin')->ID,
                'CREATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s'),
                'UPDATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s'),
            ]);
        $this->History->store(10,1,json_encode($requestData));

        return redirect('HomeAdmin/users')->with('flash_message', 'User added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $user = DB::table('USERS as usr')
            ->select(
                'usr.ID Id',
                'emp.ID_PGW as idemp',
                'emp.NAMA_PGW as empname',
                'usr.USERNAME as UserName',
                'usr.PASSWORD as pwd',
                'tyu.NAME as tyuname',
                'usr.CREATEAT as CreateAt',
                'usr.UPDATEAT as UpdateAt')
            ->leftJoin('PEGAWAI as emp','emp.ID','=','usr.IDEMPLOYE')
            ->leftJoin('TYPE_USERS as tyu','tyu.ID','=','usr.TYPEUSER')
            ->where('usr.ACTIVED','>',0)
            ->where('usr.ID',$id)
            ->first();

        if(!empty($user)){
            $user->CreateAt = Carbon::parse($user->CreateAt)->format('l, d-m-Y H:i:s');
            $user->UpdateAt = Carbon::parse($user->UpdateAt)->format('l, d-m-Y H:i:s');
            $user->pwd = Crypt::decrypt($user->pwd);
        }


        return view('backend/.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $user = DB::table('USERS as usr')
            ->select(
                'usr.ID AS Id',
                'emp.ID_PGW as IdEmploye',
                'usr.USERNAME AS UserName',
                'usr.PASSWORD as pwd',
                'tyu.ID as TypeUser',
                'usr.CREATEAT AS CreateAt',
                'usr.UPDATEAT AS UpdateAt')
            ->leftJoin('PEGAWAI as emp','emp.ID_PGW','=','usr.IDEMPLOYE')
            ->leftJoin('TYPE_USERS as tyu','tyu.ID','=','usr.TYPEUSER')
            ->where('usr.ACTIVED',1)
            ->where('usr.ID',$id)
            ->first();

        $employee =DB::table('PEGAWAI as emp')
            ->select('emp.ID_PGW as Id', 'emp.NAMA_PGW as employee')
            ->pluck('employee','Id');

        $typeuser =DB::table('TYPE_USERS as tyu')
            ->select('tyu.ID as Idtyu', 'tyu.NAME as typeuser')
            ->where('tyu.ACTIVED','>',0)
            ->pluck('typeuser','Idtyu');


        return view('backend/.users.edit', compact('user','employee','typeuser'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails())
        {
            return  redirect('HomeAdmin/users/'.$id.'/edit')->withInput()->withErrors($validation->errors());
        }

        DB::begintransaction();
        try{
            $user = DB::table('USERS as usr')
                ->where('usr.ID',$id)
                ->first();

            DB::table('USERS')->where('ID',$id)
                ->update([
                    'IDEMPLOYE'=>$request->input('IdEmploye'),
                    'USERNAME'=>$request->input('UserName'),
                    'PASSWORD'=>Crypt::encrypt($request->input('Password')),
                    'REMEMBERTOKEN'=>$request->input('Remember_Token'),
                    'TYPEUSER'=>$request->input('TypeUser'),
                    'api_token'=>Crypt::encrypt($request->input('Password')),
                    'UPDATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s')

                ]);
            $this->History->store(10,2,json_encode($user));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return  redirect('users/'.$id.'/edit')->withInput()->withErrors($validation->errors());
        }



        return redirect('HomeAdmin/users')->with('flash_message', 'User updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id){

        $user = DB::table('USERS as usr')
            ->where('usr.ID',$id)
            ->first();

        DB::table('USERS')->where('ID',$id)
            ->update([
                'ACTIVED'=>0
            ]);

        $this->History->store(10,3,json_encode($user));

        return redirect('HomeAdmin/users')->with('flash_message', 'User deleted!');
    }


    function validation(){
        return [
            'IdEmploye'=>'required',
            'UserName'=>'required',
            'Password'=>'required',
            'TypeUser'=>'required',
        ];

    }
}
