<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Http\Controllers\Utils\ViewControll;
use Validator;

class ComplainTypeControll extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */


    protected $History;
    protected $ActElement;


    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();

    }

    public function index()
    {

        return view('backend.complain-type.index');
    }

    public function getData(){

        $item =DB::table('JENIS_COMPLAIN as jpn')
            ->select(
                'jpn.ID AS Id',
                'jpn.JENIS as Jenis',
                'jpn.CREATEAT as CreateAt',
                'jpn.UPDATEAT as UpdateAt'
            )
            ->where('jpn.ACTIVED','>',0);

        return Datatables::of($item)
            ->addColumn('action', function ($item) {
                return $this->ActElement->genAction('complain-type',$item->Id);
            })
            ->editColumn('Id', '{{$Id}}')
            ->editColumn('CreateAt', function ($item) {
                return $item->CreateAt ? with(new Carbon($item->CreateAt))->format('d/m/Y') : '';
            })
            ->editColumn('UpdateAt', function ($item) {
                return $item->UpdateAt ? with(new Carbon($item->UpdateAt))->format('d/m/Y') : '';
            })

            ->make(true);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        
        return view('backend/.complain-type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request){

        $requestData = $request->all();
        $this->History->store(19,1,json_encode($requestData));

        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails())
        {
            return  redirect('HomeAdmin/complain-type/create')->withInput()->withErrors($validation->errors());
        }
        DB::table('JENIS_COMPLAIN')
            ->insert([
                'JENIS'=>$request->input('Jenis'),
                'CREATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s'),
                'UPDATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s'),
                'UserEntry'=>session('admin')->ID,
            ]);


        return redirect('HomeAdmin/complain-type')->with('flash_message', 'Jenis Complain added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $item = DB::table('JENIS_COMPLAIN as jpn')
            ->select(
                'jpn.ID AS Id',
                'jpn.JENIS as Jenis',
                'jpn.CREATEAT as CreateAt',
                'jpn.UPDATEAT as UpdateAt'
            )
            ->where('jpn.ID',$id)
            ->first();


        return view('backend/.complain-type.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $item = DB::table('JENIS_COMPLAIN as cpn')
            ->select(
                'cpn.*',
                'cpn.JENIS as Jenis',
                'cpn.ID as Id'
            )
            ->where('cpn.ID',$id)
            ->first();
        
        
        return view('backend/.complain-type.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id){

        $requestData = $request->all();
        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails()) {
            return  redirect('HomeAdmin/complain-type/'.$id.'/edit')->withInput()->withErrors($validation->errors());
        }
        $this->History->store(19,2,json_encode($requestData));

        DB::table('JENIS_COMPLAIN')->where('ID',$id)
            ->update([
                'JENIS'=>$request->input('Jenis'),
                'UPDATEAT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s'),
            ]);

        return redirect('HomeAdmin/complain-type')->with('flash_message', 'Jenis Complain updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id){

        DB::table('JENIS_COMPLAIN')->where('ID',$id)
            ->update([
                'ACTIVED'=>0
            ]);

        $this->History->store(19,3,json_encode($id));

        return redirect('HomeAdmin/complain-type')->with('flash_message', 'Jenis Complain deleted!');
    }

    function validation(){
        return [
            'Jenis'=>'required',
        ];

    }
    function formatDate($date, $format) {
        return Carbon::createFromFormat('d/m/Y', $date)->format($format);
    }
}
