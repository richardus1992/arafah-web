<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use App\MasterModule;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Http\Controllers\Utils\ViewControll;
use Validator;
use Cache;

class FacilitiRoomsControll extends Controller
{
    protected $History;
    protected $ActElement;


    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();

    }

    public function index(){
        return view('backend.faciliti-rooms.index');
    }

    public function getData(){
        $item =DB::table('FASILISTAS_KAMAR_RWTINAP as fkr')
            ->select(
                'fkr.ID_FASILISTAS as Id',
                'fkr.FASILISTAS as Name',
                'fkr.JUMLAH as Qty',
                'fkr.KETERANGAN as Description',
                'mkr.NAMA_ALIAS_RWT_INAP as RoomService',
                'mkr.LANTAI_RWT_INAP as FloorLevel',
                'mkr.KAPASITAS_RWT_INAP as Capacity',
                'mkrp.NAMA_KELAS_RINAP as NameClass',
                'mpv.NAMA_PAVILIUN as NamePaviliun'
            )
            ->join('MASTER_KAMAR_RAWAT_INAP as mkr','mkr.ID_KAMAR_RWT_INAP','fkr.ID_KAMAR_RWT_INAP')
            ->join('MASTER_KELAS_RAWAT_INAP as mkrp','mkrp.ID_KELAS_RINAP','mkr.ID_KELAS_RINAP')
            ->join('MASTER_PAVILIUN as mpv','mpv.ID_PAVILIUN','mkr.ID_PAVILIUN');


        return Datatables::of($item)

            ->addColumn('action', function ($item) {
                return $this->ActElement->genAction('faciliti-rooms',$item->Id);
            })
            ->make(true);

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {

        $rooms=DB::table('MASTER_KAMAR_RAWAT_INAP as mkri')
            ->select(
                'mkri.ID_KAMAR_RWT_INAP as Id',
                DB::raw('mkri.NAMA_ALIAS_RWT_INAP + ISNULL(\' Class : \' + mkrp.NAMA_KELAS_RINAP, \'\') as Name')
            )
            ->join('MASTER_KELAS_RAWAT_INAP as mkrp','mkrp.ID_KELAS_RINAP','mkri.ID_KELAS_RINAP')
            ->join('MASTER_PAVILIUN as mpv','mpv.ID_PAVILIUN','mkri.ID_PAVILIUN')
            ->pluck('Name','Id');

        return view('backend.faciliti-rooms.create',compact('rooms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request){
        $requestData = $request->all();
        $validation = Validator::make($request->all(),$this->validation());
        if ($validation->fails()) {
            return  redirect('HomeAdmin/faciliti-rooms/create')->withInput()->withErrors($validation->errors());
        }

        DB::begintransaction();
        try{
            DB::table('FASILISTAS_KAMAR_RWTINAP')
                ->insert([
                    'FASILISTAS'=>$request->input('Name'),
                    'JUMLAH'=>$request->input('Qty'),
                    'KETERANGAN'=>$request->input('Description'),
                    'ID_KAMAR_RWT_INAP'=>$request->input('RoomService')
                ]);
            $this->History->store(26,1,json_encode($requestData));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return  redirect('HomeAdmin/faciliti-rooms/create')->withInput()->withErrors($validation->errors());

        }


        return redirect('HomeAdmin/faciliti-rooms')->with('flash_message', 'Fasilistas added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $fkr = DB::table('FASILISTAS_KAMAR_RWTINAP as fkr')
            ->select(
                'fkr.ID_FASILISTAS as Id',
                'fkr.FASILISTAS as Name',
                'fkr.JUMLAH as Qty',
                'fkr.KETERANGAN as Description'
            )
            ->where('fkr.ID_FASILISTAS',$id)
            ->first();
        return view('backend/.faciliti-rooms.show', compact('fkr'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {

        $fkr =DB::table('FASILISTAS_KAMAR_RWTINAP as fkr')
            ->select(
                'fkr.ID_FASILISTAS as Id',
                'fkr.FASILISTAS as Name',
                'fkr.KETERANGAN as Description',
                'fkr.ID_KAMAR_RWT_INAP as RoomService',
                'fkr.JUMLAH as Qty'
            )
            ->where('fkr.ID_FASILISTAS',$id)
            ->first();

        $rooms=DB::table('MASTER_KAMAR_RAWAT_INAP as mkri')
            ->select(
                'mkri.ID_KAMAR_RWT_INAP as Id',
                'mkri.NAMA_ALIAS_RWT_INAP as Name'
            )
            ->pluck('Name','Id');
        return view('backend/.faciliti-rooms.edit', compact('fkr','rooms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();
        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails()) {
            return  redirect('HomeAdmin/faciliti-rooms/'.$id.'/edit')->withInput()->withErrors($validation->errors());
        }

        DB::begintransaction();
        try{
            DB::table('FASILISTAS_KAMAR_RWTINAP')
                ->where('ID_FASILISTAS',$id)
                ->update([
                    'FASILISTAS'=>$request->input('Name'),
                    'JUMLAH'=>$request->input('Qty'),
                    'KETERANGAN'=>$request->input('Description'),
                    'ID_KAMAR_RWT_INAP'=>$request->input('RoomService')
                ]);

            $this->History->store(26,2,json_encode($requestData));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return  redirect('HomeAdmin/faciliti-rooms/'.$id.'/edit')->withInput()->withErrors($validation->errors());

        }

        return redirect('HomeAdmin/faciliti-rooms')->with('flash_message', 'Fasilitas Kamar updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id){

        $city = DB::table('FASILISTAS_KAMAR_RWTINAP')
            ->select('*')
            ->where('ID_FASILISTAS',$id)
            ->first();

        DB::begintransaction();
        try{
            DB::table('FASILISTAS_KAMAR_RWTINAP')
                ->where('ID_FASILISTAS',$id)
                ->delete();

            $this->History->store(26,3,json_encode($city));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return redirect('HomeAdmin/faciliti-rooms')->with('error_message', 'Fasilitas Gagal Di Hapus');

        }
        return redirect('HomeAdmin/faciliti-rooms')->with('flash_message', 'Fasilitas Berhasil Di Hapus');


    }

    function validation(){
        return [
            'Name'=>'required',
        ];

    }
    function getLastId(){
        $location   =DB::table('FASILISTAS_KAMAR_RWTINAP')
            ->select('ID_FASILISTAS')
            ->orderBy('ID_FASILISTAS','desc')
            ->first();
        return $location->ID_KEPENDUDUKAN + 1;
    }

}

