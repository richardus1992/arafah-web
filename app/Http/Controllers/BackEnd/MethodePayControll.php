<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use App\MasterModule;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Http\Controllers\Utils\ViewControll;
use Validator;
use Cache;

class MethodePayControll extends Controller
{
    protected $History;
    protected $ActElement;


    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();

    }

    public function index()
    {
        return view('backend.methode-pay.index');
    }

    public function getData(Request $request){
        $item =DB::table('METODE_BAYAR as mtb')
            ->select(
                'mtb.ID as Id',
                'mtb.NAMA_METODE as Name'
            );


        return Datatables::of($item)

            ->addColumn('action', function ($item) {
                return $this->ActElement->genAction('methode-pay',$item->Id);
            })
            ->make(true);

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
       
        return view('backend.methode-pay.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request){

        $requestData = $request->all();


        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails()) {
            return  redirect('HomeAdmin/methode-pay/create')->withInput()->withErrors($validation->errors());
        }

        DB::begintransaction();
        try{
            DB::table('METODE_BAYAR')
                ->insert([
                    'NAMA_METODE'=>$request->input('Name')
                ]);
            $this->History->store(25,1,json_encode($requestData));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return  redirect('HomeAdmin/methode-pay/create')->withInput()->withErrors($validation->errors());

        }


        return redirect('HomeAdmin/methode-pay')->with('flash_message', 'Methode Bayar added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $districts = DB::table('KEPENDUDUKAN as dst')
            ->select(
                'dst.ID_KEPENDUDUKAN as Id',
                'dst.NAMA_KEPENDUDUKAN as Name',
                'cty.NAMA_KEPENDUDUKAN as City'
            )
            ->join('KEPENDUDUKAN as cty','cty.ID_KEPENDUDUKAN','dst.ATASAN_KEPENDUDUKAN')
            ->where('dst.ID_KEPENDUDUKAN',$id)
            ->first();
        return view('backend/.methode-pay.show', compact('methode-pay'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {

        $mtb =DB::table('METODE_BAYAR as mtb')
            ->select(
                'mtb.ID as Id',
                'mtb.NAMA_METODE as Name'
            )
            ->where('mtb.ID',$id)
            ->first();
        return view('backend/.methode-pay.edit', compact('mtb'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();
        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails()) {
            return  redirect('HomeAdmin/methode-pay/'.$id.'/edit')->withInput()->withErrors($validation->errors());
        }

        DB::begintransaction();
        try{
            DB::table('METODE_BAYAR')
                ->where('ID',$id)
                ->update([
                    'NAMA_METODE'=>$request->input('Name'),
                ]);

            $this->History->store(25,2,json_encode($requestData));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return  redirect('HomeAdmin/methode-pay/'.$id.'/edit')->withInput()->withErrors($validation->errors());

        }

        return redirect('HomeAdmin/methode-pay')->with('flash_message', 'Metode Bayar updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id){

        $city = DB::table('METODE_BAYAR')
            ->select('*')
            ->where('ID',$id)
            ->first();

        DB::begintransaction();
        try{
            DB::table('METODE_BAYAR')
                ->where('ID',$id)
                ->delete();

            $this->History->store(25,3,json_encode($city));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return redirect('HomeAdmin/methode-pay')->with('error_message', 'Metode Bayar Gagal Di Hapus');

        }
        return redirect('HomeAdmin/methode-pay')->with('flash_message', 'Metode Bayar Berhasil Di Hapus');


    }

    function validation(){
        return [
            'Name'=>'required',
        ];

    }
    function getLastId(){
        $location   =DB::table('KEPENDUDUKAN')
            ->select('ID_KEPENDUDUKAN')
            ->orderBy('ID_KEPENDUDUKAN','desc')
            ->first();


        return $location->ID_KEPENDUDUKAN + 1;
    }

}
