<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Controllers\Utils\ViewControll;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use DB;

class RoomServiceControll extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $History;
    protected $ActElement;


    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();

    }
    public function index()
    {
        return view('backend.room-service.index');
    }

    public function getData(){
        $item =DB::table('MASTER_KAMAR_RAWAT_INAP as mkri')
            ->select(
                'mkri.ID_KAMAR_RWT_INAP as Id',
                'mkri.ID_KELAS_RINAP as IdClass',
                'mkri.NAMA_ALIAS_RWT_INAP as Name',
                'mkri.LANTAI_RWT_INAP as FloorLevel',
                'mkri.KAPASITAS_RWT_INAP as Capacity',
                'mkrp.NAMA_KELAS_RINAP as NameClass',
                'mpv.NAMA_PAVILIUN as NamePaviliun'
            )
        ->join('MASTER_KELAS_RAWAT_INAP as mkrp','mkrp.ID_KELAS_RINAP','mkri.ID_KELAS_RINAP')
        ->join('MASTER_PAVILIUN as mpv','mpv.ID_PAVILIUN','mkri.ID_PAVILIUN');
        return DataTables::of($item)
            ->make(true);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
