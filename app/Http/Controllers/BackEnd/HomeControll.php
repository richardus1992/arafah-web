<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Controllers\Utils\MenuAccess;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class HomeControll extends Controller
{
    public function Index(){
        $navMenu = MenuAccess::buildNavMenu();
        return view('backend.layouts.master',compact('navMenu'));
    }

    public function blockAccess(){
        return view('backend.includes.block');
    }

    public function indexhome(){


        return view('backend.index');
    }
}
