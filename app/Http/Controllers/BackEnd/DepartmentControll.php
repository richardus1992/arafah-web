<?php

namespace App\Http\Controllers\BackEnd;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use App\OgnUnit;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Http\Controllers\Utils\ViewControll;
use Validator;


class DepartmentControll extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */


    protected $History;
    protected $ActElement;


    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();

    }

    public function index()
    {

        return view('backend/.department.index');
    }

    public function getData(){

        $item =DB::table('department as dpt')
            ->select(
                'dpt.Id',
                'dpt.Organisation',
                'dpt.JobDisk',
                'usr.UserName as UserEntry',
                'dpt.CreateAt',
                'dpt.UpdateAt')

            ->where('dpt.Actived','>',0)
            ->leftjoin('users as usr','usr.Id','=','dpt.UserEntry')
            ->orderBy('dpt.Id','desc');

        return Datatables::of($item)
            ->addColumn('action', function ($item) {
                return $this->ActElement->genAction('department',$item->Id);
            })
            ->editColumn('Id', '{{$Id}}')
            ->editColumn('CreateAt', function ($item) {
                return $item->CreateAt ? with(new Carbon($item->CreateAt))->format('d/m/Y') : '';
            })
            ->editColumn('UpdateAt', function ($item) {
                return $item->UpdateAt ? with(new Carbon($item->UpdateAt))->format('d/m/Y') : '';
            })
            ->make(true);

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('backend/.department.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request){

        $requestData = $request->all();
        $this->History->store(14,1,json_encode($requestData));

        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails())
        {
            return  redirect('HomeAdmin/department/create')->withInput()->withErrors($validation->errors());
        }
        DB::table('department')
            ->insert([
                'Parent'=>0,
                'Organisation'=>$request->input('Organisation'),
                'JobDisk'=>$request->input('JobDisk'),
                'Remarks'=>$request->input('Remarks'),
                'UserEntry'=>session('admin')->Id,
            ]);


        return redirect('HomeAdmin/department')->with('flash_message', 'OgnUnit added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $ognunit = DB::table('department as ogn')
            ->select(
                'ogn.Id',
                'ogn.Parent',
                'ogn.Organisation',
                'ogn.JobDisk',
                'ogn.Remarks',
                'usr.Username',
                'ogn.CreateAt',
                'ogn.UpdateAt'
            )
            ->leftJoin('users as usr','usr.Id','=','ogn.UserEntry')
            ->where('ogn.Actived','>',0)
            ->where('ogn.Id',$id)
            ->first();


        return view('backend/.department.show', compact('ognunit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $ognunit = DB::table('department')
            ->select(
                'Id',
                'Parent',
                'Organisation',
                'JobDisk',
                'Remarks',
                'CreateAt',
                'UpdateAt'
            )
            ->where('Actived','>',0)
            ->where('Id',$id)
            ->first();
        return view('backend/.department.edit', compact('ognunit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();
        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails())
        {
            return  redirect('HomeAdmin/department/'.$id.'/edit')->withInput()->withErrors($validation->errors());
        }
        $this->History->store(14,2,json_encode($requestData));

        DB::table('department')->where('Id',$id)
            ->update([
                'Parent'=>0,
                'Organisation'=>$request->input('Organisation'),
                'JobDisk'=>$request->input('JobDisk'),
                'Remarks'=>$request->input('Remarks'),

            ]);

        return redirect('HomeAdmin/department')->with('flash_message', 'DepartMent updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id){

        DB::table('department')->where('Id',$id)
            ->update([
                'Actived'=>0
            ]);

        $this->History->store(14,3,json_encode($id));

        return redirect('HomeAdmin/department')->with('flash_message', 'Department deleted!');
    }


    function validation(){
        return [
            'Organisation'=>'required',
        ];

    }
}
