<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use App\MasterModule;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Http\Controllers\Utils\ViewControll;
use Validator;

class ProvinceControll extends Controller
{
    
	protected $History;
    protected $ActElement;


    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();

    }

    public function index()
    {
        return view('backend/.province.index');
    }
    
    public function getData(){
    
        $item =DB::table('KEPENDUDUKAN as pvc')
            ->select(
                'pvc.ID_KEPENDUDUKAN as Id',
                'pvc.NAMA_KEPENDUDUKAN as Name')
            ->where('pvc.ATASAN_KEPENDUDUKAN',null);

            return Datatables::of($item)
        
            ->addColumn('action', function ($item) {
                return $this->ActElement->genAction('province',$item->Id);
            })
            ->make(true);
    
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('backend/.province.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request){

        $requestData = $request->all();


        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails()) {
            return  redirect('HomeAdmin/province/create')->withInput()->withErrors($validation->errors());
        }

        DB::begintransaction();
        try{
            DB::table('KEPENDUDUKAN')
                ->insert([
                    'NAMA_KEPENDUDUKAN'=>$request->input('Name'),
                    'ID_KEPENDUDUKAN'=>$this->getLastId()
                ]);

            $this->History->store(21,1,json_encode($requestData));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return  redirect('HomeAdmin/province/create')->withInput()->withErrors($validation->errors());

        }


        return redirect('HomeAdmin/province')->with('flash_message', 'Provinsi added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $province = DB::table('KEPENDUDUKAN as pvc')
            ->select(
                'pvc.ID_KEPENDUDUKAN as Id',
                'pvc.NAMA_KEPENDUDUKAN as Name')
            ->where('pvc.ID_KEPENDUDUKAN',$id)
            ->first();


        return view('backend/.province.show', compact('province'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $province =DB::table('KEPENDUDUKAN')
            ->SELECT(
                'ID_KEPENDUDUKAN as Id',
                'NAMA_KEPENDUDUKAN as Name'
                )
            ->where('ID_KEPENDUDUKAN',$id)
            ->first();
        return view('backend/.province.edit', compact('province'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();
        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails()) {
            return  redirect('HomeAdmin/province/'.$id.'/edit')->withInput()->withErrors($validation->errors());
        }

        DB::begintransaction();
        try{
            DB::table('KEPENDUDUKAN')
                ->where('ID_KEPENDUDUKAN',$id)
                ->update([
                    'NAMA_KEPENDUDUKAN'=>$request->input('Name'),

                ]);

            $this->History->store(21,2,json_encode($requestData));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return  redirect('HomeAdmin/province/'.$id.'/edit')->withInput()->withErrors($validation->errors());

        }

        return redirect('HomeAdmin/province')->with('flash_message', 'Provinsi updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id){

        $province = DB::table('KEPENDUDUKAN')
            ->select('*')
            ->where('ID_KEPENDUDUKAN',$id)
            ->first();

            DB::begintransaction();
        try{
            DB::table('KEPENDUDUKAN')
                ->where('ID_KEPENDUDUKAN',$id)
                ->delete();

            $this->History->store(21,3,json_encode($province));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return redirect('HomeAdmin/province')->with('error_message', 'Provinsi Gagal Dihapus');

        }
        return redirect('HomeAdmin/province')->with('flash_message', 'Provinsi Berhasil Dihapus');


    }

    function validation(){
        return [
            'Name'=>'required',
        ];

    }

    function getLastId(){
        $location   =DB::table('KEPENDUDUKAN')
            ->select('ID_KEPENDUDUKAN')
            ->orderBy('ID_KEPENDUDUKAN','desc')
            ->first();


        return $location->ID_KEPENDUDUKAN + 1;
    }

}
