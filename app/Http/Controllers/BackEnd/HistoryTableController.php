<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use App\HistoryTable;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\Utils\ViewControll;
use Validator;
use Yajra\DataTables\Facades\DataTables;

class HistoryTableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */


    protected $History;
    protected $ActElement;


    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();

    }

    public function index()
    {

        $module=DB::table('module as module')
            ->select(
                'Id',
                'Name')
            ->pluck('Name','Name');
        return view('backend/.history-table.index', compact('module'));
    }
    
    public function getData(Request $request){
    
            $item =DB::table('HISTORY as hsy')
                ->select(
                    'hsy.IDMODULE as Id',
                    'mdl.Name',
                    'hsy.DESCRIPTION as Description',
                    'hsy.ACTION as Action',
                    'usr.USERNAME as UserEntry',
                    'hsy.CREATEAT as CreateAt'
                    )
                ->leftJoin('MODULE as mdl','mdl.ID','=','hsy.IDMODULE')
                ->leftJoin('USERS as usr','usr.id','=','hsy.USERENTRY');
    
                return DataTables::of($item)
                    ->filter(function ($query) use ($request) {

                        if ($request->has('Name') && $request->get('Name') != '') {
                            $query->where('mdl.Name', 'like', "%{$request->get('Name')}%");
                        }



                    })
                ->editColumn('Id', '{{$Id}}')
                    ->editColumn('CreateAt', function ($item) {
                        return $item->CreateAt ? with(new Carbon($item->CreateAt))->format('d/m/Y') : '';
                    })
                ->make(true);
    
        }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('backend/.history-table.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request){

        $requestData = $request->all();
        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails()){
            return redirect('history-table/create')->withInput()->withErrors($validation->errors());
        }
        $this->History->store(1,1,json_encode($requestData));

        $this->validation($request);
        DB::table('HistoryTable')
            ->insert([
                'IdModule'=>$request->input('IdModule'),
                'Description'=>$request->input('Description'),
                'Action'=>$request->input('Action'),
                'UserEntry'=>$request->input('UserEntry'),
                'CreateAt'=>$request->input('create_at'),
            ]);

        return redirect('HomeAdmin/history-table')->with('flash_message', 'HistoryTable added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $historytable = DB::table('HistoryTable')
                        ->where('Actived',1)
                        ->where('Id',$id)
                        ->first();


        return view('backend/.history-table.show', compact('historytable'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $historytable = DB::table('HistoryTable')
                        ->where('Actived',1)
                        ->where('Id',$id)
                        ->first();
        return view('backend/.history-table.edit', compact('historytable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();
        $validation = Validator::make($request->all(),$this->validation());
        if ($validation->fails())
            {
                return redirect('HomeAdmin/history-table/create')->withInput()->withErrors($validation->errors());
            }
        $this->History->store(1,2,json_encode($requestData));

        DB::table('HistoryTable')->where('Id',$id)
         ->update([
                'IdModule'=>$request->input('IdModule'),
                'Description'=>$request->input('Description'),
                'Action'=>$request->input('Action'),
                'UserEntry'=>$request->input('UserEntry'),
                'create_at'=>$request->input('create_at'),
                
                ]);

        return redirect('HomeAdmin/history-table')->with('flash_message', 'HistoryTable updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id){

        DB::table('HistoryTable')->where('Id',$id)
            ->update([
                'Actived'=>0
            ]);

        $this->History->store(1,3,json_encode($id));

        return redirect('HomeAdmin/history-table')->with('flash_message', 'HistoryTable deleted!');
    }

    public function validation(){
        return[
                'IdModule'=>'required',
                'Description'=>'required',
                'Action'=>'required',
                'UserEntry'=>'required',
                'create_at'=>'required',


        ];
    }
}
