<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use App\MasterModule;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Http\Controllers\Utils\ViewControll;
use Validator;

class CountryControll extends Controller
{

    protected $History;
    protected $ActElement;


    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();

    }

    public function index()
    {
        return view('backend/.country.index');
    }

    public function getData(Request $request){

        $item =DB::table('country as pvc')
            ->select(
                'pvc.Id',
                'pvc.Name',
                'usr.UserName as UserEntry',
                'pvc.CreateAt',
                'pvc.UpdateAt')
            ->leftjoin('users as usr','usr.Id','=','pvc.UserEntry')
            ->where('pvc.Actived','>',0)
            ->orderBy('pvc.Id','desc');

        return Datatables::of($item)

            ->addColumn('action', function ($item) {
                return $this->ActElement->genAction('country',$item->Id);
            })
            ->editColumn('Id', '{{$Id}}')
            ->editColumn('CreateAt', function ($item) {
                return $item->CreateAt ? with(new Carbon($item->CreateAt))->format('d/m/Y') : '';
            })
            ->editColumn('UpdateAt', function ($item) {
                return $item->UpdateAt ? with(new Carbon($item->UpdateAt))->format('d/m/Y') : '';
            })
            ->make(true);

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $country    =DB::table('country')
            ->select(
                'Id',
                'Name'
            )
            ->where('Actived','>',0)
            ->pluck('Name','Id');
        return view('backend/.country.create',compact('country'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request){

        $requestData = $request->all();


        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails()) {
            return  redirect('HomeAdmin/country/create')->withInput()->withErrors($validation->errors());
        }

        DB::begintransaction();
        try{
            DB::table('country')
                ->insert([
                    'Name'=>$request->input('Name'),
                    'UserEntry'=>session('admin')->Id,
                ]);

            $this->History->store(18,1,json_encode($requestData));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return  redirect('HomeAdmin/country/create')->withInput()->withErrors($validation->errors());

        }


        return redirect('HomeAdmin/country')->with('flash_message', 'Provinsi added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $country = DB::table('country as pvc')
            ->select(
                'pvc.Id',
                'pvc.Name',
                'usr.UserName as UserEntry',
                'pvc.CreateAt',
                'pvc.UpdateAt')
            ->leftjoin('users as usr','usr.Id','=','pvc.UserEntry')
            ->where('pvc.Actived',1)
            ->where('pvc.Id',$id)
            ->first();

        if(!empty($country)){
            $country->CreateAt = Carbon::parse($country->CreateAt)->format('l, d-m-Y H:i:s');
            $country->UpdateAt = Carbon::parse($country->UpdateAt)->format('l, d-m-Y H:i:s');
        }
        return view('backend/.country.show', compact('country'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $country =DB::table('country')
            ->where('Actived','>',0)
            ->where('Id',$id)
            ->first();
        return view('backend/.country.edit', compact('country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();
        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails()) {
            return  redirect('HomeAdmin/country/'.$id.'/edit')->withInput()->withErrors($validation->errors());
        }

        DB::begintransaction();
        try{
            DB::table('country')
                ->where('Id',$id)
                ->update([
                    'Name'=>$request->input('Name'),

                ]);

            $this->History->store(18,2,json_encode($requestData));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return  redirect('HomeAdmin/country/'.$id.'/edit')->withInput()->withErrors($validation->errors());

        }

        return redirect('HomeAdmin/country')->with('flash_message', 'Provinsi updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id){

        $country = DB::table('country')
            ->select('*')
            ->where('Id',$id)
            ->first();

        DB::begintransaction();
        try{
            DB::table('country')
                ->where('Id',$id)
                ->update([
                    'Actived'=>0

                ]);

            $this->History->store(18,3,json_encode($country));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return redirect('HomeAdmin/country')->with('error_message', 'Provinsi Gagal Dihapus');

        }
        return redirect('HomeAdmin/country')->with('flash_message', 'Provinsi Berhasil Dihapus');


    }

    function validation(){
        return [
            'Name'=>'required',
        ];

    }
}
