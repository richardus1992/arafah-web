<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use DB;
use App\MasterModule;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\Http\Controllers\Utils\ViewControll;
use Validator;


class DistrictsControll extends Controller
{
    protected $History;
    protected $ActElement;


    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();

    }

    public function index()
    {
        return view('backend/.districts.index');
    }
    
    public function getData(Request $request){
    
        $item =DB::table('KEPENDUDUKAN as dst')
            ->select(
                'dst.ID_KEPENDUDUKAN as Id',
                'dst.NAMA_KEPENDUDUKAN as Name',
                'cty.NAMA_KEPENDUDUKAN as City'
            )
            ->join('KEPENDUDUKAN as cty','cty.ID_KEPENDUDUKAN','dst.ATASAN_KEPENDUDUKAN')
            ->where('dst.JENIS_KEPENDUDUKAN',3);


        return Datatables::of($item)
        
            ->addColumn('action', function ($item) {
                return $this->ActElement->genAction('districts',$item->Id);
            })
            ->make(true);
    
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $city =DB::table('KEPENDUDUKAN as pvc')
            ->select(
                'pvc.NAMA_KEPENDUDUKAN as Name',
                'pvc.ID_KEPENDUDUKAN as Id'
            )
            ->where('pvc.JENIS_KEPENDUDUKAN',2)
            ->pluck('Name','Id');
        return view('backend/.districts.create',compact('city'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request){

        $requestData = $request->all();


        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails()) {
            return  redirect('HomeAdmin/districts/create')->withInput()->withErrors($validation->errors());
        }

        DB::begintransaction();
        try{
            DB::table('KEPENDUDUKAN')
                ->insert([
                    'ATASAN_KEPENDUDUKAN'=>$request->input('IdCity'),
                    'NAMA_KEPENDUDUKAN'=>$request->input('Name'),
                    'JENIS_KEPENDUDUKAN'=>3,
                    'ID_KEPENDUDUKAN'=>$this->getLastId()
                ]);
            $this->History->store(23,1,json_encode($requestData));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return  redirect('HomeAdmin/districts/create')->withInput()->withErrors($validation->errors());

        }


        return redirect('HomeAdmin/districts')->with('flash_message', 'Kecamatan added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $districts = DB::table('KEPENDUDUKAN as dst')
            ->select(
                'dst.ID_KEPENDUDUKAN as Id',
                'dst.NAMA_KEPENDUDUKAN as Name',
                'cty.NAMA_KEPENDUDUKAN as City'
            )
            ->join('KEPENDUDUKAN as cty','cty.ID_KEPENDUDUKAN','dst.ATASAN_KEPENDUDUKAN')
            ->where('dst.ID_KEPENDUDUKAN',$id)
            ->first();
        return view('backend/.districts.show', compact('districts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $city =DB::table('KEPENDUDUKAN as pvc')
            ->select(
                'pvc.NAMA_KEPENDUDUKAN as Name',
                'pvc.ID_KEPENDUDUKAN as Id'
            )
            ->where('pvc.JENIS_KEPENDUDUKAN',2)
            ->pluck('Name','Id');
        $districts =DB::table('KEPENDUDUKAN as cty')
            ->select(
                'cty.ID_KEPENDUDUKAN as Id',
                'cty.NAMA_KEPENDUDUKAN as Name',
                'cty.ATASAN_KEPENDUDUKAN as IdCity'
            )
            ->where('cty.ID_KEPENDUDUKAN',$id)
            ->first();
        return view('backend/.districts.edit', compact('city','districts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();
        $validation = Validator::make($request->all(),$this->validation());

        if ($validation->fails()) {
            return  redirect('HomeAdmin/districts/'.$id.'/edit')->withInput()->withErrors($validation->errors());
        }

        DB::begintransaction();
        try{
            DB::table('KEPENDUDUKAN')
                ->where('ID_KEPENDUDUKAN',$id)
                ->update([
                    'ATASAN_KEPENDUDUKAN'=>$request->input('IdCity'),
                    'NAMA_KEPENDUDUKAN'=>$request->input('Name'),
                    'JENIS_KEPENDUDUKAN'=>3,
                    
                ]);

            $this->History->store(23,2,json_encode($requestData));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return  redirect('HomeAdmin/districts/'.$id.'/edit')->withInput()->withErrors($validation->errors());

        }

        return redirect('HomeAdmin/districts')->with('flash_message', 'Kecamatan updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id){

        $city = DB::table('KEPENDUDUKAN')
            ->select('*')
            ->where('ID_KEPENDUDUKAN',$id)
            ->first();

            DB::begintransaction();
        try{
            DB::table('KEPENDUDUKAN')
                ->where('ID_KEPENDUDUKAN',$id)
                ->delete();

            $this->History->store(23,3,json_encode($city));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return redirect('HomeAdmin/districts')->with('error_message', 'Kecamatan Gagal Di Hapus');

        }
        return redirect('HomeAdmin/districts')->with('flash_message', 'Kecamatan Berhasil Di Hapus');


    }

    function validation(){
        return [
            'Name'=>'required',
            'IdCity'=>'required',
        ];

    }
    function getLastId(){
        $location   =DB::table('KEPENDUDUKAN')
            ->select('ID_KEPENDUDUKAN')
            ->orderBy('ID_KEPENDUDUKAN','desc')
            ->first();


        return $location->ID_KEPENDUDUKAN + 1;
    }

}
