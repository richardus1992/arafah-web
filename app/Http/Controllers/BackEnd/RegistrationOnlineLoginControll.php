<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Controllers\Utils\ViewControll;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Crypt;
use Ramsey\Uuid\Uuid;
use Yajra\DataTables\DataTables;
use Validator;

class RegistrationOnlineLoginControll extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $Paginate=20;
    protected $History;
    protected $ActElement;

    public function __construct()
    {

        $this->History = new HistoryControll();
        $this->ActElement = new ViewControll();

    }
    public function index()
    {

        return view('backend.registration-online-login.index');
    }


    public function getData(){

        $item =DB::table('PENDAFTARAN_ONLINE as pdo')
            ->select(
                'pdo.ID_ONLINE as Id',
                'pdo.ID_USER as IdUser',
                'pdo.NO_ANTRIAN as OrdinalQueue',
                'pdo.ID_PASIEN as IdPatien',
                'pdo.NAMA_PASIEN as Name',
                'pdo.NIK as Nik',
                'pdo.TGLLAHIR_PASIEN as DateBirth',
                'pdo.ALAMAT_PASIEN as Addres',
                'pdo.KOTA_PASIEN as IdCity',
                'pdo.PROVINSI_PASIEN as IdProvince',
                'pdo.JK_PASIEN as Sex',
                'pdo.AGAMA as Religion',
                'pdo.STATUS_PERKAWINAN as Married',
                'pdo.ID_LAYANAN_RS as IdService',
                'pdo.ID_PGW as IdEmployee',
                'pdo.TGL_PELAYANAN as DateService',
                'pdo.KONFIRMASI_KEDATANGAN as Confirm',
                'pdo.KETERANGAN as Remarks',
                'pdo.NO_HP as Phone',
                'pdo.CARA_BAYAR as MethodePay',
                'pdo.CREATEAT as CreateAt',
                'pvc.NAMA_KEPENDUDUKAN as Province',
                'cty.NAMA_KEPENDUDUKAN as City',
                'ply.NAMA_LAYANAN_RS as RoomService',
                'csr.USERNAME as UserName',
                'csr.PASSWORD as Password'

            )
            ->join('KEPENDUDUKAN as pvc','pvc.ID_KEPENDUDUKAN','pdo.KOTA_PASIEN')
            ->join('KEPENDUDUKAN as cty','cty.ID_KEPENDUDUKAN','pdo.PROVINSI_PASIEN')
            ->join('JENIS_LAYANAN_RS as ply','ply.ID_LAYANAN_RS','pdo.ID_LAYANAN_RS')
            ->join('PENDAFTAR_LOGIN as csr','csr.ID_USER','pdo.ID_USER')
            ->where('pdo.ID_USER','<>','0');

        return Datatables::of($item)
            ->addColumn('action', function ($item) {
                return $this->ActElement->genAction('register-online-login',$item->Id);
            })
            ->editColumn('Id', '{{$Id}}')
            ->editColumn('Password', function ($item) {
                return Crypt::decrypt($item->Password);
            })
            ->editColumn('DateBirth', function ($item) {
                return $item->DateBirth ? with(new Carbon($item->DateBirth))->format('d/m/Y') : '';
            })
            ->editColumn('DateService', function ($item) {
                return $item->DateService ? with(new Carbon($item->DateService))->format('d/m/Y') : '';
            })

            ->editColumn('CreateAt', function ($item) {
                return $item->CreateAt ? with(new Carbon($item->CreateAt))->format('d/m/Y H:i:s') : '';
            })

            ->editColumn('Confirm', function ($item) {
                return $item->Confirm==0 ?$this->ActElement->linkButtonInfo('Belum Datang'):$this->ActElement->linkButtonActived('Suda Datang');
            })
            ->escapeColumns(['*'])

            ->make(true);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        $regionline=DB::table('PENDAFTARAN_ONLINE')
                ->select(
                    DB::raw('NAMA_PASIEN + ISNULL(\' Alamat : \' + ALAMAT_PASIEN, \'\') as Name'),
                    DB::raw('cast( ID_ONLINE as varchar) + ISNULL(\'|\' + NAMA_PASIEN, \'\') as Id')
                )
           ->where('ID_PASIEN',null)
           ->Orwhere('ID_PASIEN','0')
           ->Orwhere('ID_PASIEN','')
           ->pluck('Name','Id');




        return view('backend.registration-online-login.create',compact('regionline'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $regisOnline = explode('|', $request->input('IdRegisOnline'));
        $idRegisOnline = $regisOnline[0];
        $requestData = $request->all();
        $validation = Validator::make($request->all(),$this->validationRules(0));
        if ($validation->fails()) {
            return  redirect('HomeAdmin/register-online-login/create')->withInput()->withErrors($validation->errors());
        }

        DB::begintransaction();
        try{

            DB::table('PENDAFTARAN_ONLINE')
                ->where('ID_ONLINE',$idRegisOnline)
                ->UPDATE([
                    'ID_PASIEN'=>$request->input('IdPatient')
                ]);

            $this->History->store(8,3,json_encode($requestData));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return redirect('HomeAdmin/register-online-login')->with('error_message', 'Registrasi Online Gagal Dihapus');

        }
        return redirect('HomeAdmin/register-online-login')->with('flash_message', 'Registrasi Online Berhasil Tambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $item=DB::table('CUSTOMER_LOGIN as clo')
            ->select(
                'clo.USERNAME as UserName',
                'clo.PASSWORD as Password',
                'clo.ID as Id',
                'pdo.NAMA_PASIEN as PatientName',
                DB::raw('cast( pdo.ID_ONLINE as varchar) + ISNULL(\'|\' + pdo.NAMA_PASIEN, \'\') as IdRegisOnline'),
                'pdo.ID_PASIEN as IdPatient'


            )
            ->join('PENDAFTARAN_ONLINE as pdo','pdo.ID_ONLINE','clo.IDCUSTOMER')
            ->where('pdo.ID_ONLINE',$id)
            ->first();
        if(!empty($item)){
            $item->Password =Crypt::decrypt($item->Password);
        }


        $regionline=DB::table('PENDAFTARAN_ONLINE')
            ->select(
                DB::raw('NAMA_PASIEN + ISNULL(\' Alamat : \' + ALAMAT_PASIEN, \'\') as Name'),
                DB::raw('cast( ID_ONLINE as varchar) + ISNULL(\'|\' + NAMA_PASIEN, \'\') as Id')
            )
            ->where('ID_ONLINE','=',$id)
            ->Orwhere(function ($q) {
                $q->where('ID_PASIEN',null)
                    ->Orwhere('ID_PASIEN','0');
            })
            ->pluck('Name','Id');




        return view('backend.registration-online-login.edit',compact('item','regionline'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();
        $validation = Validator::make($request->all(),$this->validationRules($id));

        if ($validation->fails()) {
            return  redirect('HomeAdmin/register-online-login/'.$id.'/edit')->withInput()->withErrors($validation->errors());
        }
        $regisOnline = explode('|', $request->input('IdRegisOnline'));
        $idRegisOnline = $regisOnline[0];
        DB::begintransaction();
        try{


            DB::table('CUSTOMER_LOGIN')
                ->where('ID',$id)
                ->update([
                    'USERNAME'=>$request->input('UserName'),
                    'PASSWORD'=>Crypt::encrypt($request->input('Password')),
                    'api_token'=>Crypt::encrypt($request->input('Password')),
                ]);

            $this->History->store(8,3,json_encode($requestData));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return redirect('HomeAdmin/register-online-login')->with('error_message', 'Registrasi Online Gagal Dihapus');

        }
        return redirect('HomeAdmin/register-online-login')->with('flash_message', 'Registrasi Online Berhasil Tambahkan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::begintransaction();
        try{


            DB::table('CUSTOMER_LOGIN')
                ->where('IDCUSTOMER',$id)
                ->delete();

            DB::table('PENDAFTARAN_ONLINE')
                ->where('ID_ONLINE',$id)
                ->update([
                    'ID_PASIEN'=>'0'
                ]);

            $this->History->store(8,3,json_encode($id));
            DB::commit();
        }catch (Exception $e){
            DB::rollback();
            return redirect('HomeAdmin/register-online-login')->with('error_message', 'Registrasi Online Gagal Dihapus');

        }
        return redirect('HomeAdmin/register-online-login')->with('flash_message', 'Registrasi Online Berhasil Tambahkan');

    }

    public function visited(Request $request){
        $id=$request->get('Id');
        $regisOnline = DB::table('PENDAFTARAN_ONLINE')
            ->select('*')
            ->where('ID_ONLINE',$id)
            ->first();

        if(!empty($regisOnline)){
            DB::begintransaction();
            try{
                $patient = DB::table('PASIEN')
                    ->select('*')
                    ->where('TGLLAHIR_PASIEN',$regisOnline->TGLLAHIR_PASIEN)
                    ->where('NAMA_PASIEN',$regisOnline->NAMA_PASIEN)
                    ->where('AGAMA',$regisOnline->AGAMA)
                    ->first();

                if(empty($patient)){
                    $idPatient=DB::table('PASIEN')
                        ->insertGetId([
                            'ID_PASIEN'=>$this->generatePatientId($regisOnline->NAMA_PASIEN),
                            'NAMA_PASIEN'=>strtoupper($regisOnline->NAMA_PASIEN),
                            'JK_PASIEN'=>$regisOnline->JK_PASIEN,
                            'TGLLAHIR_PASIEN'=>$regisOnline->TGLLAHIR_PASIEN,
                            'STATUS_PERKAWINAN'=>$regisOnline->STATUS_PERKAWINAN,
                            'ALAMAT_PASIEN'=>strtoupper($regisOnline->ALAMAT_PASIEN),
                            'propinsi_pasien'=>$regisOnline->PROVINSI_PASIEN,
                            'KABUPATEN_PASIEN'=>$regisOnline->KOTA_PASIEN,
                            'TELP_PASIEN'=>$regisOnline->NO_HP,
                            'AGAMA'=>$regisOnline->AGAMA,
                            'PGW_INPUT'=>1,
                            'TGL_INPUT'=>Carbon::parse(Carbon::now())->format('Y-m-d H:i:s'),

                        ]);

                    DB::table('PENDAFTARAN_ONLINE')
                        ->where('ID_ONLINE',$id)
                        ->update([
                            'KONFIRMASI_KEDATANGAN'=>1,
                            'ID_PASIEN'=>$idPatient

                        ]);
                    DB::table('ANTRIAN')
                        ->insert([
                            'MAS_ID_PGW'=>$regisOnline->ID_PGW,
                            'ID_LAYANAN_RS'=>$regisOnline->ID_LAYANAN_RS,
                            'TANGGAL_JADWAL'=>$regisOnline->TGL_PELAYANAN,
                            'NO_ANTRIAN'=>random_int(500000,1000000),
                            'ID_PASIEN'=>$idPatient,
                            'ID_PGW'=>$regisOnline->PROVINSI_PASIEN

                        ]);


                }

                $this->History->store(8,3,json_encode($regisOnline));
                DB::commit();
            }catch (Exception $e){
                DB::rollback();
                return redirect('HomeAdmin/register-online-login')->with('error_message', 'Registrasi Online Gagal Tambahkan');

            }
        }


        return redirect('HomeAdmin/register-online-login')->with('flash_message', 'Registrasi Online Berhasil Tambahkan');
    }

    function generatePatientId($Name){
        $patient = DB::table('PASIEN')
            ->select(
                'ID_PASIEN',
                'ID'
            )
            ->where('ID_PASIEN','LIKE',strtoupper($Name[0]).'%')
            ->orderBy('ID','desc')
            ->first();


        if(empty($patient)){
            return strtoupper($Name[0]).'1000001';
        }else{
            $numb=preg_replace('/[^0-9]/', '', $patient->ID_PASIEN) * 1 + 1;
            return strtoupper($Name[0]).$numb;
        }


    }



    function validationRules ($id) {
        if($id==0){
            return [
                'IdPatient'=>'required',
            ];
        }else{
            return [
                'UserName'=>'unique:CUSTOMER_LOGIN,USERNAME,' . $id,
                'IdPatient'=>'required',
            ];
        }

}

    public function fetchDistrict(Request $request){
        $id =$request->get('Id');
        $output = '';
        $disrict =DB::table('KEPENDUDUKAN as dst')
            ->select(
                'dst.ID_KEPENDUDUKAN as Id',
                'dst.NAMA_KEPENDUDUKAN as Name'
            )
            ->where('dst.ATASAN_KEPENDUDUKAN',$id)
            ->get();

        foreach($disrict as $row)
        {
            $output .= '<option value="'.$row->Id.'">'.$row->Name.'</option>';
        }



        echo $output;

    }

    public function fetchPatient(Request $request){
        $value =$request->get('search');
        $output = '';
        $patien=DB::table('PASIEN')
            ->select(
                'ID_PASIEN as Id',
                DB::raw('NAMA_PASIEN + ISNULL(\' Alamat : \' + ALAMAT_PASIEN, \'\') as Name')
            )
            ->where('NAMA_PASIEN','LIKE',$value.'%')
//            ->where('NAMA_PASIEN','LIKE','Bobi%')
            ->get();

        foreach($patien as $row)
        {
            $output .= '<option value="'.$row->Id.'">'.$row->Name.'</option>';
        }

        echo $output;

    }
}
