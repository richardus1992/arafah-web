<?php
/**
 * Created by PhpStorm.
 * User: IRUR
 * Date: 17/12/2017
 * Time: 20:45
 */

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers\Utils\MenuAccess;

class UserAccess
{

  public function handle($request, Closure $next)
  {
    if(MenuAccess::preventAccessMenu($request)){
      return redirect('HomeAdmin/block');
    }

    return $next($request);
  }
}