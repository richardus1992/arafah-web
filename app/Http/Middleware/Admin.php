<?php

namespace App\Http\Middleware;

use Closure;


class Admin
{

    public function handle($request, Closure $next){
        if(!session()->get('admin'))
        {
            return redirect()->to('manage');
        }
        return $next($request);
    }
}
