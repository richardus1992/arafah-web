<fieldset>
    <div class="row">
        <div class="col-md-12">
            <div class="thumbnail">
                @if(!empty($item->FileImage))
                    <img src="{{asset($item->FileImage)}}" class="img-responsive">
                @endif
                <div class="caption">
                    <h3>{{$item->ComplainType}}</h3>
                    <p>{{$item->ContentComplain}}.</p>

                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group {{ $errors->has('ComplainType') ? 'has-error' : ''}}">
                {!! Form::label('ComplainType', 'Jenis', ['class' => 'control-label']) !!}
                <small class="text-danger" style="float: right;">* Wajib Di Isi</small>
                <label class="input">
                    {!! Form::text('ComplainType', null,['class' => 'form-control','readonly']) !!}
                    {!! $errors->first('ComplainType', '<p class="help-block">:message</p>') !!}
                </label>

            </div>

        </div>
        <div class="col-md-12">
            <div class="form-group {{ $errors->has('Complain') ? 'has-error' : ''}}">
                {!! Form::label('Complain', 'Balas Komplain', ['class' => 'control-label']) !!}
                <small class="text-danger" style="float: right;">* Wajib Di Isi</small>
                <label class="input">
                    {!! Form::textarea('Complain', null,  ['class' => 'form-control','rows'=>'3']) !!}
                    {!! $errors->first('Complain', '<p class="help-block">:message</p>') !!}
                </label>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Upload Foto</label>
                <input class="input-file-upload" id="input-21" type="file" accept="image/*" data-show-upload="false" name="FileAttachment">
            </div>
        </div>
        {!! Form::hidden('IdTypeComplain', null,  ['class' => 'form-control']) !!}
        {!! Form::hidden('IdPatient', null,  ['class' => 'form-control']) !!}
    </div>
</fieldset>
<footer>

    <a href="#HomeAdmin/complain" title="Back"><button type="button" class="btn btn-warning btn-pretty"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
    {!! Form::button(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-info btn-pretty','type'=>'submit']) !!}

</footer>
<script>
    @include('backend.includes.selectjs')
    @include('backend.includes.uploadjs')


</script>