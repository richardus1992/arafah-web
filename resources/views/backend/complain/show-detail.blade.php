
    <div class="right_col" role="main">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Komplain {{ $item->Id }}</div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $item->Id }}</td>
                                        <th> Nama </th>
                                        <td> {{ $item->PatientName }} </td>
                                    </tr>
                                    <tr>
                                        <th> Alamat </th>
                                        <td> {{ $item->AddPatient }} </td>
                                        <th> Telp </th>
                                        <td> {{ $item->PhonePatient }} </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                        @foreach($itemDetail as $val)
                        <div class="col-md-12">
                            <div class="thumbnail">
                                @if(!empty($val->FileImage))
                                <img src="{{asset($val->FileImage)}}" class="img-responsive">
                                @endif
                                <div class="caption">
                                    <h3>{{$val->ComplainType}}</h3>
                                    <p>{{$val->ContentComplain}}</p>

                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>

