<div id="form-province">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h4>Balas Komplain #{{ $item->Id }}</h4>
                            </div>
                        </div>

                        <div class="panel-body no-padding">


                        {!! Form::model($item, [
                            'method' => 'PATCH',
                            'url' => ['HomeAdmin/complain', $item->Id],
                            'class' => 'lobi-form ajaxPostImage',
                            'files' => true
                        ]) !!}

                        @include ('backend/.complain.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>

</div>

