<div class="lobimail fadeInDown animated" data-view-email-url="{{URL::to('HomeAdmin/complain/1/messageview')}}">
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-3 col-menu-search">
            <div class="col-search">
                <form>
                    <div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Search">
                        <span class="input-group-btn">
                            <button class="btn btn-default">OK</button>
                        </span>
                    </div><!-- /input-group -->
                </form>
            </div>
            <div class="col-menu">
                <a href="{{URL::to('HomeAdmin/complain/1/compose')}}" class="btn btn-info btn-block btn-lg btn-compose btn-pretty">Compose</a>
                <ul class="nav nav-stacked nav-menu">
                    <li>
                        <a href="{{URL::to('HomeAdmin/complain/1/inbox')}}" data-type="inbox">
                            <i class="fa fa-inbox"></i>
                            Inbox
                            <span class="badge bg-info pull-right badge-unread"></span>
                        </a>
                    </li>
                    <li><a href="{{URL::to('HomeAdmin/complain/1/sent')}}" data-type="sent"><i class="fa fa-envelope"></i> Sent</a></li>
                    <li><a href="#"><i class="fa fa-file" data-type="draft"></i> Draft</a></li>
                    <li><a href="#"><i class="fa fa-trash-o" data-type="trash"></i> Spam</a></li>
                    <li><a href="#"><i class="fa fa-archive" data-type="archive"></i> Archive</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12 col-sm-8 col-md-9 col-content">

        </div>
    </div>

</div>
<script !src="">
    @include('backend/.includes.indexjs')
    @include('backend.includes.selectjs')
    LobiAdmin.loadScript([
        'clouds/backend/template/js/lobi-plugins/lobimail.min.js',
        'clouds/backend/template/js/plugin/summernote/summernote.min.js',
        'clouds/backend/template/js/plugin/select2/select2.min.js'
    ], function(){
        $('.lobimail').lobiMail();
    });
    hashReplace('HomeAdmin/complain#blank');
</script>
</div>

