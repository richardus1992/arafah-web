<div id="data-Komplain-type">
    <div class="panel panel-light" id="lobipanel-MasterProvince">
        <div class="panel-heading">
            <div class="panel-title">
                <h4> Komplain Pasien</h4>
            </div>
        </div>

        <div class="panel-body">
            <div class="table-responsive">
                <table id="MasterProvince" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="with-input"><input type="text" class="form-control" placeholder="ID"/></th>
                        <th class="with-input"><input type="text" class="form-control" placeholder="Jenis Komplain"/></th>
                        <th class="with-input"><input type="text" class="form-control" placeholder="Tgl Komplain"/></th>
                        <th class="with-input"><input type="text" class="form-control" placeholder="Pasien"/></th>
                        <th class="with-input"><input type="text" class="form-control" placeholder="Di Baca Pegawai"/></th>
                        <th class="with-input"><input type="text" class="form-control" placeholder="Pegawai Di Komplain"/></th>
                        <th class="with-input"><input type="text" class="form-control" placeholder="Tgl Di Baca"/></th>
                        <th></th>
                    </tr>
                    <tr>
                        <th>#</th>
                        <th>Jenis Komplain</th>
                        <th>Tgl Komplain</th>
                        <th>Pasien</th>
                        <th>Di Baca Pegawai</th>
                        <th>Pegawai Balas Komplain</th>
                        <th>Tgl Di Baca</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
</div>
<script !src="">
    @include('backend/.includes.indexjs')
    @include('backend.includes.selectjs')
    function initPage(){
        $('#lobipanel-MasterProvince').lobiPanel({
            reload: false,
            close: false,
            unpin: false,
            editTitle: false
        });
        var table=  $('#MasterProvince').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('data-complain') }}',
            order: [[ 0, "desc" ]],
            columns: [
                {data: 'Id', name: 'kpn.ID_KOMPLAIN_PASIEN'},
                {data: 'ComplainType', name: 'jpn.ComplainType'},
                {data: 'DateComplain', name: 'kpn.TANGGAL_KOMPLAIN'},
                {data: 'PatientName', name: 'psn.NAMA_PASIEN'},
                {data: 'EmpRead', name: 'pgwr.NAMA_PGW'},
                {data: 'EmpNameGetComplain', name: 'pgw.NAMA_PGW'},
                {data: 'ReadComplain', name: 'kpn.TANGGAL_DILIHAT'},
                {data: 'action', name: 'action'}
            ]
        });

        $("#MasterProvince thead th input[type=text]").on( 'keyup change', function (e) {
            if( e.keyCode == 13) {
                table
                    .column( $(this).closest('th').index()+':visible' )
                    .search( this.value )
                    .draw();
            }
            // Ensure we clear the search if they backspace far enough
            if(this.value == "") {
                table
                    .column( $(this).closest('th').index()+':visible' )
                    .search( this.value )
                    .draw();
            }

        });
        // Apply the search
        $("#MasterProvince thead th select").on('change', function () {

            var val = $.fn.dataTable.util.escapeRegex(
                $(this).val()
            );

            table
                .column( $(this).closest('th').index()+':visible' )
                .search( this.value )
                .draw();

        } );

        $("#MasterProvince_filter").hide();

    }
    hashReplace('HomeAdmin/complain#blank');
</script>
</div>

