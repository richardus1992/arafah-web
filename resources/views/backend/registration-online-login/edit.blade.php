<div id="form-registration-online">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h4>Edit Pendaftaran Online #{{ $item->Id }}</h4>
                            </div>
                        </div>

                        <div class="panel-body no-padding">


                        {!! Form::model($item, [
                            'method' => 'PATCH',
                            'url' => ['HomeAdmin/register-online-login', $item->Id],
                            'class' => 'lobi-form ajaxPost',
                            'files' => true
                        ]) !!}

                        @include ('backend/.registration-online-login.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>

</div>

