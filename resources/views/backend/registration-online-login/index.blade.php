<div id="data-polyclinic">
    <div class="panel panel-light" id="lobipanel-MasterProvince">
        <div class="panel-heading">
            <div class="panel-title">
                <h4>Master Pendaftaran Online Login</h4>
            </div>
        </div>
        <div class="panel-body">
            <a href="#HomeAdmin/register-online-login/create" class="btn btn-success btn-sm" title="Add New Kokab">
                <i class="fa fa-plus" aria-hidden="true"></i> Add New
            </a>
            <br/>
            <br/>
                <div class="table-responsive">
                    <table id="MasterProvince" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="with-input"><input type="text" class="form-control" placeholder="ID"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="ID User"/></th>
                            <th></th>
                            <th></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Nama User"/></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr>
                            <th>#</th>
                            <th>ID User</th>
                            <th>User Name</th>
                            <th>Password</th>
                            <th>Nama Pasien</th>
                            <th>Nik</th>
                            <th>Tgl Lahir</th>
                            <th>Kelamin</th>
                            <th>Status Nikah</th>
                            <th>Alamat</th>
                            <th>Provinsi</th>
                            <th>Kota</th>
                            <th>Poliklinik</th>
                            <th>Konfirmasi Kedatangan</th>
                            <th>Catatan</th>
                            <th>Tgl Daftar</th>
                        </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
</div>
<script !src="">
   @include('backend/.includes.indexjs')
   @include('backend.includes.selectjs')
   function initPage(){
       $('#lobipanel-MasterProvince').lobiPanel({
           reload: false,
           close: false,
           unpin: false,
           editTitle: false
       });
          var table=  $('#MasterProvince').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('data-register-online-login') }}',
            order: [[ 0, "desc" ]],
            columns: [
                  {data: 'Id', name: 'pdo.ID_ONLINE'},
                  {data: 'IdUser', name: 'pdo.ID_USER'},
                  {data: 'UserName', name: 'csr.USERNAME'},
                  {data: 'Password', name: 'csr.PASSWORD'},
                  {data: 'Name', name: 'pdo.NAMA_PASIEN'},
                  {data: 'Nik', name: 'pdo.NIK'},
                  {data: 'DateBirth', name: 'pdo.TGLLAHIR_PASIEN'},
                  {data: 'Sex', name: 'pdo.JK_PASIEN'},
                  {data: 'Married', name: 'pdo.STATUS_PERKAWINAN'},
                  {data: 'Addres', name: 'pdo.ALAMAT_PASIEN'},
                  {data: 'Province', name: 'pvc.NAMA_KEPENDUDUKAN'},
                  {data: 'City', name: 'cty.NAMA_KEPENDUDUKAN'},
                  {data: 'RoomService', name: 'ply.NAMA_LAYANAN_RS'},
                {data: 'Confirm', name: 'pdo.KONFIRMASI_KEDATANGAN'},
                {data: 'Remarks', name: 'pdo.KETERANGAN'},
                {data: 'CreateAt', name: 'pdo.CreateAt'}

            ]
          });

           $("#MasterProvince thead th input[type=text]").on( 'keyup change', function (e) {
               if( e.keyCode == 13) {
                   table
                           .column( $(this).closest('th').index()+':visible' )
                           .search( this.value )
                           .draw();
               }
               // Ensure we clear the search if they backspace far enough
               if(this.value == "") {
                   table
                           .column( $(this).closest('th').index()+':visible' )
                           .search( this.value )
                           .draw();
               }

           });
       // Apply the search
           $("#MasterProvince thead th select").on('change', function () {

               var val = $.fn.dataTable.util.escapeRegex(
                       $(this).val()
               );

               table
                       .column( $(this).closest('th').index()+':visible' )
                       .search( this.value )
                       .draw();

           } );

            $("#MasterProvince_filter").hide();

        }
    hashReplace('HomeAdmin/register-online-login#blank');
</script>
</div>

