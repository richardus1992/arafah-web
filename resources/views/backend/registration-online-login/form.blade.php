<fieldset id="register-online-login">
<div class="row">
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('IdRegisOnline') ? 'has-error' : ''}}">
            {!! Form::label('IdRegisOnline', 'Pendaftar Online', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;">* Wajib Di Pilih</small>
            <label class="input">
                {!! Form::select('IdRegisOnline', [''=>'- Pilih Pasien -']+$regionline->toArray(), null, array('class' => 'form-control select2-demo','id'=>'IdRegisOnline')) !!}

                {!! $errors->first('IdRegisOnline', '<p class="help-block">:message</p>') !!}
            </label>

        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('IdPatient') ? 'has-error' : ''}}">
            {!! Form::label('IdPatient', 'Pasien', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;">* Wajib Di Pilih</small>
            <label class="input">
                @if(isset($submitButtonText))

                    {!! Form::text('IdPatient', $item->PatientName,  ['class' => 'form-control','readonly']) !!}
                @else

                    {!! Form::select('IdPatient', [''=>'- Pilih Pasien -'], null, array('class' => 'form-control select2-demo','id'=>'IdPatient')) !!}
                @endif
                {!! $errors->first('IdPatient', '<p class="help-block">:message</p>') !!}
            </label>

        </div>
    </div>
</div>
</fieldset>
<footer>

    <a href="#HomeAdmin/registration-online" title="Back"><button type="button" class="btn btn-warning btn-pretty"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
    {!! Form::button(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-info btn-pretty','type'=>'submit']) !!}

</footer>
<script>
    @include('backend.includes.selectjs')
    function loading(stat,elem){
        if(stat){
            var anim = '<span class="input-icon input-icon-append" id="loading" style="color: #337bb2;margin-right: 2px;">' +
                '<i class="fa fa-cog fa-spin fa-2x"></i></span>';
            elem.closest('label.input').append(anim);
        }else{
            $('#loading').remove();
        }
    }

    function loadPatient(elem) {
        console.log(elem.val());
        var fieldunits    =elem.val().split('|');
        var value = fieldunits[1].substring(0,4);
        loading(true,elem);
        $.ajax({
            url:"{{ route('register-online-login.fetchPatient') }}",
            method:"GET",
            data:{search:value},
            success:function(result)
            {
                $('#IdPatient').html(result);
            }

        }).always(function () {
            loading(false,elem);
        });
    }



</script>