<fieldset>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('IdKab') ? 'has-error' : ''}}">
            {!! Form::label('Kab / Kota', 'Kab / Kota', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;">* Wajib Di Pilih</small>

            <label class="input">
                {!! Form::select('IdKab', $kabupaten, null, array('class' => 'form-control chosen-demo')) !!}
                {!! $errors->first('IdKab', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('IdOgnUnits') ? 'has-error' : ''}}">
            {!! Form::label('Departement', 'Departement', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;">* Wajib Di Pilih</small>
            <label class="input">
                {!! Form::select('IdOgnUnits', $dept, null, array('class' => 'form-control chosen-demo')) !!}
                {!! $errors->first('IdOgnUnits', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('Name') ? 'has-error' : ''}}">
            {!! Form::label('Name', 'Name', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;">* Wajib Di Isi</small>

            <label class="input">
                {!! Form::text('Name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('Name', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('Married') ? 'has-error' : ''}}">
            {!! Form::label('Married', 'Married', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;">* Wajib Di Pilih</small>

            <label class="input">
                {{ Form::select('Married', [
                   'Menikah' => 'Menikah',
                   'Lajang' => 'Lajang',
                   'Janda' => 'Janda',
                   'Duda' => 'Duda']
                ) }}
                {!! $errors->first('Married', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('NickName') ? 'has-error' : ''}}">
            {!! Form::label('NickName', 'Nickname', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;">* Wajib Di Isi</small>

            <label class="input">
                {!! Form::text('NickName', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('NickName', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('DateOfBirth') ? 'has-error' : ''}}">
            {!! Form::label('DateOfBirth', 'Tanggal Lahir', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;">* Wajib Di Isi</small>

            <label class="input">
                {!! Form::text('DateOfBirth',null, array('class' => 'date-picker form-control','placeholder'=>'Tanggal Lahir','readonly')) !!}
                {!! $errors->first('DateOfBirth', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('CellPhone') ? 'has-error' : ''}}">
            {!! Form::label('CellPhone', 'Cellphone', ['class' => 'control-label']) !!}

            <label class="input">
                {!! Form::text('CellPhone', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('CellPhone', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('HomePhone') ? 'has-error' : ''}}">
            {!! Form::label('HomePhone', 'Homephone', ['class' => 'control-label']) !!}

            <label class="input">
                {!! Form::text('HomePhone', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('HomePhone', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('Address') ? 'has-error' : ''}}">
            {!! Form::label('Address', 'Address', ['class' => 'control-label']) !!}
            <label class="input">
                {!! Form::text('Address', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('Address', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('NIP') ? 'has-error' : ''}}">
            {!! Form::label('NIP', 'NIP', ['class' => 'control-label']) !!}
            <label class="input">
                {!! Form::text('NIP', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('NIP', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('Religion') ? 'has-error' : ''}}">
            {!! Form::label('Religion', 'Religion', ['class' => 'control-label']) !!}

            <label class="input">
                {!! Form::text('Religion', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('Religion', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('Status') ? 'has-error' : ''}}">
            {!! Form::label('Status', 'Status', ['class' => 'control-label']) !!}
            <label class="input">
                {{ Form::select('Status', [
                   'Staff' => 'Staff',
                   'Kontrak' => 'Kontrak']
                ) }}
                {!! $errors->first('Status', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>

</div>

</fieldset>
<footer>

    <a href="#HomeAdmin/employee" title="Back"><button type="button" class="btn btn-warning btn-pretty"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
    {!! Form::button(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-info btn-pretty','type'=>'submit']) !!}

</footer>
<script !src="">

    @include('backend/.includes.selectjs')
    @include('backend.includes.daterangepicker')
    function initDateRangePicker(){
        $('.date-picker').datepicker({
            format: 'd/m/yyyy'
        });
        $('.datepicker-demo-autoclose').datepicker({
            autoclose: true,
            format: 'd/m/yyyy'
        });

    }

</script>