<div id="data-employee">
    <div class="panel panel-light" id="lobpanel-employee">
        <div class="panel-heading">
            <div class="panel-title">
                <h4>Master Pegawai</h4>
            </div>
        </div>
        <div class="panel-body">
            <br/>
            <div class="table-responsive">
                <table id="Employee" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="with-input"><input type="text" class="form-control" placeholder="ID"/></th>
                    <th class="with-input"><input type="text" class="form-control" placeholder="NIP"/></th>
                    <th class="with-input"><input type="text" class="form-control" placeholder="Nama"/></th>
                    <th class="with-input"><input type="text" class="form-control" placeholder="Alamat"/></th>

                </tr>

                    <tr>
                        <th>#</th>
                        <th>NIP</th>
                        <th>Nama</th>
                        <th>ALAMAT</th>
                    </tr>
                </thead>
                </table>
            </div>
        </div>
    </div>

    <script !src="">
        @include('backend/.includes.indexjs')
        function initPage(){
            $('#lobpanel-employee').lobiPanel({
                reload: false,
                close: false,
                unpin: false,
                editTitle: false
            });
            var table   =$('#Employee').DataTable({
                processing: true,
                serverSide: true,
                order: [[ 0, "desc" ]],
                ajax: '{{ route('data-employee') }}',
                columns: [
                    {data: 'Id', name: 'emp.ID_PGW'},
                    {data: 'NIP', name: 'emp.NIP'},
                    {data: 'Name', name: 'emp.NAMA_PGW'},
                    {data: 'Address', name: 'emp.ALAMAT_PGW'}

                ]
            });

            $("#Employee_filter").hide();


            $("#Employee thead th input[type=text]").on( 'keyup change', function (e) {
                if( e.keyCode == 13) {
                    table
                            .column( $(this).closest('th').index()+':visible' )
                            .search( this.value )
                            .draw();
                }
                // Ensure we clear the search if they backspace far enough
                if(this.value == "") {
                    table
                            .column( $(this).closest('th').index()+':visible' )
                            .search( this.value )
                            .draw();
                }

            });
            // Apply the search
            $("#Employee thead th select").on('change', function () {
                table
                        .column( $(this).closest('th').index()+':visible' )
                        .search( this.value )
                        .draw();

            });

        }
        hashReplace('HomeAdmin/employee#blank');
    </script>
</div>

