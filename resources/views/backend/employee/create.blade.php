<div id="form-layouts">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        <h4>Create New Karyawan</h4>
                    </div>
                </div>
                <div class="panel-body no-padding">
                    {!! Form::open(['url' => 'HomeAdmin/employee', 'class' => 'lobi-form ajaxPost', 'files' => true]) !!}

                    @include ('backend/.employee.form')

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

