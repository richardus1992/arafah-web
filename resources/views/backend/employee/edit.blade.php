<div id="form-employee">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>Edit Karyawan</h4>
                        </div>
                    </div>

                    <div class="panel-body no-padding">


                    {!! Form::model($employee, [
                        'method' => 'PATCH',
                        'url' => ['HomeAdmin/employee', $employee->Id],
                        'class' => 'lobi-form ajaxPost',
                        'files' => true
                    ]) !!}

                    @include ('backend/.employee.form', ['submitButtonText' => 'Update'])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>

