<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">Detail Karyawan</div>
                <div class="panel-body">

                    <a href="#HomeAdmin/employee" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    <a href="#HomeAdmin/employee/{{$employee->Id}}/edit" title="Edit Employee"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                    <a href="#HomeAdmin/employee/{{$employee->Id}}/delete" title="Delete Kokab"
                                            onclick="javascript:return confirm('Yakin ingin menghapus data?')"><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button></a>


                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <th>ID</th><td>{{ $employee->Id }}</td>
                                </tr>
                                <tr><th> Kab / Kota </th><td> {{ $employee->kabupaten }} </td></tr>
                                <tr><th> Departement </th><td> {{ $employee->departemen }} </td></tr>
                                <tr><th> Name </th><td> {{ $employee->Name }} </td></tr>
                                <tr><th> Married </th><td> {{ $employee->Married }} </td></tr>
                                <tr><th> NickName </th><td> {{ $employee->NickName }} </td></tr>
                                <tr><th> DateOfBirth </th><td> {{ $employee->DateOfBirth }} </td></tr>
                                <tr><th> CellPhone </th><td> {{ $employee->CellPhone }} </td></tr>
                                <tr><th> HomePhone </th><td> {{ $employee->HomePhone }} </td></tr>
                                <tr><th> Address </th><td> {{ $employee->Address }} </td></tr>
                                <tr><th> Religion </th><td> {{ $employee->Religion }} </td></tr>
                                <tr><th> Status </th><td> {{ $employee->Status }} </td></tr>
                                <tr><th> User Entry </th><td> {{ $employee->Username }} </td></tr>
                                <tr><th> CreateAt </th><td> {{ $employee->CreateAt }} </td></tr>
                                <tr><th> UpdateAt </th><td> {{ $employee->UpdateAt }} </td></tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

