<div id="data-province">
    <div class="panel panel-light" id="lobipanel-MasterProvince">
        <div class="panel-heading">
            <div class="panel-title">
                <h4>Master Provinsi</h4>
            </div>
        </div>
        <div class="panel-body">
            <a href="#HomeAdmin/province/create" class="btn btn-success btn-sm" title="Add New Kokab">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
            </a>
            <br/>
            <br/>
                <div class="table-responsive">
                    <table id="MasterProvince" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="with-input"><input type="text" class="form-control" placeholder="ID"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Provinsi"/></th>
                            <th></th>
                        </tr>
                        <tr>
                            <th>#</th>
                            <th>Provinsi</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
</div>
<script !src="">
   @include('backend/.includes.indexjs')
   @include('backend.includes.selectjs')
   function initPage(){
       $('#lobipanel-MasterProvince').lobiPanel({
           reload: false,
           close: false,
           unpin: false,
           editTitle: false
       });
          var table=  $('#MasterProvince').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('data-province') }}',
            order: [[ 0, "desc" ]],
            columns: [
                  {data: 'Id', name: 'pvc.ID_KEPENDUDUKAN'},
                  {data: 'Name', name: 'pvc.NAMA_KEPENDUDUKA'},
                  {data: 'action', name: 'action'}
            ]
          });

           $("#MasterProvince thead th input[type=text]").on( 'keyup change', function (e) {
               if( e.keyCode == 13) {
                   table
                           .column( $(this).closest('th').index()+':visible' )
                           .search( this.value )
                           .draw();
               }
               // Ensure we clear the search if they backspace far enough
               if(this.value == "") {
                   table
                           .column( $(this).closest('th').index()+':visible' )
                           .search( this.value )
                           .draw();
               }

           });
       // Apply the search
           $("#MasterProvince thead th select").on('change', function () {

               var val = $.fn.dataTable.util.escapeRegex(
                       $(this).val()
               );

               table
                       .column( $(this).closest('th').index()+':visible' )
                       .search( this.value )
                       .draw();

           } );

            $("#MasterProvince_filter").hide();

        }
    hashReplace('HomeAdmin/province#blank');
</script>
</div>

