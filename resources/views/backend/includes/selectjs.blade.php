LobiAdmin.loadScript([
    'clouds/backend/template/js/plugin/moment/moment.min.js',
    'clouds/backend/template/js/plugin/fileinput/fileinput.min.js',
    'clouds/backend/template/js/plugin/chosen/chosen.jquery.min.js',
    'clouds/backend/template/js/plugin/select2/select2.min.js',
    'clouds/backend/template/js/plugin/sweetalert2/sweetalert2.all.min.js',

    ], selectpage);

function selectpage(){
    $('.select2-demo').select2();
    $(".chosen-demo").chosen({
        disable_search_threshold: 10,
        width:"100%"
    });
}
