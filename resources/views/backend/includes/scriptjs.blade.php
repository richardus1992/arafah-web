<script type="text/javascript" src="{{ asset('clouds/backend/template/js/lib/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('clouds/backend/template/js/lib/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('clouds/backend/template/js/bootstrap/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('clouds/backend/template/js/lobi-plugins/lobibox.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('clouds/backend/template/js/lobi-plugins/lobipanel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('clouds/backend/template/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('clouds/backend/template/js/autoNumeric.js') }}"></script>
<script type="text/javascript" src="{{ asset('clouds/backend/template/js/jquery.lazzynumeric.js') }}"></script>
<script type="text/javascript" src="{{ asset('clouds/backend/template/js/config.js') }}"></script>
<script type="text/javascript" src="{{ asset('clouds/backend/template/js/LobiAdmin.js') }}"></script>
<script type="text/javascript" src="{{ asset('clouds/backend/template/js/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('clouds/backend/template/js/timepicki.js') }}"></script>


<script !src="">
    var allowMenu ={!! json_encode(session('allow_menu')) !!};
    {{--var allowMenu = JSON.parse('{{ json_encode(session('allow_menu')) }}');--}}
    function allowAccessMenu() {
        for(var i=0;i<allowMenu.length;i++){
            $('#menu-'+allowMenu[i]).show();
        }
    }
    $(window).load(function () {
        allowAccessMenu();
    });

    $(function () {
        $(document).on('click','.access input[type=checkbox]',function () {
            var checked = $(this).is(':checked');
            giveAccess(checked,$(this));
        });
        function giveAccess(checked,elem) {
            //var id = elem.attr('id');
            var Ul = elem.closest('ul');
            var isLastUl = Ul.hasClass('access');
            var isAction = Ul.hasClass('action');
            elem.prop('checked', checked);
            if(!isLastUl && checked){
                var prevCB = Ul.closest('li').find('input[type=checkbox]').eq(0);
                //alert(prevCB.attr('id'));
                giveAccess(checked,prevCB);
            }
            if(!checked){
                var child = elem.closest('li').find('ul');
                if(child.length > 0){
                    child.find('input[type=checkbox]').prop('checked', checked);
                }
            }
        }



        $(document).on('keyup', '#FormCombinateNumber input.generateNumber',function (event) {
            event.preventDefault();
            generateNumber()
        });

        function addNode() {
            alert('add');
        }

    });

    $(document).on('change','#register-online-login select#IdRegisOnline',function () {
        loadPatient($(this));

    });
    $(document).on('click', '#data-order-queue button#btnScrollRight',function (event) {
        scrollRightTable();
    });
    $(document).on('click', '#data-order-queue button#btnScrollLeft',function (event) {
        scrollLeftTable();
    });


    // $(document).on('keyup','#register-online-login select#IdPatient',function () {
    //     loadPatient($(this));
    //
    // });
</script>
