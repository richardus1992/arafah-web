<link href="{{ asset('clouds/backend/template/css/bootstrap.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset('clouds/backend/template/css/font-awesome.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset('clouds/backend/template/css/weather-icons.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset('clouds/backend/template/css/demo.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset('clouds/backend/template/css/lobiadmin-with-plugins.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset('clouds/backend/template/js/plugin/datetimepicker/jquery-ui-timepicker-addon.css') }}" media="all" rel="stylesheet" type="text/css" />
