
LobiAdmin.loadScript([
    'clouds/backend/template/js/plugin/daterangepicker/daterangepicker.min.js',
    'clouds/backend/template/js/plugin/datetimepicker/jquery-ui-timepicker-addon.js',
    'clouds/backend/template/js/plugin/moment/moment.min.js',
    'clouds/backend/template/js/plugin/fileinput/fileinput.min.js',
    'clouds/backend/template/js/plugin/bootstrap-datepicker/bootstrap-datepicker.min.js',
    'clouds/backend/template/js/plugin/jasny-bootstrap/jasny-bootstrap.min.js',
    'clouds/backend/template/js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js',
    ], initDateRangePicker);