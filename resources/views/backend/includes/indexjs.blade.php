
LobiAdmin.loadScript([
    'clouds/backend/template/js/plugin/datatables/jquery.dataTables.min.js',
    'clouds/backend/template/js/plugin/highlight/highlight.pack.js'
    ], function(){
LobiAdmin.loadScript([
    'clouds/backend/template/js/plugin/datatables/dataTables.bootstrap.min.js',
    'clouds/backend/template/js/plugin/datatables/dataTables.responsive.min.js',
    'clouds/backend/template/js/plugin/bootstrap-datepicker/bootstrap-datepicker.js'
    ], initPage);
});
@if (Session::has('flash_message'))
    Lobibox.notify('success', {
    sound: false,
    title: 'Berhasil',
    delay: 3000,
    msg: '{!! Session::get('flash_message') !!}'
    });
@endif

@if (Session::has('error_message'))
    Lobibox.notify('error', {
    sound: false,
    title: 'Gagal',
    delay: 3000,
    msg: '{!! Session::get('error_message') !!}'
    });
@endif