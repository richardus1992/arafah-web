LobiAdmin.loadScript([
'clouds/backend/template/js/plugin/jasny-bootstrap/jasny-bootstrap.min.js',
'clouds/backend/template/js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js'
], inputMask);
function inputMask(){
    var masks = $('.inputmask');
    masks.each(function (ind, el) {
        var $el = $(el);
        var obj = {};
        if ($el.data('mask')) {
        obj.mask = $el.data('mask');
    }
    if ($el.data('mask-placeholder')) {
        obj.placeholder = $el.data('mask-placeholder');
    }
    $el.inputmask(obj);
});
}