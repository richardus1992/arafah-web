<nav>
    <ul>
        <li>
            <a href="#HomeAdmin/index">
                <i class="fa fa-home menu-item-icon"></i>
                <span class="inner-text">Dashboard</span>
            </a>
        </li>
        {!! $navMenu !!}
    </ul>
</nav>