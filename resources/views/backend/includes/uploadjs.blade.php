LobiAdmin.loadScript([
    'clouds/backend/template/js/plugin/minicolors/jquery.minicolors.min.js',
    'clouds/backend/template/js/plugin/clockpicker/bootstrap-clockpicker.min.js',
    'clouds/backend/template/js/plugin/timepicker/bootstrap-timepicker.min.js',
    'clouds/backend/template/js/plugin/maxlength/bootstrap-maxlength.min.js',
    'clouds/backend/template/js/plugin/bootstrap-datepicker/bootstrap-datepicker.min.js',
    'clouds/backend/template/js/plugin/jasny-bootstrap/jasny-bootstrap.min.js',
    'clouds/backend/template/js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js',
    'clouds/backend/template/js/plugin/fileinput/fileinput.min.js',
    'clouds/backend/template/js/plugin/chosen/chosen.jquery.min.js',
    'clouds/backend/template/js/plugin/duallistbox/jquery.bootstrap-duallistbox.min.js',
    'clouds/backend/template/js/plugin/select2/select2.min.js',
    {{--'clouds/backend/template/js/plugin/krajee/fileinput.min.js',--}}
    {{--'clouds/backend/template/js/plugin/krajee/theme.js',--}}
    {{--'clouds/backend/template/js/plugin/krajee/fas/theme.min.js',--}}

    ], uploadpage);

function uploadpage(){
    $(".input-file-upload").fileinput({showUpload: false});
}
