<div id="form-type-user">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h4>Edit Type User</h4>
                            </div>
                        </div>

                        <div class="panel-body no-padding">


                        {!! Form::model($typeuser, [
                            'method' => 'PATCH',
                            'url' => ['HomeAdmin/type-user', $typeuser->Id],
                            'class' => 'lobi-form ajaxPost',
                            'files' => true
                        ]) !!}

                        @include ('backend/.type-user.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>

</div>

