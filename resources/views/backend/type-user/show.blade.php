
    <div class="right_col" role="main">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Detail Type User</div>
                    <div class="panel-body">

                        <a href="#HomeAdmin/type-user" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="#HomeAdmin/type-user/{{$typeuser->Id}}/edit" title="Edit TypeUser"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <a href="#HomeAdmin/type-user/{{$typeuser->Id}}/delete" title="Delete Kokab"
                                                onclick="javascript:return confirm('Yakin ingin menghapus data?')"><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button></a>


                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $typeuser->Id }}</td>
                                    </tr>
                                    <tr><th> Name </th><td> {{ $typeuser->Name }} </td></tr>
                                    <tr><th> Description </th><td> {{ $typeuser->Description }} </td></tr>
                                    <tr><th> User Entry </th><td> {{ $typeuser->UserName }} </td></tr>
                                    <tr><th> Created At </th><td> {{ $typeuser->CreateAt }} </td></tr>
                                    <tr><th> Updated At </th><td> {{ $typeuser->UpdateAt }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

