<fieldset>
<div class="row">
        <div class="col-sm-12">
        <div class="form-group {{ $errors->has('Name') ? 'has-error' : ''}}">
            {!! Form::label('Name', 'Name', ['class' => 'control-label']) !!}
            <label class="input">
                {!! Form::text('Name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('Name', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>
</div><div class="row">
        <div class="col-sm-12">
        <div class="form-group {{ $errors->has('Description') ? 'has-error' : ''}}">
            {!! Form::label('Description', 'Description', ['class' => 'control-label']) !!}
            <label class="input">
                {!! Form::textarea('Description', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('Description', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        {!! Form::label('Access-Menu', 'Access Menu', ['class' => 'control-label','style'=>'font-size:17px;']) !!}

        {!! $treeForm !!}
    </div>
</div>
</fieldset>
<footer>

    <a href="#HomeAdmin/type-user" title="Back"><button type="button" class="btn btn-warning btn-pretty"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
    {!! Form::button(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-info btn-pretty','type'=>'submit']) !!}

</footer>

<script !src="">

    function checkedBox(ua) {
        for(var i=0;i<ua.length;i++){
            var sel = 'input[type=checkbox][id='+ua[i].id+']';
            if(typeof ua[i].action !== "undefined")
            {
                sel += '[act="'+ua[i].action+'"]';
            }
            $(sel).prop('checked',true);
        }
    }

            @if(session()->has('userAccess'))
    var userAccess = {!! session('userAccess') !!};
    checkedBox(userAccess);
            @elseif(isset($userAccess))
    var userAccess = {!! $userAccess !!};
    checkedBox(userAccess);
    @endif
</script>