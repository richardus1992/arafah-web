<div id="data-type-user">
    <div class="panel panel-light" id="lobipanel-type-user">
        <div class="panel-heading">
            <div class="panel-title">
                <h4>Type user</h4>
            </div>
        </div>
        <div class="panel-body">
            <a href="#HomeAdmin/type-user/create" class="btn btn-success btn-sm" title="Add New Kokab">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
            </a>
            <br/>
            <br/>
            <div class="table-responsive">
                <table id="Typeuser" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="with-input"><input type="text" class="form-control" placeholder="ID"/></th>
                        <th class="with-input"><input type="text" class="form-control" placeholder="Nama"/></th>
                        <th class="with-input"><input type="text" class="form-control" placeholder="User Entry"/></th>
                        <th class="with-input"><input type="text" class="form-control" placeholder="Created At"/></th>
                        <th class="with-input"><input type="text" class="form-control" placeholder="Updated At"/></th>
                        <th></th>
                    </tr>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>CreateAt</th>
                        <th>UpdateAt</th>
                        <th>Actions</th>

                    </tr>
                </thead>
                </table>
            </div>
        </div>
    </div>
    <script !src="">
       @include('backend/.includes.indexjs')
            function initPage(){
               $('#lobipanel-type-user').lobiPanel({
                   reload: false,
                   close: false,
                   unpin: false,
                   editTitle: false
               });
                var table   =$('#Typeuser').DataTable({
                    processing: true,
                    serverSide: true,
                    order: [[ 0, "desc" ]],
                    ajax: '{{ route('data-type-user') }}',
                    columns: [
                        {data: 'Id', name: 'Id'},
                        {data: 'Name', name: 'Name'},
                        {data: 'Description', name: 'Description'},
                        {data: 'CreateAt', name: 'CreateAt'},
                        {data: 'UpdateAt', name: 'UpdateAt'},
                        {data: 'action', name: 'action'}
                    ]
                });

           $("#Typeuser_filter").hide();


           $("#Typeuser thead th input[type=text]").on( 'keyup change', function (e) {
               if( e.keyCode == 13) {
                   table
                           .column( $(this).closest('th').index()+':visible' )
                           .search( this.value )
                           .draw();
               }
               // Ensure we clear the search if they backspace far enough
               if(this.value == "") {
                   table
                           .column( $(this).closest('th').index()+':visible' )
                           .search( this.value )
                           .draw();
               }

           });
           // Apply the search
           $("#Typeuser thead th select").on('change', function () {
               table
                       .column( $(this).closest('th').index()+':visible' )
                       .search( this.value )
                       .draw();

                });

            }
       hashReplace('HomeAdmin/type-user#blank');
    </script>
</div>

