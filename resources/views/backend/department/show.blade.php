
    <div class="right_col" role="main">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Department {{ $ognunit->Id }}</div>
                    <div class="panel-body">

                        <a href="#HomeAdmin/department" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="#HomeAdmin/department/{{$ognunit->Id}}/edit" title="Edit Department"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <a href="#HomeAdmin/department/{{$ognunit->Id}}/delete" title="Delete Department"
                                                onclick="javascript:return confirm('Yakin ingin menghapus data?')"><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button></a>


                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $ognunit->Id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Organisation </th>
                                        <td> {{ $ognunit->Organisation }} </td>
                                    </tr>
                                    <tr>
                                        <th> JobDisk </th>
                                        <td> {{ $ognunit->JobDisk }} </td>
                                    </tr>
                                    <tr>
                                        <th> Keterangan </th>
                                        <td> {{ $ognunit->Remarks }} </td>
                                    </tr>
                                    <tr>
                                        <th> User Entry </th>
                                        <td> {{ $ognunit->Username }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

