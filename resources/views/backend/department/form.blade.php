<fieldset>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group {{ $errors->has('Organisation') ? 'has-error' : ''}}">
            {!! Form::label('Organisation', 'Department', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;">* Wajib Di Isi</small>
            <label class="input">
                {!! Form::text('Organisation', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('Organisation', '<p class="help-block">:message</p>') !!}
            </label>
            
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group {{ $errors->has('JobDisk') ? 'has-error' : ''}}">
            {!! Form::label('JobDisk', 'JobDisk', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;"></small>
            <label class="input">
                {!! Form::textarea('JobDisk', null, ['class' => 'form-control','rows'=>'3']) !!}
                {!! $errors->first('JobDisk', '<p class="help-block">:message</p>') !!}
            </label>

        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group {{ $errors->has('Remarks') ? 'has-error' : ''}}">
            {!! Form::label('Remarks', 'Remarks', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;"></small>
            <label class="input">
                {!! Form::textarea('Remarks', null, ['class' => 'form-control','rows'=>'3']) !!}
                {!! $errors->first('Remarks', '<p class="help-block">:message</p>') !!}
            </label>

        </div>
    </div>
</div>

</fieldset>
<footer>

    <a href="#HomeAdmin/department" title="Back"><button type="button" class="btn btn-warning btn-pretty"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
    {!! Form::button(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-info btn-pretty','type'=>'submit']) !!}

</footer>
<script>
    @include('backend.includes.selectjs')

</script>