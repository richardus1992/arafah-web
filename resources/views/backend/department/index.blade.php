<div id="data-department">
    <div class="panel panel-light" id="lobipanel-MasterDepartMent">
        <div class="panel-heading">
            <div class="panel-title">
                <h4>Master Department</h4>
            </div>
        </div>
        <div class="panel-body">
            <a href="#HomeAdmin/department/create" class="btn btn-success btn-sm" title="Add New Kokab">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
            </a>
            <br/>
            <br/>
                <div class="table-responsive">
                    <table id="MasterDepartMent" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="with-input"><input type="text" class="form-control" placeholder="ID"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Organisasi"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="JobDisk"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="UserEntry"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="CreateAt"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="UpdateAt"/></th>
                            <th></th>
                        </tr>
                        <tr>
                            <th>#</th>
                            <th>Organisasi</th>
                            <th>JobDisk</th>
                            <th>UserEntry</th>
                            <th>CreateAt</th>
                            <th>UpdateAt</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
</div>
<script !src="">
   @include('backend/.includes.indexjs')
   @include('backend.includes.selectjs')
   function initPage(){
       $('#lobipanel-MasterDepartMent').lobiPanel({
           reload: false,
           close: false,
           unpin: false,
           editTitle: false
       });
          var table=  $('#MasterDepartMent').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('data-department') }}',
            order: [[ 0, "desc" ]],
            columns: [
                  {data: 'Id', name: 'dpt.Id'},
                  {data: 'Organisation', name: 'dpt.Organisation'},
                  {data: 'JobDisk', name: 'dpt.JobDisk'},
                  {data: 'UserEntry', name: 'usr.UserName'},
                  {data: 'CreateAt', name: 'dpt.CreateAt'},
                  {data: 'UpdateAt', name: 'dpt.UpdateAt'},
                  {data: 'action', name: 'action'}
            ]
          });

           $("#MasterDepartMent thead th input[type=text]").on( 'keyup change', function (e) {
               if( e.keyCode == 13) {
                   table
                           .column( $(this).closest('th').index()+':visible' )
                           .search( this.value )
                           .draw();
               }
               // Ensure we clear the search if they backspace far enough
               if(this.value == "") {
                   table
                           .column( $(this).closest('th').index()+':visible' )
                           .search( this.value )
                           .draw();
               }

           });
       // Apply the search
           $("#MasterDepartMent thead th select").on('change', function () {

               var val = $.fn.dataTable.util.escapeRegex(
                       $(this).val()
               );

               table
                       .column( $(this).closest('th').index()+':visible' )
                       .search( this.value )
                       .draw();

           } );

            $("#MasterDepartMent_filter").hide();

        }
    hashReplace('HomeAdmin/department#blank');
</script>
</div>

