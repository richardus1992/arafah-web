<fieldset>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('RoomService') ? 'has-error' : ''}}">
            {!! Form::label('RoomService', 'Kamar', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;">* Wajib Di Pilih</small>
            <label class="input">
                {!! Form::select('RoomService', [''=>'- Pilih Kamar -']+$rooms->toArray(), null, array('class' => 'form-control select2-demo')) !!}
                {!! $errors->first('RoomService', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="form-group {{ $errors->has('Qty') ? 'has-error' : ''}}">
            {!! Form::label('Qty', 'Jumlah', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;">* Wajib Di Isi</small>
            <label class="input">
                {!! Form::text('Qty', null, ['class' => 'form-control numeral']) !!}
                {!! $errors->first('Qty', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group {{ $errors->has('Name') ? 'has-error' : ''}}">
            {!! Form::label('Name', 'Name', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;">* Wajib Di Isi</small>
            <label class="input">
                {!! Form::text('Name', null, ['class' => 'form-control']) !!}
                {!! $errors->first('Name', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="form-group {{ $errors->has('Description') ? 'has-error' : ''}}">
            {!! Form::label('Description', 'Keterangan', ['class' => 'control-label']) !!}
            <label class="input">
                {!! Form::textarea('Description', null,['class' => 'form-control','rows'=>3]) !!}
                {!! $errors->first('Description', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>
</div>
</fieldset>
<footer>
    <a href="#HomeAdmin/faciliti-rooms" title="Back"><button type="button" class="btn btn-warning btn-pretty"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
    {!! Form::button(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-info btn-pretty','type'=>'submit']) !!}
</footer>
<script>
    @include('backend.includes.selectjs')

</script>