
    <div class="right_col" role="main">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Fasilitas Kamar {{ $districts->Id }}</div>
                    <div class="panel-body">

                        <a href="#HomeAdmin/districts" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="#HomeAdmin/districts/{{$districts->Id}}/edit" title="Edit Kecamatan"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <a href="#HomeAdmin/districts/{{$districts->Id}}/delete" title="Delete Kecamatan"
                                                onclick="javascript:return confirm('Yakin ingin menghapus data?')"><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button></a>


                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $districts->Id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Name </th>
                                        <td> {{ $districts->Name }} </td>
                                    </tr>
                                    <tr>
                                        <th> City </th>
                                        <td> {{ $districts->City }} </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

