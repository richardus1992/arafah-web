<div id="data-city">
    <div class="panel panel-light" id="lobipanel-Mastercity">
        <div class="panel-heading">
            <div class="panel-title">
                <h4>Master Kota</h4>
            </div>
        </div>
        <div class="panel-body">
            <a href="#HomeAdmin/city/create" class="btn btn-success btn-sm" title="Add New Kokab">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
            </a>
            <br/>
            <br/>
                <div class="table-responsive">
                    <table id="Mastercity" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="with-input"><input type="text" class="form-control" placeholder="ID"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Kota/Kab"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Provinsi"/></th>

                            <th></th>
                        </tr>
                        <tr>
                            <th>#</th>
                            <th>Kota/Kab</th>
                            <th>Provinsi</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
</div>
<script !src="">
   @include('backend/.includes.indexjs')
   @include('backend.includes.selectjs')
   function initPage(){
       $('#lobipanel-Mastercity').lobiPanel({
           reload: false,
           close: false,
           unpin: false,
           editTitle: false
       });
          var table=  $('#Mastercity').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('data-city') }}',
            order: [[ 0, "desc" ]],
            columns: [
                  {data: 'Id', name: 'cty.ID_KEPENDUDUKAN'},
                  {data: 'City', name: 'cty.NAMA_KEPENDUDUKAN'},
                  {data: 'Province', name: 'pvc.NAMA_KEPENDUDUKAN'},
                  {data: 'action', name: 'action'}
            ]
          });

           $("#Mastercity thead th input[type=text]").on( 'keyup change', function (e) {
               if( e.keyCode == 13) {
                   table
                           .column( $(this).closest('th').index()+':visible' )
                           .search( this.value )
                           .draw();
               }
               // Ensure we clear the search if they backspace far enough
               if(this.value == "") {
                   table
                           .column( $(this).closest('th').index()+':visible' )
                           .search( this.value )
                           .draw();
               }

           });
       // Apply the search
           $("#Mastercity thead th select").on('change', function () {

               var val = $.fn.dataTable.util.escapeRegex(
                       $(this).val()
               );

               table
                       .column( $(this).closest('th').index()+':visible' )
                       .search( this.value )
                       .draw();

           } );

            $("#Mastercity_filter").hide();

        }
    hashReplace('HomeAdmin/city#blank');
</script>
</div>

