<div id="data-districts">
    <div class="panel panel-light" id="lobipanel-Masterkecamatan">
        <div class="panel-heading">
            <div class="panel-title">
                <h4>Master Kecamatan</h4>
            </div>
        </div>
        <div class="panel-body">
            <a href="#HomeAdmin/districts/create" class="btn btn-success btn-sm" title="Add New Kokab">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
            </a>
            <br/>
            <br/>
                <div class="table-responsive">
                    <table id="Masterkecamatan" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="with-input"><input type="text" class="form-control" placeholder="ID"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Kecamatan"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Kota/Kab"/></th>
                            <th></th>
                        </tr>
                        <tr>
                            <th>#</th>
                            <th>Kecamatan</th>
                            <th>Kota/Kab</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
</div>
<script !src="">
   @include('backend/.includes.indexjs')
   @include('backend.includes.selectjs')
   function initPage(){
       $('#lobipanel-Masterkecamatan').lobiPanel({
           reload: false,
           close: false,
           unpin: false,
           editTitle: false
       });
          var table=  $('#Masterkecamatan').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('data-districts') }}',
            order: [[ 0, "desc" ]],
            columns: [
                  {data: 'Id', name: 'dst.ID_KEPENDUDUKAN'},
                  {data: 'Name', name: 'dst.NAMA_KEPENDUDUKAN'},
                  {data: 'City', name: 'cty.NAMA_KEPENDUDUKAN'},
                  {data: 'action', name: 'action'}
            ]
          });

           $("#Masterkecamatan thead th input[type=text]").on( 'keyup change', function (e) {
               if( e.keyCode == 13) {
                   table
                           .column( $(this).closest('th').index()+':visible' )
                           .search( this.value )
                           .draw();
               }
               // Ensure we clear the search if they backspace far enough
               if(this.value == "") {
                   table
                           .column( $(this).closest('th').index()+':visible' )
                           .search( this.value )
                           .draw();
               }

           });
       // Apply the search
           $("#Masterkecamatan thead th select").on('change', function () {

               var val = $.fn.dataTable.util.escapeRegex(
                       $(this).val()
               );

               table
                       .column( $(this).closest('th').index()+':visible' )
                       .search( this.value )
                       .draw();

           } );

            $("#Masterkecamatan_filter").hide();

        }
    hashReplace('HomeAdmin/districts#blank');
</script>
</div>

