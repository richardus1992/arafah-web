<div id="form-history-table">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h4>Edit Kokab #{{ $historytable->Id }}</h4>
                            </div>
                        </div>

                        <div class="panel-body no-padding">


                        {!! Form::model($historytable, [
                            'method' => 'PATCH',
                            'url' => ['HomeAdmin/history-table', $historytable->Id],
                            'class' => 'lobi-form ajaxPost',
                            'files' => true
                        ]) !!}

                        @include ('backend/.history-table.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>

</div>

