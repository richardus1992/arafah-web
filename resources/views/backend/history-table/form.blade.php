<fieldset>
<div class="row">
        <div class="col-sm-12">
        <div class="form-group {{ $errors->has('IdModule') ? 'has-error' : ''}}">
            {!! Form::label('IdModule', 'Idmodule', ['class' => 'control-label']) !!}
            <label class="input">
                {!! Form::text('IdModule', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('IdModule', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>
</div><div class="row">
        <div class="col-sm-12">
        <div class="form-group {{ $errors->has('Description') ? 'has-error' : ''}}">
            {!! Form::label('Description', 'Description', ['class' => 'control-label']) !!}
            <label class="input">
                {!! Form::text('Description', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('Description', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>
</div><div class="row">
        <div class="col-sm-12">
        <div class="form-group {{ $errors->has('Action') ? 'has-error' : ''}}">
            {!! Form::label('Action', 'Action', ['class' => 'control-label']) !!}
            <label class="input">
                {!! Form::text('Action', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('Action', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>
</div><div class="row">
        <div class="col-sm-12">
        <div class="form-group {{ $errors->has('UserEntry') ? 'has-error' : ''}}">
            {!! Form::label('UserEntry', 'Userentry', ['class' => 'control-label']) !!}
            <label class="input">
                {!! Form::text('UserEntry', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('UserEntry', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>
</div><div class="row">
        <div class="col-sm-12">
        <div class="form-group {{ $errors->has('create_at') ? 'has-error' : ''}}">
            {!! Form::label('create_at', 'Create At', ['class' => 'control-label']) !!}
            <label class="input">
                {!! Form::text('create_at', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('create_at', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>
</div>
</fieldset>
<footer>

    <a href="#HomeAdmin/kokab" title="Back"><button type="button" class="btn btn-warning btn-pretty"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
    {!! Form::button(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-info btn-pretty','type'=>'submit']) !!}

</footer>