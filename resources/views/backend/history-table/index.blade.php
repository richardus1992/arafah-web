<div id="data-history-table">
    <div class="panel panel-light" id="lobipanel-history-table">
        <div class="panel-heading">
            <div class="panel-title">
                <h4>Historytable</h4>
            </div>
        </div>
        <div class="panel-body">

            <br/>
            <br/>
            <div class="table-responsive">
            <table id="Historytable" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th></th>
                        <th class="with-input">{{Form::select( '',[''=>'- ALL -']+$module->toArray(), null, array('class' => 'form-control select2-demo'))}}</th>
                        <th class="with-input"><input type="text" class="form-control" placeholder="Desc"/></th>
                        <th class="with-input">{{Form::select( '',[''=>'- ALL -','INSERT'=>'INSERT','UPDATE'=>'UPDATE','EDIT'=>'EDIT','DELETE'=>'DELETE','PUBLISH'=>'PUBLISH','UNPUBLISH'=>'UNPUBLISH'], null, array('class' => 'form-control  select2-demo'))}}</th>
                        <th class="with-input"><input type="text" class="form-control" placeholder="USER ENTRY"/></th>
                        <th class="with-input">
                            <div class="input-group datecreate date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" class="form-control" placeholder="Start Date"/>
                            </div>
                        </th>

                    </tr>
                    <tr>
                        <th>#</th>
                        <th>Module</th>
                        <th>Description</th>
                        <th>Action</th>
                        <th>UserEntry</th>
                        <th>create_at</th>
                    </tr>
                </thead>
            </table>

            </div>
        </div>
</div>
</div>
 <script !src="">
        @include('backend/.includes.indexjs')
        @include('backend.includes.selectjs')

        function initPage(){
            $('#lobipanel-history-table').lobiPanel({
                reload: false,
                close: false,
                unpin: false,
                editTitle: false
            });
           var table= $('#Historytable').DataTable({
                    processing: true,
                    serverSide: true,
                    order: [[ 0, "desc" ]],
                    ajax: '{{ route('data-history-table') }}',
                    columns: [
                       {data: 'Id', name: 'hsy.Id'},
                       {data: 'Name', name: 'mdl.Name'},
                       {data: 'Description', name: 'hsy.Description'},
                       {data: 'Action', name: 'hsy.Action'},
                       {data: 'UserEntry', name: 'usr.UserName'},
                       {data: 'CreateAt', name: 'hsy.CreateAt'}
                   ]
                });

            $("#Historytable thead th input[type=text]").on( 'keyup change', function () {

                table
                        .column( $(this).closest('th').index()+':visible' )
                        .search( this.value )
                        .draw();

            });
            // Apply the search
            $("#Historytable thead th select").on('change', function () {

                var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                );

                table
                        .column( $(this).closest('th').index()+':visible' )
                        .search( this.value )
                        .draw();

            } );
            $('.datecreate').datepicker({
                format: 'yyyy-mm-dd'
            });

            $("#Historytable_filter").hide();


        }
    hashReplace('HomeAdmin/history-table#blank');

</script>
