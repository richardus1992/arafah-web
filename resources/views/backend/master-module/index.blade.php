<div id="data-master-module">
    <div class="panel panel-light" id="lobipanel-Mastermodule">
        <div class="panel-heading">
            <div class="panel-title">
                <h4>Master Module</h4>
            </div>
        </div>
        <div class="panel-body">
            <a href="#HomeAdmin/master-module/create" class="btn btn-success btn-sm" title="Add New Kokab">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
            </a>
            <br/>
            <br/>
                <div class="table-responsive">
                    <table id="Mastermodule" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="with-input"><input type="text" class="form-control" placeholder="ID"/></th>
                            <th class="with-input">{{Form::select( '',[''=>'- ALL -']+$header->toArray(), null, array('class' => 'form-control  select2-demo'))}}</th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Name"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Description"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="CreateAt"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="UpdateAt"/></th>
                            <th></th>
                        </tr>
                        <tr>
                            <th>#</th>
                            <th>Parent</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>CreateAt</th>
                            <th>UpdateAt</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script !src="">
   @include('backend/.includes.indexjs')
   @include('backend.includes.selectjs')
   function initPage(){
       $('#lobipanel-Mastermodule').lobiPanel({
           reload: false,
           close: false,
           unpin: false,
           editTitle: false
       });
          var table=  $('#Mastermodule').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('data-master-module') }}',
            order: [[ 0, "desc" ]],
            columns: [
                  {data: 'Id', name: 'mdl.ID'},
                  {data: 'Parent', name: 'mdll.NAME'},
                  {data: 'Name', name: 'mdl.NAME'},
                  {data: 'Description', name: 'mdl.DESCRIPTION'},
                  {data: 'CreateAt', name: 'mdl.CREATEAT'},
                  {data: 'UpdateAt', name: 'mdl.UPDATEAT'},
                  {data: 'action', name: 'action'}
            ]
          });

           $("#Mastermodule thead th input[type=text]").on( 'keyup change', function (e) {
               if( e.keyCode == 13) {
                   table
                           .column( $(this).closest('th').index()+':visible' )
                           .search( this.value )
                           .draw();
               }
               // Ensure we clear the search if they backspace far enough
               if(this.value == "") {
                   table
                           .column( $(this).closest('th').index()+':visible' )
                           .search( this.value )
                           .draw();
               }

           });
       // Apply the search
           $("#Mastermodule thead th select").on('change', function () {

               var val = $.fn.dataTable.util.escapeRegex(
                       $(this).val()
               );

               table
                       .column( $(this).closest('th').index()+':visible' )
                       .search( this.value )
                       .draw();

           } );

            $("#Mastermodule_filter").hide();

        }
    hashReplace('HomeAdmin/master-module#blank');
</script>
</div>

