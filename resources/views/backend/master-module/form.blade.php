<fieldset>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('Parent') ? 'has-error' : ''}}">
            {!! Form::label('Parent', 'Parent', ['class' => 'control-label']) !!}

            <label class="input">
                {!! Form::select('Parent', ['0'=>'- PARENT -']+$parent->toArray(), null, array('class' => 'form-control select2-demo')) !!}
                {!! $errors->first('Parent', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('Name') ? 'has-error' : ''}}">
            {!! Form::label('Name', 'Name', ['class' => 'control-label']) !!}
            <label class="input">
                {!! Form::text('Name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('Name', '<p class="help-block">:message</p>') !!}
            </label>
            <small class="text-danger">* Wajib Di Isi</small>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group {{ $errors->has('Description') ? 'has-error' : ''}}">
            {!! Form::label('Description', 'Description', ['class' => 'control-label']) !!}
            <label class="input">
                {!! Form::text('Description', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('Description', '<p class="help-block">:message</p>') !!}
            </label>
            <small class="text-danger">*Wajib Di Isi</small>
        </div>
    </div>
</div>
</fieldset>
<footer>

    <a href="#HomeAdmin/master-module" title="Back"><button type="button" class="btn btn-warning btn-pretty"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
    {!! Form::button(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-info btn-pretty','type'=>'submit']) !!}

</footer>
<script>
    @include('backend.includes.selectjs')

</script>