
    <div class="right_col" role="main">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">MasterModule {{ $mastermodule->Id }}</div>
                    <div class="panel-body">

                        <a href="#HomeAdmin/master-module" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="#HomeAdmin/master-module/{{$mastermodule->Id}}/edit" title="Edit MasterModule"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <a href="#HomeAdmin/master-module/{{$mastermodule->Id}}/delete" title="Delete Kokab"
                                                onclick="javascript:return confirm('Yakin ingin menghapus data?')"><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button></a>


                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $mastermodule->Id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Parent </th>
                                        <td> {{ $mastermodule->Parent }} </td>
                                    </tr>
                                    <tr>
                                        <th> Name </th>
                                        <td> {{ $mastermodule->Name }} </td>
                                    </tr>
                                    <tr>
                                        <th> Description </th>
                                        <td> {{ $mastermodule->Description }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

