<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>Lobi Admin</title>

        <link rel="shortcut icon" href="{{asset('clouds/backend/template/img/logo/lobiadmin-logo-16.ico')}}" />
        <link href="{{URL::asset('clouds/backend/template/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('clouds/backend/template/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('clouds/backend/template/css/login.css')}}" rel="stylesheet">
    </head>
    <body style="background-image: url('http://202.148.30.138:8012/clouds/backend/file/image/background_arafah.png');">
        <div class="login-wrapper fadeInDown animated">
            <div class="text-center">
                <div class="text-center">
                    <img src="{{asset('clouds/backend/file/image/arafah100.png')}}">

                </div>

            </div>

            <form action="{{URL::to('manage/login')}}" method="POST" class="lobi-form login-form visible">
                {!! csrf_field() !!}
                <div class="login-header">
                    <div class="text-center">
                        LOGIN ACCOUNTSS
                    </div>
                </div>
                <div class="login-body no-padding">
                    <fieldset>
                        <div class="form-group {{ $errors->has('errmsg') ? 'has-error' : ''}}">
                            <label>Username</label>
                            <label class="input">
                                <span class="input-icon input-icon-prepend fa fa-user"></span>
                                <input type="text" name="username" placeholder="UserName" required="" >
                                <span class="tooltip tooltip-top-left"><i class="fa fa-user text-cyan-dark"></i> Please enter the username</span>
                            </label>
                        </div>
                        <div class="form-group {{ $errors->has('errmsg') ? 'has-error' : ''}}">
                            <label>Password</label>
                            <label class="input">
                                <span class="input-icon input-icon-prepend fa fa-key"></span>
                                <input type="password" name="password" placeholder="Password" required="">
                                <span class="tooltip tooltip-top-left"><i class="fa fa-key text-cyan-dark"></i> Please enter your password</span>
                            </label>
                        </div>

                        <div class="form-group {{ $errors->has('errmsg') ? 'has-error' : ''}}">
                        {!! $errors->first('errmsg', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="row">
                            <div class="col-xs-6">
                                <button type="submit" class="btn btn-info btn-block"><span class="glyphicon glyphicon-log-in"></span> Login</button>
                            </div>
                            <div class="col-xs-6">
                                <button type="submit" class="btn btn-warning btn-block"><span class="fa fa-info"></span> Manual Bantuan</button>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </form>


</div>


<script src="{{URL::asset('clouds/backend/template/js/lib/jquery.min.js')}}"></script>
<script src="{{URL::asset('clouds/backend/template/js/bootstrap/bootstrap.min.js')}}"></script>
<script !src="">
    $(document).ready(function () {
        if($('#content').length > 0)
        {
            window.location.href='{{url('/')}}';
        }
    });
    @include('backend/.includes.indexjs')
    function initPage(){
        $('.lobipanel').lobiPanel();
        $('#demoPanel11').lobiPanel();
        $('#lobipanel-basic').lobiPanel();
        $('#lobipanel-custom-control').lobiPanel({
            reload: false,
            close: false,
            editTitle: false
        });
        $('#lobipanel-font-awesome').lobiPanel({
            reload: {
                icon: 'fa fa-refresh'
            },
            editTitle: {
                icon: 'fa fa-edit',
                icon2: 'fa fa-save'
            },
            unpin: {
                icon: 'fa fa-arrows'
            },
            minimize: {
                icon: 'fa fa-chevron-up',
                icon2: 'fa fa-chevron-down'
            },
            close: {
                icon: 'fa fa-times-circle'
            },
            expand: {
                icon: 'fa fa-expand',
                icon2: 'fa fa-compress'
            }
        });
        $('#lobipanel-constrain-size').lobiPanel({
            minWidth: 300,
            minHeight: 300,
            maxWidth: 600,
            maxHeight: 480
        });
        $('#lobipanel-from-url').on('loaded.lobiPanel', function(ev, lobiPanel){
            var $body = lobiPanel.$el.find('.panel-body');
            $body.html('<div class="highlight"><pre><code>' + $body.html() + '</code></pre></div>');
            hljs.highlightBlock($body.find('code')[0]);
        });
        $('#lobipanel-from-url').lobiPanel({
            loadUrl: 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.css',
            bodyHeight: 400
        });
        $('#lobipanel-multiple .panel').lobiPanel({
            sortable: true
        });
    }
</script>

</body>
</html>
