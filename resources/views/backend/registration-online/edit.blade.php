<div id="form-registration-online">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h4>Edit Pendaftaran Online #{{ $province->Id }}</h4>
                            </div>
                        </div>

                        <div class="panel-body no-padding">


                        {!! Form::model($province, [
                            'method' => 'PATCH',
                            'url' => ['HomeAdmin/registration-online', $province->Id],
                            'class' => 'lobi-form ajaxPost',
                            'files' => true
                        ]) !!}

                        @include ('backend/.registration-online.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>

</div>

