
    <div id="form-layouts">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>Create Pendaftaran Online</h4>
                        </div>
                    </div>
                    <div class="panel-body no-padding">
                        {!! Form::open(['url' => 'HomeAdmin/registration-online', 'class' => 'lobi-form ajaxPost', 'files' => true]) !!}

                        @include ('backend/.registration-online.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>

