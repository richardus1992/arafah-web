<div id="dashboard" class="bg-white padding-15">
    <div class="right_col" role="main">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="x_content">
                    <h1>Hello, {{session('admin')->UserName}} </h1>
                    <p>Selamat Datang Di Halaman Admin</p>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="tile border-1 border-success text-gray">
                    <a href="#HomeAdmin/employee-actvdaily">
                        <div class="tile-body bg-white">
                            <h2 class="margin-top-5 text-success" id="actvdailypersen">00</h2>
                            <h5><b>Total Kunjungan</b></h5>
                            <i class="fa fa-newspaper-o fa-4x vertical-align-middle text-right text-success"></i>
                        </div>
                    </a>
                    <a href="#HomeAdmin/employee-actvdaily" class="tile-footer bg-success link-white-inverse">
                        <h4 class="no-margin">
                            Aktifitas
                            <i class="fa fa-chevron-circle-right pull-right"></i>
                        </h4>
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="tile border-1 border-primary text-gray">
                    <a href="#HomeAdmin/employee-absen">
                        <div class="tile-body bg-white">
                            <h2 class="margin-top-5 text-primary" id="presance">00</h2>
                            <h5><b>Riwayat Jalan</b></h5>
                            <i class="fa fa-check fa-4x vertical-align-middle text-right text-primary"></i>
                        </div>
                    </a>
                    <a href="#HomeAdmin/employee-absen" class="tile-footer bg-primary link-white-inverse">
                        <h4 class="no-margin">
                            Kehadiran
                            <i class="fa fa-chevron-circle-right pull-right"></i>
                        </h4>
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="tile text-gray-lighter bg-info">
                    <div class="tile-body">
                        <h3 class="margin-top-5 text-overflow" id="tppworkloadbeforemonth">00</h3>
                        <h5><b>Rawat Inap</b></h5>
                        <i class="fa fa-money fa-4x vertical-align-middle text-right text-white"></i>
                    </div>
                    <a href="#" class="tile-footer bg-info-light link-white-inverse">
                        <h4 class="no-margin"  id="tppwbeforemontname">
                            Details
                            <i class="fa fa-chevron-circle-right pull-right"></i>
                        </h4>
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="tile text-gray-lighter bg-primary-light">
                    <div class="tile-body">
                        <h3 class="margin-top-5 text-overflow" id="tppworkloadthismonth"> 00</h3>
                        <h5><b> Bulan Ini</b></h5>
                        <i class="fa fa-money fa-4x vertical-align-middle text-right text-white"></i>
                    </div>
                    <a href="#" class="tile-footer bg-primary link-white-inverse">
                        <h4 class="no-margin"  id="tppwthismontname">
                            Details
                            <i class="fa fa-chevron-circle-right pull-right"></i>
                        </h4>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="bar-chart" class="panel panel-light">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>Grafik Kunjungan</h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div>
                            <canvas width="400" height="140"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script !src="">
   @include('backend/.includes.indexjs')
   @include('backend.includes.selectjs')
   @include('backend.includes.chartjs')
   function initPageChart(){
       $('.panel').lobiPanel({
           reload: false,
           editTitle: false,
           sortable: true
       });

       var options = {
           responsive: true
       };
       var
           barChart;

       var barData,
           barChartData;
       var COLOR1 = LobiAdmin.lightenColor(LobiAdmin.DEFAULT_COLOR, -15);
       var COLOR3 = LobiAdmin.fadeOutColor(LobiAdmin.DEFAULT_COLOR, 20);
       var COLOR5 = LobiAdmin.fadeOutColor(LobiAdmin.DEFAULT_COLOR, 60);
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
       (function(){
           // Get the context of the canvas element we want to select
           var ctx = $("#bar-chart canvas")[0].getContext("2d");
           barData = {
               labels: ["Januari", "Febuari", "Maret", "April", "Mei", "June", "Juli","Agustus","September","Oktober","November","Desember"],
               datasets: fillChartJsColors('bar', [
                   {
                       label: "My First dataset",
                       strokeColor: LobiAdmin.fadeOutColor(LobiAdmin.DEFAULT_COLOR, 50),
                       data: [65, 59, 80, 81, 56, 55, 40,51,55,61,23,11]
                   }
               ])
           };
           barChart = new Chart(ctx).Bar(barData, options);
       })();

       var randomScalingFactor = function(){ return Math.round(Math.random() * 100)};

       barChartData = {
           labels : ["January","February","March","April","May","June","July", "August", "September", "October"],
           datasets : fillChartJsColors('bar', [
               {
//                        fillColor : "rgba(220,220,220,0.5)",
                   strokeColor : COLOR5,
//                        highlightFill: "rgba(220,220,220,0.75)",
//                        highlightStroke: "rgba(220,220,220,1)",
                   data :  [60, 55, 71, 67, 81, 100, 49, 62, 69, 75]
               },
               {
//                        fillColor : "rgba(151,187,205,0.5)",
                   strokeColor : COLOR3,
//                        highlightFill : "rgba(151,187,205,0.75)",
//                        highlightStroke : "rgba(151,187,205,1)",
                   data : [40, 81, 47, 82, 42, 73, 35, 42, 52, 91]
               },
               {
//                        fillColor : "rgba(240,73,73,0.5)",
                   strokeColor : COLOR1,
//                        highlightFill : "rgba(240,73,73,0.75)",
//                        highlightStroke : "rgba(240,73,73,1)",
                   data : [49, 79, 48, 38, 86, 82, 62, 99, 95, 96]
               }
           ])
       };
       var ctx = $("#stacked-bar-chart canvas")[0].getContext("2d");
       $('body').on('beforePageLoad.lobiAdmin.1', function(){
           barChart.destroy();
           $('body').off('beforePageLoad.lobiAdmin.1');
       });


       $("#bar-chart").on('onFullScreen.lobiPanel onSmallSize.lobiPanel resizeStop.lobiPanel onPin.lobiPanel onUnpin.lobiPanel', function(ev, lobiPanel){
           setTimeout(function(){
               barChart.destroy();
               barChart = new Chart(barChart.chart.ctx).Bar(barData, options);
           }, 100);
       });

   }
</script>



