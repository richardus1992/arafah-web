@if($errors->has('errmsg'))
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        <strong>Error</strong> : {{ $errors->first('errmsg', ':message') }}
    </div>
@endif
<fieldset>
<div class="row">
    <div class="col-md-6">
        <div class="form-group {{ $errors->has('EmpName') ? 'has-error' : ''}}">
            {!! Form::label('EmpName', 'Nama Pegawi', ['class' => 'control-label']) !!}
            <label class="input">
                {!! Form::text('EmpName', session('admin')->EmployeName, ['class' => 'form-control','readonly']) !!}
                {!! $errors->first('EmpName', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group {{ $errors->has('UserName') ? 'has-error' : ''}}">
            {!! Form::label('UserName', 'User Login', ['class' => 'control-label']) !!}
            <label class="input">
                {!! Form::text('UserName', session('admin')->UserName, ['class' => 'form-control','readonly']) !!}
                {!! $errors->first('UserName', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group {{ $errors->has('PasswordOld') ? 'has-error' : ''}}">
            {!! Form::label('PasswordOld', 'Password Lama', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;">* Wajib Di Isi</small>
            <label class="input">
                {!! Form::text('PasswordOld', null, ['class' => 'form-control']) !!}
                {!! $errors->first('PasswordOld', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{ $errors->has('PasswordNew') ? 'has-error' : ''}}">
            {!! Form::label('PasswordNew', 'Password Baru', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;">* Wajib Di Isi</small>
            <label class="input">
                {!! Form::text('PasswordNew', null, ['class' => 'form-control']) !!}
                {!! $errors->first('PasswordNew', '<p class="help-block">:message</p>') !!}
            </label>

        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{ $errors->has('PasswordRep') ? 'has-error' : ''}}">
            {!! Form::label('PasswordRep', 'Ulangi Password Baru', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;">* Wajib Di Isi</small>
            <label class="input">
                {!! Form::text('PasswordRep', null, ['class' => 'form-control']) !!}
                {!! $errors->first('PasswordRep', '<p class="help-block">:message</p>') !!}
            </label>

        </div>
    </div>
</div>
</fieldset>
<footer>

    <a href="#HomeAdmin/my-profile" title="Back"><button type="button" class="btn btn-warning btn-pretty"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
    {!! Form::button(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-info btn-pretty','type'=>'submit']) !!}

</footer>
<script>
    @include('backend.includes.selectjs')

</script>