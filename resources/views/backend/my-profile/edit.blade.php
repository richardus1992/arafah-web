<div id="form-city">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h4>Edit Profile #{{ $myprofile->Id }}</h4>
                            </div>
                        </div>

                        <div class="panel-body no-padding">


                        {!! Form::model($myprofile, [
                            'method' => 'PATCH',
                            'url' => ['HomeAdmin/my-profile', $myprofile->Id],
                            'class' => 'lobi-form ajaxPost',
                            'files' => true
                        ]) !!}

                        @include ('backend/.my-profile.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>

</div>

