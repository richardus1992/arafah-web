<div id="data-city">
    <div class="panel panel-light" id="lobipanel-Mastercity">
        <div class="panel-heading">
            <div class="panel-title">
                <h4>Profile Saya</h4>
            </div>
        </div>
        <div class="panel-body">
            <div class="profile-page padding-15">
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-header">
                            <div class="row row-header">
                                <div class="col-lg-6 background">
                                    <a href="#HomeAdmin/my-profile/{!! session('admin')->ID !!}/edit" class="btn btn-info btn-pretty btn-follow btn-xl">Edit Profile</a>
                                </div>
                                <div class="col-lg-6 col-xs-12">
                                    <img src="{{asset('clouds/backend/file/image/arafah100.png')}}" alt="" class="img-thumbnail avatar-image" />
                                    <div class="header-text">
                                        <h1>{!! session('admin')->EmployeName !!}</h1>
                                        <h4>{!! session('admin')->EmployeName !!}</h4>
                                        <ul>
                                            <li><i class="fa fa-users text-info"></i> {!! session('admin')->UserName !!}</li>
                                        </ul>

                                        <span>

                                </span>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script !src="">

    hashReplace('HomeAdmin/my-profile#blank');
</script>
</div>

