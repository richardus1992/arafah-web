<div id="data-patient">
    <div class="panel panel-light" id="lobipanel-MasterProvince">
        <div class="panel-heading">
            <div class="panel-title">
                <h4>Master Pasien</h4>
            </div>
        </div>
        <div class="panel-body">
                <div class="table-responsive">
                    <table id="MasterProvince" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="with-input"><input type="text" class="form-control" placeholder="ID"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Nama"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="JK"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Tgl Lahir"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Pernikahan"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Alamat"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Phone"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Pendidikan"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Provinsi"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Kota"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Kecamatan"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="CreateAt"/></th>
                            <th></th>
                        </tr>
                        <tr>
                            <th>#</th>
                            <th>Nama Pasien</th>
                            <th>JK</th>
                            <th>Tgl Lahir</th>
                            <th>Pernikahan</th>
                            <th>Alamat</th>
                            <th>Phone</th>
                            <th>Pendidikan</th>
                            <th>Provinsi</th>
                            <th>Kota</th>
                            <th>Kecamatan</th>
                            <th>CreateAt</th>
                            <th>Action</th>

                        </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
</div>
<script !src="">
   @include('backend/.includes.indexjs')
   @include('backend.includes.selectjs')
   function initPage(){
       $('#lobipanel-MasterProvince').lobiPanel({
           reload: false,
           close: false,
           unpin: false,
           editTitle: false
       });
          var table=  $('#MasterProvince').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('data-patient') }}',
            columns: [
                  {data: 'Id', name: 'psn.ID_PASIEN'},
                  {data: 'Name', name: 'psn.NAMA_PASIEN'},
                  {data: 'Sex', name: 'psn.JK_PASIEN'},
                  {data: 'DateBirth', name: 'psn.TGLLAHIR_PASIEN'},
                  {data: 'Married', name: 'psn.STATUS_PERKAWINAN'},
                  {data: 'Address', name: 'psn.ALAMAT_PASIEN'},
                  {data: 'Phone', name: 'psn.TELP_PASIEN'},
                  {data: 'Studying', name: 'psn.PENDIDIKAN'},
                  {data: 'Province', name: 'pvc.NAMA_KEPENDUDUKAN'},
                  {data: 'City', name: 'cty.NAMA_KEPENDUDUKAN'},
                  {data: 'Districts', name: 'dst.NAMA_KEPENDUDUKAN'},
                  {data: 'CreateAt', name: 'psn.TGL_INPUT'},
                  {data: 'action', name: 'action'}
            ]
          });

           $("#MasterProvince thead th input[type=text]").on( 'keyup change', function (e) {
               if( e.keyCode == 13) {
                   table
                           .column( $(this).closest('th').index()+':visible' )
                           .search( this.value )
                           .draw();
               }
               // Ensure we clear the search if they backspace far enough
               if(this.value == "") {
                   table
                           .column( $(this).closest('th').index()+':visible' )
                           .search( this.value )
                           .draw();
               }

           });
       // Apply the search
           $("#MasterProvince thead th select").on('change', function () {

               var val = $.fn.dataTable.util.escapeRegex(
                       $(this).val()
               );

               table
                       .column( $(this).closest('th').index()+':visible' )
                       .search( this.value )
                       .draw();

           } );

            $("#MasterProvince_filter").hide();

        }
    hashReplace('HomeAdmin/patient#blank');
</script>
</div>

