<fieldset>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('IdCountry') ? 'has-error' : ''}}">
            {!! Form::label('IdCountry', 'Negara', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;">* Wajib Di Pilih</small>
            <label class="input">
                {!! Form::select('IdCountry', [''=>'- Pilih Kota/kab -']+$country->toArray(), null, array('class' => 'form-control select2-demo')) !!}

                {!! $errors->first('IdCountry', '<p class="help-block">:message</p>') !!}
            </label>

        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('Name') ? 'has-error' : ''}}">
            {!! Form::label('Name', 'Name', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;">* Wajib Di Isi</small>
            <label class="input">
                {!! Form::text('Name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('Name', '<p class="help-block">:message</p>') !!}
            </label>
            
        </div>
    </div>
</div>
</fieldset>
<footer>

    <a href="#HomeAdmin/province" title="Back"><button type="button" class="btn btn-warning btn-pretty"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
    {!! Form::button(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-info btn-pretty','type'=>'submit']) !!}

</footer>
<script>
    @include('backend.includes.selectjs')

</script>