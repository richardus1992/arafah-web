
    <div class="right_col" role="main">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Master Pasien {{ $item->Id }}</div>
                    <div class="panel-body">

                        <a href="#HomeAdmin/patient" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="#HomeAdmin/patient/{{$item->Id}}/edit" title="Edit Provinsi"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <a href="#HomeAdmin/patient/{{$item->Id}}/delete" title="Delete Provinsi"
                                                onclick="javascript:return confirm('Yakin ingin menghapus data?')"><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button></a>


                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $item->Id }}</td>
                                    <th> Name </th>
                                    <td> {{ $item->Name }} </td>
                                    <th> Jenis Kelamin </th>
                                    <td> {{ $item->Sex }} </td>
                                </tr>
                                <tr>
                                    <th>Tgl Lahir</th>
                                    <td>{{ $item->DateBirth }}</td>
                                    <th> Pernikahan </th>
                                    <td> {{ $item->Married }} </td>
                                    <th> Alamat </th>
                                    <td> {{ $item->Address }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

