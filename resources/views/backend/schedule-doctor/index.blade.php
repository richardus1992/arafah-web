<div id="data-schedule-doctor">
    <div class="panel panel-light" id="lobipanel-MasterProvince">
        <div class="panel-heading">
            <div class="panel-title">
                <h4>Jawal Dockter</h4>
            </div>
        </div>
        <div class="panel-body">
            <a href="#HomeAdmin/schedule-doctor/create" class="btn btn-success btn-sm" title="Add New Kokab">
                <i class="fa fa-plus" aria-hidden="true"></i> Add New
            </a>
            <br/>
            <br/>
                <div class="table-responsive">
                    <table id="MasterProvince" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="with-input"><input type="text" class="form-control" placeholder="ID"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Status Aktif"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Nama  Dokter"/></th>
                            <th class="with-input">
                                {!! Form::select('DayName', [''=>'- Pilih Hari -']+$dayname, null, array('class' => 'form-control select2-demo')) !!}
                            </th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Jam Mulai"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Jam Selesai"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Ruang Pelayanan"/></th>
                            <th></th>
                        </tr>
                        <tr>
                            <th>#</th>
                            <th>Status Aktif</th>
                            <th>Nama Dokter</th>
                            <th>Hari</th>
                            <th>Jam Mulai</th>
                            <th>Jam Selesai</th>
                            <th>Ruang Pelayanan</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
</div>
<script !src="">
   @include('backend/.includes.indexjs')
   @include('backend.includes.selectjs')
   var table;
   function initPage(){
       $('#lobipanel-MasterProvince').lobiPanel({
           reload: false,
           close: false,
           unpin: false,
           editTitle: false
       });
          table=  $('#MasterProvince').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('data-schedule-doctor') }}',
            order: [[ 0, "desc" ]],
            columns: [
                  {data: 'Id', name: 'msj.ID_JADWAL'},
                {
                    data:   "Status",
                    name:   "msj.flag_tampil",
                    render: function ( data, type, row ) {
                        if ( type === 'display' ) {
                            return data;
                            // return '<label class="switch"><input class="editor-active" type="checkbox"'+data+'><i data-switch-off="OFF" data-switch-on="ON" value="'+data+'"></i></label>';
                        }
                        return data;
                    },
                    className: "dt-body-center"
                },

                  {data: 'EmployeName', name: 'pgw.NAMA_PGW'},
                  {data: 'DayName', name: 'msj.HARI_JADWAL'},
                  {data: 'TimeStart', name: 'msj.JAM_MULAI_JADWAL'},
                  {data: 'TimeEnd', name: 'msj.JAM_BERAKHIR_JADWAL'},
                  {data: 'RoomService', name: 'jlr.NAMA_LAYANAN_RS'},
                  {data: 'action', name: 'action'}

            ],
              rowCallback: function ( row, data ) {
                  // Set the checked state of the checkbox in the table
                  $('input.editor-active', row).prop( 'checked', data.Status == 'checked' );
              }
          });


           $("#MasterProvince thead th input[type=text]").on( 'keyup change', function (e) {
               if( e.keyCode == 13) {
                   table
                           .column( $(this).closest('th').index()+':visible' )
                           .search( this.value )
                           .draw();
               }
               // Ensure we clear the search if they backspace far enough
               if(this.value == "") {
                   table
                           .column( $(this).closest('th').index()+':visible' )
                           .search( this.value )
                           .draw();
               }

           });
       // Apply the search
           $("#MasterProvince thead th select").on('change', function () {

               var val = $.fn.dataTable.util.escapeRegex(
                       $(this).val()
               );

               table
                       .column( $(this).closest('th').index()+':visible' )
                       .search( this.value )
                       .draw();

           } );



            $("#MasterProvince_filter").hide();

        }
       $('#MasterProvince').on( 'change', 'input.check-data', function () {
           var id=$(this).attr('idaction');
           var status=$(this).attr('statusview');
           $.ajax({
               url:"{{ route('schedule-doctor.update-status') }}",
               method:"GET",
               data:{Status:status,Id:id},
               success:function(result)
               {
                   $('#MasterProvince').DataTable().ajax.reload();
               }

           }).always(function () {
               $('#MasterProvince').DataTable().ajax.reload();
           });
       });



    hashReplace('HomeAdmin/schedule-doctor#blank');
</script>
