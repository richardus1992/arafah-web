<fieldset>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('IdEmploye') ? 'has-error' : ''}}">
            {!! Form::label('IdEmploye', 'Pegawai', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;">* Wajib Di Pilih</small>
            <label class="input">
                {!! Form::select('IdEmploye', [''=>'- Pilih Pegawai -']+$Employe->toArray(), null, array('class' => 'form-control select2-demo')) !!}

                {!! $errors->first('IdEmploye', '<p class="help-block">:message</p>') !!}
            </label>

        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('IdServiceType') ? 'has-error' : ''}}">
            {!! Form::label('IdServiceType', 'Jenis Pelayanan', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;">* Wajib Di Pilih</small>
            <label class="input">
                {!! Form::select('IdServiceType', [''=>'- Pilih Jenis Pelayana -']+$ServiceType->toArray(), null, array('class' => 'form-control select2-demo')) !!}

                {!! $errors->first('IdServiceType', '<p class="help-block">:message</p>') !!}
            </label>

        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-3">
        <div class="form-group {{ $errors->has('DateIn') ? 'has-error' : ''}}">
            {!! Form::label('DateIn', 'Tgl Mask', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;">* Wajib Di Massukan</small>
            <label class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                {!! Form::text('DateIn',null, array('class' => 'date-picker form-control','placeholder'=>'Tanggal Masuk','readonly','id'=>'DateIn')) !!}
                {!! $errors->first('DateIn', '<p class="help-block">:message</p>') !!}
            </label>

        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group {{ $errors->has('TimeIn') ? 'has-error' : ''}}">
            {!! Form::label('TimeIn', 'Jam Masuk', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;">* Wajib Di Massukan</small>
            <label class="input-group">
                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                {!! Form::text('TimeIn', null,['class' => 'form-control inputmask', 'data-mask'=>'99:99']) !!}
                {!! $errors->first('TimeIn', '<p class="help-block">:message</p>') !!}
            </label>

        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group {{ $errors->has('DateOut') ? 'has-error' : ''}}">
            {!! Form::label('DateOut', 'Tgl Selesai', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;">* Wajib Di Massukan</small>
            <label class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                {!! Form::text('DateOut',null, array('class' => 'date-picker form-control','placeholder'=>'Tanggal Selesai','readonly','id'=>'DateOut')) !!}
                {!! $errors->first('DateOut', '<p class="help-block">:message</p>') !!}
            </label>

        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group {{ $errors->has('TimeEnd') ? 'has-error' : ''}}">
            {!! Form::label('TimeEnd', 'Jam Pulang', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;">* Wajib Di Massukan</small>
            <label class="input-group">
                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                {!! Form::text('TimeEnd', null,['class' => 'form-control inputmask', 'data-mask'=>'99:99']) !!}
                {!! $errors->first('TimeEnd', '<p class="help-block">:message</p>') !!}
            </label>

        </div>
    </div>
</div>
</fieldset>
<footer>

    <a href="#HomeAdmin/province" title="Back"><button type="button" class="btn btn-warning btn-pretty"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
    {!! Form::button(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-info btn-pretty','type'=>'submit']) !!}

</footer>
<script>
    @include('backend.includes.selectjs')
    @include('backend.includes.daterangepicker')
    function initDateRangePicker(){
        $('.date-picker').datepicker({
            autoclose: true,
            format: 'd/mm/yyyy'
        });
        $('.datepicker-demo-autoclose').datepicker({
            autoclose: true,
            format: 'd/mm/yyyy'
        });

    }
</script>