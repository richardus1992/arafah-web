    <div id="form-users">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h4>Edit User</h4>
                            </div>
                        </div>

                        <div class="panel-body no-padding">


                        {!! Form::model($user, [
                            'method' => 'PATCH',
                            'url' => ['HomeAdmin/users', $user->Id],
                            'class' => 'lobi-form ajaxPost',
                            'files' => true
                        ]) !!}

                        @include ('backend/.users.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>

</div>

