
    <div class="right_col" role="main">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Detail User</div>
                    <div class="panel-body">

                        <a href="#HomeAdmin/users" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="#HomeAdmin/users/{{$user->Id}}/edit" title="Edit User"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <a href="#HomeAdmin/users/{{$user->Id}}/delete" title="Delete Kokab"
                                                onclick="javascript:return confirm('Yakin ingin menghapus data?')"><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button></a>


                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $user->Id }}</td>
                                    </tr>
                                    <tr><th> Name </th><td> {{ $user->empname }} </td></tr>
                                    <tr><th> UserName </th><td> {{ $user->UserName }} </td></tr>
                                    <tr><th> Password </th><td> {{ $user->pwd }} </td></tr>
                                    <tr><th> Type User </th><td> {{ $user->tyuname }} </td></tr>
                                    <tr><th> Created At </th><td> {{ $user->CreateAt }} </td></tr>
                                    <tr><th> Updated At </th><td> {{ $user->UpdateAt }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

