<fieldset>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('UserName') ? 'has-error' : ''}}">
            {!! Form::label('UserName', 'Username', ['class' => 'control-label']) !!}
            <label class="input">
                {!! Form::text('UserName', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('UserName', '<p class="help-block">:message</p>') !!}
            </label>
            <small class="text-danger">* Wajib Di Isi</small>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('IdEmploye') ? 'has-error' : ''}}">
            {!! Form::label('IdEmploye', 'Karyawan', ['class' => 'control-label']) !!}
            <label class="input">

                {!! Form::select('IdEmploye', $employee, null, array('class' => 'form-control chosen-demo')) !!}
                {!! $errors->first('IdEmploye', '<p class="help-block">:message</p>') !!}
            </label>
            <small class="text-danger">* Wajib Di Isi</small>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('Password') ? 'has-error' : ''}}">
            {!! Form::label('Password', 'Password', ['class' => 'control-label']) !!}
            <label class="input">
                {!! Form::text('Password',isset($user)?Crypt::decrypt($user->pwd):null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('Password', '<p class="help-block">:message</p>') !!}
            </label>
            <small class="text-danger">* Wajib Di Isi</small>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('TypeUser') ? 'has-error' : ''}}">
            {!! Form::label('TypeUser', 'Typeuser', ['class' => 'control-label']) !!}
            <label class="input">
                {!! Form::select('TypeUser', $typeuser, null, array('class' => 'form-control chosen-demo')) !!}
                {!! $errors->first('TypeUser', '<p class="help-block">:message</p>') !!}
            </label>
        </div>
    </div>
</div>

</fieldset>
<footer>

    <a href="#HomeAdmin/users" title="Back"><button type="button" class="btn btn-warning btn-pretty"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
    {!! Form::button(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-info btn-pretty','type'=>'submit']) !!}

</footer>

<script !src="">

    @include('backend/.includes.selectjs')

</script>