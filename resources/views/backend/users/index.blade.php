<div id="data-users">
    <div class="panel panel-light" id="lobipanel-users">
        <div class="panel-heading">
            <div class="panel-title">
                <h4>Master User</h4>
            </div>
        </div>
        <div class="panel-body">
            <a href="#HomeAdmin/users/create" class="btn btn-success btn-sm" title="Add New Kokab">
                <i class="fa fa-plus" aria-hidden="true"></i> Add New
            </a>
            <br/>
            <br/>
            <div class="table-responsive">
                <table id="Users" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="with-input"><input type="text" class="form-control" placeholder="ID"/></th>
                        <th class="with-input"><input type="text" class="form-control" placeholder="Karyawan"/></th>
                        <th class="with-input"><input type="text" class="form-control" placeholder="UserName"/></th>
                        <th class="with-input"><input type="text" class="form-control" placeholder="TypeUser"/></th>
                        <th class="with-input"><input type="text" class="form-control" placeholder="CreatAt"/></th>
                        <th class="with-input"><input type="text" class="form-control" placeholder="UpdateAt"/></th>
                        <th></th>
                    </tr>
                    <tr>
                        <th>#</th>
                        <th>Karyawan</th>
                        <th>UserName</th>
                        <th>TypeUser</th>
                        <th>CreatAt</th>
                        <th>UpdateAt</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                </table>
            </div>
        </div>
    </div>
    <script !src="">
       @include('backend/.includes.indexjs')

        function initPage(){
           $('#lobipanel-users').lobiPanel({
               reload: false,
               close: false,
               unpin: false,
               editTitle: false
           });
            var table=$('#Users').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('data-users') }}',
                columns: [
                    {data: 'Id', name: 'usr.ID'},
                    {data: 'Name', name: 'emp.NAMA_PGW'},
                    {data: 'UserName', name: 'usr.USERNAME'},
                    {data: 'typeuser', name: 'tyu.NAME'},
                    {data: 'CreateAt', name: 'usr.CREATEAT'},
                    {data: 'UpdateAt', name: 'usr.UPDATEAT'},
                    {data: 'action', name: 'action'}
                ]

            });

           $("#Users thead th input[type=text]").on( 'keyup change', function (e) {
               if( e.keyCode == 13) {
                   table
                           .column( $(this).closest('th').index()+':visible' )
                           .search( this.value )
                           .draw();
               }
               // Ensure we clear the search if they backspace far enough
               if(this.value == "") {
                   table
                           .column( $(this).closest('th').index()+':visible' )
                           .search( this.value )
                           .draw();
               }

           });
           // Apply the search
           $("#Users thead th select").on('change', function () {

               table
                       .column( $(this).closest('th').index()+':visible' )
                       .search( this.value )
                       .draw();

           } );

           $("#Users_filter").hide();
        }
       hashReplace('HomeAdmin/users#blank');
    </script>
</div>

