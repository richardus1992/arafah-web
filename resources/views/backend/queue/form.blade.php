<fieldset>
<div class="row">

    <div class="col-sm-12">
        <div class="form-group {{ $errors->has('Name') ? 'has-error' : ''}}">
            {!! Form::label('Name', 'Name', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;">* Wajib Di Isi</small>
            <label class="input">
                {!! Form::text('Name', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('Name', '<p class="help-block">:message</p>') !!}
            </label>
            
        </div>
    </div>
</div>
</fieldset>
<footer>

    <a href="#HomeAdmin/queue" title="Back"><button type="button" class="btn btn-warning btn-pretty"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
    {!! Form::button(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-info btn-pretty','type'=>'submit']) !!}

</footer>
<script>
    @include('backend.includes.selectjs')

</script>