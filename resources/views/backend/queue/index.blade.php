@if(Session::has('errEmp'))
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        <strong>Error</strong> : <br>
    </div>
@endif

<div id="data-order-queue">
    <div class="panel panel-light" id="lobipanel-MasterProvince">
        <div class="panel-heading">
            <div class="panel-title">
                <h4>Antrian Pasien</h4>
            </div>
        </div>
        <div class="panel-body">
            <button id="btnScrollLeft" class="btn btn-info btn-circle btn-lg center"
                    style=" position: fixed;top: 50%; z-index: 1;">
                &lt;
            </button>
            <div class="table-responsive">
                <table id="MasterProvince" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="with-input"><input type="text" class="form-control" placeholder="NO ORDER"/></th>
                        <th class="with-input"><input type="text" class="form-control" placeholder="NAMA"/></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr>
                        <th>#</th>
                        <th>Nama Pasien</th>
                        <th>Nik</th>
                        <th>Tgl Periksa</th>
                        <th>Tgl Daftar</th>
                        <th>Kelamin</th>
                        <th>Status Nikah</th>
                        <th>Alamat</th>
                        <th>Provinsi</th>
                        <th>Kota</th>
                        <th>Poliklinik</th>
                        <th>Metode Bayar</th>
                        <th>Konfirmasi Kedatangan</th>
                        <th>Catatan</th>
                        <th>Aksi</th>
                    </tr>
                    <button id="btnScrollRight" class="btn btn-info btn-circle btn-lg text-center"   style=" position: fixed;top: 50%; z-index: 1; right:40px;">&gt;
                    </button>
                    </thead>
                </table>
            </div>

        </div>
    </div>
</div>
<script !src="">
    @include('backend/.includes.indexjs')
    @include('backend.includes.selectjs')
    function initPage(){
        $('#lobipanel-MasterProvince').lobiPanel({
            reload: false,
            close: false,
            unpin: false,
            editTitle: false
        });
        var table=  $('#MasterProvince').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('data-queue') }}',
            order: [[ 0, "desc" ]],
            columns: [
                {data: 'Id', name: 'pdo.ID_ONLINE'},
                {data: 'Name', name: 'pdo.NAMA_PASIEN'},
                {data: 'Nik', name: 'pdo.NIK'},
                {data: 'DateVisit', name: 'pdo.TGL_PELAYANAN'},
                {data: 'CreateAt', name: 'pdo.CREATEAT'},
                {data: 'Sex', name: 'pdo.JK_PASIEN'},
                {data: 'Married', name: 'pdo.STATUS_PERKAWINAN'},
                {data: 'Addres', name: 'pdo.ALAMAT_PASIEN'},
                {data: 'Province', name: 'pvc.NAMA_KEPENDUDUKAN'},
                {data: 'City', name: 'cty.NAMA_KEPENDUDUKAN'},
                {data: 'RoomService', name: 'ply.NAMA_LAYANAN_RS'},
                {data: 'MethodePay', name: 'mtb.NAMA_METODE'},
                {data: 'Confirm', name: 'pdo.KONFIRMASI_KEDATANGAN'},
                {data: 'Remarks', name: 'pdo.KETERANGAN'},
                {data: 'action', name: 'action'}

            ]
        });

        $("#MasterProvince thead th input[type=text]").on( 'keyup change', function (e) {
            if( e.keyCode == 13) {
                table
                    .column( $(this).closest('th').index()+':visible' )
                    .search( this.value )
                    .draw();
            }
            // Ensure we clear the search if they backspace far enough
            if(this.value == "") {
                table
                    .column( $(this).closest('th').index()+':visible' )
                    .search( this.value )
                    .draw();
            }

        });
        // Apply the search
        $("#MasterProvince thead th select").on('change', function () {

            var val = $.fn.dataTable.util.escapeRegex(
                $(this).val()
            );

            table
                .column( $(this).closest('th').index()+':visible' )
                .search( this.value )
                .draw();

        } );

        $("#MasterProvince_filter").hide();

    }
    function scrollLeftTable(){
        console.log("Left");
        var leftPos = $(".table-responsive").scrollLeft();
        $(".table-responsive").animate({scrollLeft: leftPos - 200}, 100);

    }
    function scrollRightTable(){
        console.log("Right");
        var leftPos = $(".table-responsive").scrollLeft();
        $(".table-responsive").animate({scrollLeft: leftPos + 200}, 100);

    }
    hashReplace('HomeAdmin/queue#blank');
</script>

