<!DOCTYPE html>
<html lang="en">
<head>
    <title>Lobi Admin</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href={{asset('clouds/backend/template/img/logo/lobiadmin-logo-64.png')}} />
    @include('backend.includes.stylecss')


</head>
<body>
<nav class="navbar navbar-default navbar-header header">
    <a class="navbar-brand" href="#">
         <div class="navbar-brand-img"></div>

    </a>
    <!--Menu show/hide toggle button-->
    <ul class="nav navbar-nav pull-left show-hide-menu">
        <li>
            <a href="#" class="border-radius-0 btn font-size-lg" data-action="show-hide-sidebar">
                <i class="fa fa-bars"></i>
            </a>
        </li>
    </ul>

    <div class="navbar-items">
        <!--User avatar dropdown-->
        <ul class="nav navbar-nav navbar-right user-actions">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img class="user-avatar" src="{{asset('clouds/backend/template/img/avatar/team_64.png')}}" alt="..."/>
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">

                    <li><a href="{{URL::to('manage/logout')}}"><span class="glyphicon glyphicon-off"></span> &nbsp;&nbsp;Log out</a></li>
                </ul>
            </li>
        </ul>

    </div>
    <div class="clearfix-xxs"></div>
    <div class="navbar-items-2">
        <!--Choose languages dropdown-->

        <ul class="nav navbar-nav navbar-actions">
             <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="fa fa-envelope-o"></span>
                    <span class="badge badge-danger badge-xs">0</span>
                </a>
                <div class="dropdown-menu dropdown-notifications notification-messages border-1 animated-fast flipInX">
                    <div class="notifications-heading border-bottom-1 bg-white">
                        Messages
                        <div class="header-actions pull-right">
                            <a href="#lobimail" data-action="compose-email"><small><i class="fa fa-edit"></i> Create new</small></a>
                        </div>
                    </div>
                    <ul class="notifications-body max-h-300">
                        <li class="unread">
                            <a href="#lobimail" data-action="open-email" data-key="1" class="notification">
                                <img class="notification-image" src="{{asset('clouds/backend/template/img/avatar/team_64.png')}}" alt="">
                                <div class="notification-msg">
                                    <h4 class="notification-heading">George Darso</h4>
                                    <h5 class="notification-sub-heading">Happy birthday!!</h5>
                                    <p class="body-text">Happy birthday. Lorem ipsum dolor sit amor.</p>
                                </div>
                                <span class="notification-time">5 min. ago</span>
                            </a>
                        </li>
                    </ul>
                    <div class="notifications-footer text-center border-top-1 bg-white">
                        <a href="#lobimail">View all</a>
                    </div>
                </div>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="fa fa fa-users"></span>
                    <span class="badge badge-danger badge-xs">0</span>
                </a>
                <div class="dropdown-menu dropdown-notifications notification-messages border-1 animated-fast flipInX">
                    <div class="notifications-heading border-bottom-1 bg-white">
                        Messages
                        <div class="header-actions pull-right">
                            <a href="#lobimail" data-action="compose-email"><small><i class="fa fa-edit"></i> Create new</small></a>
                        </div>
                    </div>
                    <ul class="notifications-body max-h-300">
                        <li class="unread">
                            <a href="#lobimail" data-action="open-email" data-key="1" class="notification">
                                <img class="notification-image" src="{{asset('clouds/backend/template/img/avatar/team_64.png')}}" alt="">
                                <div class="notification-msg">
                                    <h4 class="notification-heading">George Darso</h4>
                                    <h5 class="notification-sub-heading">Happy birthday!!</h5>
                                    <p class="body-text">Happy birthday. Lorem ipsum dolor sit amor.</p>
                                </div>
                                <span class="notification-time">5 min. ago</span>
                            </a>
                        </li>
                    </ul>
                    <div class="notifications-footer text-center border-top-1 bg-white">
                        <a href="#lobimail">View all</a>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div class="clearfix"></div>
</nav>
<div class="menu">
        <div class="menu-header-buttons-wrapper clearfix">
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                        <img src="{{asset('clouds/backend/template/img/avatar/team_64.png')}}"/>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="text-center">
                        <h5><strong>{!! session('admin')->UserName !!}</strong></h5>
                        <h5>{!! session('admin')->EmployeName !!}</h5>

                    </div>
                </div>
            </div>

    </div>

    @include('backend.includes.sidebarleft')
    <div class="menu-collapse-line">
        <!--Menu collapse/expand icon is put and control from LobiAdmin.js file-->
        <div class="menu-toggle-btn" data-action="collapse-expand-sidebar"></div>
    </div>
</div>
<div id="main">
    <div id="ribbon" class="hidden-print">
        <a href="#HomeAdmin/index" class="btn-ribbon" data-container="#main" data-toggle="tooltip" data-title="Show dashboard"><i class="fa fa-home"></i></a>
        <span class="vertical-devider">&nbsp;</span>
        <button class="btn-ribbon" data-container="#main" data-action="reload" data-toggle="tooltip" data-title="Reload content by ajax"><i class="fa fa-refresh"></i></button>
        <ol class="breadcrumb">
        </ol>
    </div>
    <div id="content">

    </div>
</div>
<!--Setting box-->
@include('backend.includes.settinbox')

<!--Loading indicator for ajax page loading-->
<div class="spinner spinner-horizontal hide">
    <span class="spinner-text">Loading...</span>
    <div class="bounce1"></div>
    <div class="bounce2"></div>
    <div class="bounce3"></div>
</div>

@include('backend.includes.scriptjs')
</body>
</html>
