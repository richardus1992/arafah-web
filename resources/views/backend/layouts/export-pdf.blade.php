@extends('backend.layouts.print-potrait')
@section('content')
<style>
    @page { size : portrait }
    @page rotated { size : landscape }
    table { page : rotated }
</style>
<br>
<br>
<div></div>
<table class="datatable table table-striped table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
    <thead>
    <tr>
        @foreach($dataexport[0] as $key => $value)
            <th>{{ ucfirst($key) }}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($dataexport as $row)
        <tr>
            @foreach ($row as $value)
                <td>{!! $value !!}</td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>
<br>


@endsection