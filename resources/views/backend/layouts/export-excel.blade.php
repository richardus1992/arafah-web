<table class="datatable table table-striped table-bordered dt-responsive nowrap dataTable no-footer dtr-inline">
    <thead>
    <tr>
        @foreach($data[0] as $key => $value)
            <th>{{ ucfirst($key) }}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($data as $row)
        <tr>
            @foreach ($row as $value)
                <td>{!! str_replace('</br>','',$value) !!}</td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>