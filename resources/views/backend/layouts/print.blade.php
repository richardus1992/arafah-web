<!doctype html>
<html lang="en">
<head>
    <title>PI</title>
    <link href="{{ asset('clouds/backend/template/css/bootstrap.min.css') }}" media="all" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{{ asset('clouds/backend/js/lib/jquery.min.js') }}"></script>
    <style>

        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
            padding: 0 3px 0 3px;
            line-height: 1.42857143;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }
        table thead th {text-align: center !important;}
        table {font-size: 16px;}
        .table-bordered, .table-bordered>tbody>tr>td,
        .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td,
        .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
            border: 1px solid black;
        }
        .table>tbody>tr>td>label{font-weight: normal;}
        body{color: black !important;font-family: monospace;margin:auto;}

        @media print {
            .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
                padding: 0 3px 0 3px;
                line-height: 1.42857143;
                vertical-align: top;
                border-top: 1px solid #ddd;
            }
            table thead th {text-align: center !important;}
            table {font-size: 12px;}
            .table-bordered, .table-bordered>tbody>tr>td,
            .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td,
            .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th{
                border: 1px solid black;
            }
            .table>tbody>tr>td>label{font-weight: normal;}
            body{color: black !important;font-family: monospace;margin:auto;}
        }
    </style>
    <script !src="">
        $(window).load(function () {
            window.print();
            window.close();
        });
    </script>
</head>
<body>
<table width="100%" style="margin-bottom: -50px">
    <tr>
        <td>
            <img src="{{URL::asset('clouds/backend/template/img/logo/ic_image128.png')}}" alt=""><br>
            <label style="font-size: 9px">
                DINAS PASURUAN<br>
                PASURUAN
            </label>
        </td>
        <td align="right">Tanggal Cetak : {{date('d/m/Y H:i:s')}}</td>
    </tr>
</table>
@yield('content')
</body>
</html>