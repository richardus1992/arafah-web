
    <div class="right_col" role="main">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Provinsi {{ $item->Id }}</div>
                    <div class="panel-body">

                        <a href="#HomeAdmin/complain-type" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="#HomeAdmin/complain-type/{{$item->Id}}/edit" title="Edit Provinsi"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <a href="#HomeAdmin/complain-type/{{$item->Id}}/delete" title="Delete Provinsi"
                                                onclick="javascript:return confirm('Yakin ingin menghapus data?')"><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button></a>


                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $item->Id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Jeni </th>
                                        <td> {{ $item->Jenis }} </td>
                                    </tr>
                                    <tr>
                                        <th>Create At</th>
                                        <td> {{ $item->CreateAt }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

