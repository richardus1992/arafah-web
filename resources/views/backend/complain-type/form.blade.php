<fieldset>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group {{ $errors->has('Jenis') ? 'has-error' : ''}}">
            {!! Form::label('Jeni', 'Jenis', ['class' => 'control-label']) !!}
            <small class="text-danger" style="float: right;">* Wajib Di Isi</small>
            <label class="input">
                {!! Form::text('Jenis', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                {!! $errors->first('Jenis', '<p class="help-block">:message</p>') !!}
            </label>
            
        </div>
    </div>
</div>
</fieldset>
<footer>

    <a href="#HomeAdmin/complain-type" title="Back"><button type="button" class="btn btn-warning btn-pretty"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
    {!! Form::button(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-info btn-pretty','type'=>'submit']) !!}

</footer>
<script>
    @include('backend.includes.selectjs')

</script>