<div id="data-room-service">
    <div class="panel panel-light" id="lobipanel-MasterProvince">
        <div class="panel-heading">
            <div class="panel-title">
                <h4>Master Info Kamar</h4>
            </div>
        </div>
        <div class="panel-body">
                <div class="table-responsive">
                    <table id="MasterProvince" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="with-input"><input type="text" class="form-control" placeholder="ID"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Nama  Kamar"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Lantai"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Kapasitas"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Kelas"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Paviliun"/></th>
                        </tr>
                        <tr>
                            <th>#</th>
                            <th>Nama Kamar</th>
                            <th>Lantai</th>
                            <th>Kapasitas</th>
                            <th>Kelas</th>
                            <th>Paviliun</th>
                        </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
</div>
<script !src="">
   @include('backend/.includes.indexjs')
   @include('backend.includes.selectjs')
   function initPage(){
       $('#lobipanel-MasterProvince').lobiPanel({
           reload: false,
           close: false,
           unpin: false,
           editTitle: false
       });
          var table=  $('#MasterProvince').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('data-room-service') }}',
            order: [[ 0, "desc" ]],
            columns: [
                  {data: 'Id', name: 'mkri.ID_KAMAR_RWT_INAP'},
                  {data: 'Name', name: 'mkri.NAMA_ALIAS_RWT_INAP'},
                  {data: 'FloorLevel', name: 'mkri.LANTAI_RWT_INAP'},
                  {data: 'Capacity', name: 'mkri.KAPASITAS_RWT_INAP'},
                  {data: 'NameClass', name: 'mkrp.NAMA_KELAS_RINAP'},
                  {data: 'NamePaviliun', name: 'mpv.NAMA_PAVILIUN'}
            ]
          });

           $("#MasterProvince thead th input[type=text]").on( 'keyup change', function (e) {
               if( e.keyCode == 13) {
                   table
                           .column( $(this).closest('th').index()+':visible' )
                           .search( this.value )
                           .draw();
               }
               // Ensure we clear the search if they backspace far enough
               if(this.value == "") {
                   table
                           .column( $(this).closest('th').index()+':visible' )
                           .search( this.value )
                           .draw();
               }

           });
       // Apply the search
           $("#MasterProvince thead th select").on('change', function () {

               var val = $.fn.dataTable.util.escapeRegex(
                       $(this).val()
               );

               table
                       .column( $(this).closest('th').index()+':visible' )
                       .search( this.value )
                       .draw();

           } );

            $("#MasterProvince_filter").hide();

        }
    hashReplace('HomeAdmin/room-service#blank');
</script>
</div>

