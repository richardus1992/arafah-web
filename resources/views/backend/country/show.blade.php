
    <div class="right_col" role="main">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Country {{ $country->Id }}</div>
                    <div class="panel-body">

                        <a href="#HomeAdmin/country" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="#HomeAdmin/country/{{$country->Id}}/edit" title="Edit Provinsi"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <a href="#HomeAdmin/country/{{$country->Id}}/delete" title="Delete Provinsi"
                                                onclick="javascript:return confirm('Yakin ingin menghapus data?')"><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button></a>


                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <td>{{ $country->Id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Name </th>
                                        <td> {{ $country->Name }} </td>
                                    </tr>
                                    <tr>
                                        <th> User Entry </th>
                                        <td> {{ $country->UserEntry }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

