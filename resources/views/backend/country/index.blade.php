<div id="data-country">
    <div class="panel panel-light" id="lobipanel-Mastercountry">
        <div class="panel-heading">
            <div class="panel-title">
                <h4>Master Negara</h4>
            </div>
        </div>
        <div class="panel-body">
            <a href="#HomeAdmin/country/create" class="btn btn-success btn-sm" title="Add New Kokab">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
            </a>
            <br/>
            <br/>
                <div class="table-responsive">
                    <table id="Mastercountry" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="with-input"><input type="text" class="form-control" placeholder="ID"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="Provinsi"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="User Entry"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="CreateAt"/></th>
                            <th class="with-input"><input type="text" class="form-control" placeholder="UpdateAt"/></th>
                            <th></th>
                        </tr>
                        <tr>
                            <th>#</th>
                            <th>Provinsi</th>
                            <th>UserEntry</th>
                            <th>CreateAt</th>
                            <th>UpdateAt</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
</div>
<script !src="">
   @include('backend/.includes.indexjs')
   @include('backend.includes.selectjs')
   function initPage(){
       $('#lobipanel-Mastercountry').lobiPanel({
           reload: false,
           close: false,
           unpin: false,
           editTitle: false
       });
          var table=  $('#Mastercountry').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('data-country') }}',
            order: [[ 0, "desc" ]],
            columns: [
                  {data: 'Id', name: 'pvc.Id'},
                  {data: 'Name', name: 'pvc.Name'},
                  {data: 'UserEntry', name: 'usr.UserName'},
                  {data: 'CreateAt', name: 'pvc.CreateAt'},
                  {data: 'UpdateAt', name: 'pvc.UpdateAt'},
                  {data: 'action', name: 'action'}
            ]
          });

           $("#Mastercountry thead th input[type=text]").on( 'keyup change', function (e) {
               if( e.keyCode == 13) {
                   table
                           .column( $(this).closest('th').index()+':visible' )
                           .search( this.value )
                           .draw();
               }
               // Ensure we clear the search if they backspace far enough
               if(this.value == "") {
                   table
                           .column( $(this).closest('th').index()+':visible' )
                           .search( this.value )
                           .draw();
               }

           });
       // Apply the search
           $("#Mastercountry thead th select").on('change', function () {

               var val = $.fn.dataTable.util.escapeRegex(
                       $(this).val()
               );

               table
                       .column( $(this).closest('th').index()+':visible' )
                       .search( this.value )
                       .draw();

           } );

            $("#Mastercountry_filter").hide();

        }
    hashReplace('HomeAdmin/country#blank');
</script>
</div>

