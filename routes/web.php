<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => '/'], function () {
    Route::get('/', 'Auth\AdminControll@showLoginForm');
});

Route::group(['prefix' => 'manage'], function () {
    Route::get('/', 'Auth\AdminControll@showLoginForm');
    Route::post('/login', 'Auth\AdminControll@login');

    Route::get('/logout', 'Auth\AdminControll@logout');
});

Route::group(['middleware' => ['admin','access']], function () {
    Route::group(['prefix' => 'HomeAdmin'], function () {
        Route::get('/', 'BackEnd\HomeControll@Index');
        Route::get('index', 'BackEnd\HomeControll@indexhome');
        Route::get('block', 'BackEnd\HomeControll@blockAccess');

        Route::resource('master-module', 'BackEnd\MasterModuleController');
        Route::get('data-master-module', 'BackEnd\MasterModuleController@getData')->name('data-master-module');
        Route::get('master-module/{Id}/delete', 'BackEnd\MasterModuleController@destroy')->name('master-module.delete');

        Route::resource('history-table', 'BackEnd\HistoryTableController');
        Route::get('data-history-table', 'BackEnd\HistoryTableController@getData')->name('data-history-table');
        Route::get('history-table/{Id}/delete', 'BackEnd\HistoryTableController@destroy');

        Route::resource('users', 'BackEnd\UsersController');
        Route::get('data-users', 'BackEnd\UsersController@getData')->name('data-users');
        Route::get('users/{Id}/delete', 'BackEnd\UsersController@destroy')->name('users.delete');

        Route::resource('type-user', 'BackEnd\TypeUserController');
        Route::get('data-type-user', 'BackEnd\TypeUserController@getData')->name('data-type-user');
        Route::get('type-user/{Id}/delete', 'BackEnd\TypeUserController@destroy')->name('type-user.delete');

        Route::resource('districts', 'BackEnd\DistrictsControll');
        Route::get('data-districts', 'BackEnd\DistrictsControll@getData')->name('data-districts');
        Route::get('districts/{Id}/delete', 'BackEnd\DistrictsControll@destroy')->name('districts.delete');

        Route::resource('city', 'BackEnd\CityControll');
        Route::get('data-city', 'BackEnd\CityControll@getData')->name('data-city');
        Route::get('city/{Id}/delete', 'BackEnd\CityControll@destroy')->name('city.delete');


        Route::resource('district', 'BackEnd\DistrictsControll');
        Route::get('data-district', 'BackEnd\DistrictsControll@getData')->name('data-district');
        Route::get('district/{Id}/delete', 'BackEnd\DistrictsControll@destroy')->name('district.delete');

        Route::resource('province', 'BackEnd\ProvinceControll');
        Route::get('data-province', 'BackEnd\ProvinceControll@getData')->name('data-province');
        Route::get('province/{Id}/delete', 'BackEnd\ProvinceControll@destroy')->name('province.delete');

        Route::resource('country', 'BackEnd\CountryControll');
        Route::get('data-country', 'BackEnd\CountryControll@getData')->name('data-country');
        Route::get('country/{Id}/delete', 'BackEnd\CountryControll@destroy')->name('country.delete');

        Route::resource('my-profile', 'BackEnd\MyProfileControll');
        Route::get('data-my-profile', 'BackEnd\MyProfileControll@getData')->name('data-my-profile');
        Route::get('my-profile/{Id}/delete', 'BackEnd\MyProfileControll@destroy')->name('my-profile.delete');


        Route::resource('employee', 'BackEnd\EmployeeController');
        Route::get('data-employee', 'BackEnd\EmployeeController@getData')->name('data-employee');
        Route::get('employee/{Id}/delete', 'BackEnd\EmployeeController@destroy')->name('emloyee.delete');

        Route::resource('department', 'BackEnd\DepartmentControll');
        Route::get('data-department', 'BackEnd\DepartmentControll@getData')->name('data-department');
        Route::get('department/{Id}/delete', 'BackEnd\DepartmentControll@destroy')->name('department.delete');

        Route::resource('polyclinic', 'BackEnd\PolyClinicControll');
        Route::get('data-polyclinic', 'BackEnd\PolyClinicControll@getData')->name('data-polyclinic');
        Route::get('polyclinic/{Id}/delete', 'BackEnd\PolyClinicControll@destroy')->name('polyclinic.delete');

        Route::resource('room-service', 'BackEnd\RoomServiceControll');
        Route::get('data-room-service', 'BackEnd\RoomServiceControll@getData')->name('data-room-service');
        Route::get('room-service/{Id}/delete', 'BackEnd\RoomServiceControll@destroy')->name('room-service.delete');

        Route::resource('patient', 'BackEnd\PatientControll');
        Route::get('data-patient', 'BackEnd\PatientControll@getData')->name('data-patient');
        Route::get('patient/{Id}/delete', 'BackEnd\PatientControll@destroy')->name('patient.delete');

        Route::resource('schedule-doctor', 'BackEnd\ScheduleDoctorControll');
        Route::get('data-schedule-doctor', 'BackEnd\ScheduleDoctorControll@getData')->name('data-schedule-doctor');
        Route::get('schedule-doctor/{Id}/delete', 'BackEnd\ScheduleDoctorControll@destroy')->name('schedule-doctor.delete');
        Route::get('schedule-doctor/1/updatestatus', 'BackEnd\ScheduleDoctorControll@updateStatus')->name('schedule-doctor.update-status');

        Route::resource('complain', 'BackEnd\ComplainControll');
        Route::get('data-complain', 'BackEnd\ComplainControll@getData')->name('data-complain');
        Route::get('complain/{Id}/delete', 'BackEnd\ComplainControll@destroy')->name('complain.delete');
        Route::get('complain/1/messageview', 'BackEnd\ComplainControll@messageview')->name('complain.messageview');
        Route::get('complain/1/inbox', 'BackEnd\ComplainControll@inbox')->name('complain.inbox');
        Route::get('complain/1/sent', 'BackEnd\ComplainControll@sent')->name('complain.sent');
        Route::get('complain/1/compose', 'BackEnd\ComplainControll@compose')->name('complain.compose');

        Route::resource('register-online', 'BackEnd\RegistrationOnlineControll');
        Route::get('data-register-online', 'BackEnd\RegistrationOnlineControll@getData')->name('data-register-online');
        Route::get('register-online/{Id}/delete', 'BackEnd\RegistrationOnlineControll@destroy')->name('register-online.delete');
        Route::get('register-online/1/confirm-visited', 'BackEnd\RegistrationOnlineControll@visited')->name('register-online.confirm-visited');

        Route::resource('complain-type', 'BackEnd\ComplainTypeControll');
        Route::get('data-complain-type', 'BackEnd\ComplainTypeControll@getData')->name('data-complain-type');
        Route::get('complain-type/{Id}/delete', 'BackEnd\ComplainTypeControll@destroy')->name('complain-type.delete');

        Route::resource('complain', 'BackEnd\ComplainControll');
        Route::get('data-complain', 'BackEnd\ComplainControll@getData')->name('data-complain');
        Route::get('complain/{Id}/delete', 'register-onlineBackEnd\ComplainControll@destroy')->name('complain.delete');

        Route::resource('register-online-login', 'BackEnd\RegistrationOnlineLoginControll');
        Route::get('data-register-online-login', 'BackEnd\RegistrationOnlineLoginControll@getData')->name('data-register-online-login');
        Route::get('register-online-login/{Id}/delete', 'BackEnd\RegistrationOnlineLoginControll@destroy')->name('register-online-login.delete');
        Route::get('register-online-login/1/fetchPatient', 'BackEnd\RegistrationOnlineLoginControll@fetchPatient')->name('register-online-login.fetchPatient');

        Route::resource('queue', 'BackEnd\QueueControll');
        Route::get('data-queue', 'BackEnd\QueueControll@getData')->name('data-queue');
        Route::get('queue/{Id}/delete', 'BackEnd\QueueControll@destroy')->name('queue.delete');
        Route::get('queue/1/confirm-visited', 'BackEnd\QueueControll@visited')->name('queue.confirm-visited');

        Route::resource('methode-pay', 'BackEnd\MethodePayControll');
        Route::get('data-methode-pay', 'BackEnd\MethodePayControll@getData')->name('data-methode-pay');
        Route::get('methode-pay/{Id}/delete', 'BackEnd\MethodePayControll@destroy')->name('methode-pay.delete');

        Route::resource('faciliti-rooms', 'BackEnd\FacilitiRoomsControll');
        Route::get('data-faciliti-rooms', 'BackEnd\FacilitiRoomsControll@getData')->name('data-faciliti-rooms');
        Route::get('faciliti-rooms/{Id}/delete', 'BackEnd\FacilitiRoomsControll@destroy')->name('faciliti-rooms.delete');

    });
});