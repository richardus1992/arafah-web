<?php

use Illuminate\Http\Request;




Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(array('prefix'=>'v1/'),function(){
    Route::post('login-admin','MobileApi\ApiAdminControll@login');
    Route::get('data-versionapp','MobileApi\ApiAdminControll@getDataVersionApp');
    Route::post('customer-default-register','Api\CustomerControll@registerDefault');

    Route::get('query-test','Api\ApiRoomsControll@indexTest');
});

Route::group(['middleware' => ['auth:customer']], function () {
    Route::group(array('prefix'=>'v1/'),function(){
        Route::post('login','Api\CustomerControll@login');
        Route::get('customer','Api\CustomerControll@index');
        Route::get('rooms','Api\ApiRoomsControll@index');
        Route::get('rooms-facilities','Api\ApiRoomsControll@indexFacilities');
        Route::get('poly-clinic','Api\ApiPoliControll@index');
        Route::get('schedule-doctor','Api\ApiScheduleDoctorControll@index');
        Route::post('register','Api\CustomerControll@register');
        Route::post('my-profile-update','Api\CustomerControll@updateMyProfile');
        Route::get('complain-type','Api\ApiComplainTypControll@index');
        Route::get('complain/{idPasien}','Api\ApiComplainControll@index');
        Route::get('complain-detail/{idComplain}','Api\ApiComplainControll@indexDetail');
        Route::get('province','Api\ApiLocationControll@indexProvince');
        Route::get('city-province/{idPorvince}','Api\ApiLocationControll@indexCityProvince');
        Route::get('city-district/{idDistrict}','Api\ApiLocationControll@indexDistrictCity');
        Route::get('poly-clinic-all','Api\ApiPoliControll@indexAll');
        Route::get('poly-clinic-detail/{id}','Api\ApiPoliControll@indexScheduleDoctor');
        Route::get('methode-pay-all','Api\ApiMethodePayControll@indexAll');
        Route::get('schedule-doctor-all','Api\ApiScheduleDoctorControll@indexDetailAll');
        Route::get('schedule-doctor-register','Api\ApiScheduleDoctorControll@indexDetailRegister');
        Route::post('register-queue/store','Api\ApiRegisterQueueControll@store');
        Route::post('register-queue/check-patient','Api\ApiRegisterQueueControll@checkPatientExist');
        Route::get('queue/{id}','Api\ApiQueueControll@index');
        Route::get('history-check/{id}','Api\ApiHistoryCheckControll@index');
        Route::get('history-check-detail/{id}','Api\ApiHistoryCheckControll@indexDetail');
        Route::post('store-complain','Api\ApiComplainControll@store');
        Route::get('dashboard/{id}','Api\ApiDashBoardControll@index');
        Route::get('schedule-today','Api\ApiScheduleTodayControll@index');


    });
});
